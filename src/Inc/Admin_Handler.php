<?php
namespace Src\Inc;

class Admin_Handler
{
    private $conn;

    function __construct($conn)
    {
        $this->conn = $conn;
    }

       function __destruct()
    {
     mysqli_close($this->conn);
     $this->conn = null ;
   }

    public function getSupports($page , $limit)
    {
        $start = ($page -1) * $limit;
        $response = array();
        $st = $this->conn->prepare("select support_id , user_id , title , message,mobile,pic,created_at from support where state = 0  ORDER BY support_id  LIMIT $start,$limit  ");
        $st->execute();
        $result = $st->get_result();
        if ($result->num_rows == 0) {
           if ($page==1) {
               $response['message'] = "هیچ درخواستی ثبت نشده است";
               $response['status'] = 400;
               $st->close();
               return $response;
           }else {
               $response['message'] = "هیچ درخواست دیگری یافت نشد";
               $response['status'] = 404;
               $st->close();
               return $response;
           }


        } else {
            $response['status'] = 200;
            $response['supports'] = array();
            while ($single_support = $result->fetch_assoc()) {
                array_push($response['supports'], $single_support);
            }
            $st->close();
            return $response;
        }
    }

	   public function checkSupport($support_id)
    {


        $st = $this->conn->prepare("update support set state = 1 where support_id = ?");
		$st->bind_param("i",$support_id);
        $st->execute();
		if($this->conn->affected_rows > 0 ) {
			return getResponse("بررسی گزارش انجام شد" , 201);
		}else {
			return getResponse("خطا در به روز رسانی . لطفا دوباره تلاش کنید" , 401);
		}


    }


    ////=========================================================SocialFunctions=============================================================\\\\\\\\\\\\
    public function makeSocialCat($cat_deatils)
    {

        $upload_path = __DIR__ . '/../uploads/social_cat/';
        $file_info_pic = pathinfo($_FILES['pic']['name']);
        $file_info_icon = pathinfo($_FILES['icon']['name']);
        $randomNum = substr(str_shuffle("0123456789"), 0, 4);

        $pic_name_to_sotre = 'pic_' . $randomNum . '.' . $file_info_pic['extension'];
        $icon_name_to_store = 'icon_' . $randomNum . '.' . $file_info_icon['extension'];
        $file_path_pic = $upload_path . $pic_name_to_sotre;
        $file_path_bg = $upload_path . $icon_name_to_store;
        if (!move_uploaded_file($_FILES['pic']['tmp_name'], $file_path_pic)) {
            $object = (object)[
                "error" => true,
                "message" => "خطلا در اپلود عکس ",
                "error_code" => 501
            ];

            return $object;


        } else if (!move_uploaded_file($_FILES['icon']['tmp_name'], $file_path_bg)) {
            $object = (object)[
                "error" => true,
                "message" => "خطلا در اپلود عکس آیکون ",
                "error_code" => 501
            ];

            return $object;
        } else {


            $st = $this->conn->prepare("INSERT INTO sociall (p_name,e_name,uri,package,description,prefix,pic,icon) values (?,?,?,?,?,?,?,?)");
            $st->bind_param("ssssssss", $cat_deatils['pname'], $cat_deatils['ename'], $cat_deatils['uri'], $cat_deatils['package'], $cat_deatils['des'], $cat_deatils['prefix'],
                $pic_name_to_sotre, $icon_name_to_store);
            if (!$st->execute()) {

                $object = (object)[
                    "error" => true,
                    "message" => "خطا در ذخیره دسته بندی",
                    "error_code" => 501
                ];
            } else {
                $object = (object)[
                    "error" => false,
                    "message" => "دسته بندی جدید ذخیره شد"
                    , "error_code" => 201
                ];
            }
            $st->close();
            return $object;
        }
    }

    public function getAllSocial()
    {
        $response = array();
        $stmt = $this->conn->prepare("select social_id ,p_name, e_name, uri ,package,web
                ,description ,pic, icon ,active,filtered, prefix,prefix_p,member_prefix, created_at from sociall where active = 1;");

        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows == 0) {
            $stmt->close();
            $response[ERROR] = true;
            $response[MESSAGE] = 'هیچ دسته ای یافت نشد';
            $response[STATUS] = 500;
            return $response;
        } else {
            $stmt->close();
            $response['socials'] = array();
            while ($single = $result->fetch_assoc()) {
                array_push($response['socials'], $single);
            }
            $response[STATUS] = 201;
            return $response;
        }

    }

    public function updateSocial($social_id, $cat_deatils)
    {

        $response = array();
        $st = $this->conn->prepare("UPDATE  sociall set p_name = ? , e_name = ? , uri = ? ,package = ? , description = ? , prefix = ?  where 
            social_id like ? ");
        $st->bind_param("ssssssi", $cat_deatils['pname'], $cat_deatils['ename'], $cat_deatils['uri'], $cat_deatils['package'], $cat_deatils['des'], $cat_deatils['prefix'],
            $social_id);
        $st->execute();
        if ($this->conn->affected_rows > 0) {
            $response["error"] = false;
            $response["message"] = "به روز رسانی انجام شد";
            $response['status'] = 201;
            return $response;

        } else {
            $response["error"] = true;
            $response["message"] = "خطا در به روز رسانی";
            $response['status'] = 401;
            return $response;
        }
    }

    public function updateSocialActive($social_id, $state)
    {
        $response = array();
        $st = $this->conn->prepare("UPDATE  sociall set active = ?  where social_id like ? ");

        $st->bind_param("ii", $state, $social_id);
        $st->execute();
        if ($this->conn->affected_rows > 0) {
            $response["error"] = false;
            $response["message"] = "به روز رسانی انجام شد";
            $response['status'] = 201;
            return $response;

        } else {
            $response["error"] = true;
            $response["message"] = "خطا در به روز رسانی";
            $response['status'] = 401;
            return $response;
        }
    }

    public function updateSocialPic($pic_name)
    {
        $response = array();
        $upload_path = __DIR__ . '/../uploads/social_cat/';
        $file_path_pic = $upload_path . $pic_name;


        if (!move_uploaded_file($_FILES['pic']['tmp_name'], $file_path_pic)) {
            $response['error'] = true;
            $response['status'] = 501;
            $response['message'] = "خطا در آپلود عکس ";
            return $response;
        } else {

            $response['error'] = false;
            $response['status'] = 200;
            $response['message'] = "به روز رسانی عکس انجام شد";
            return $response;
        }
    }

    public function updateSocialIcon($icon_name)
    {
        $response = array();
        $upload_path = __DIR__ . '/../uploads/social_cat/';
        $file_path_icon = $upload_path . $icon_name;

        if (!move_uploaded_file($_FILES['icon']['tmp_name'], $file_path_icon)) {
            $response['error'] = true;
            $response['status'] = 501;
            $response['message'] = "خطا در آپلود آیکون ";
            return $response;
        } else {

            $response['error'] = false;
            $response['status'] = 200;
            $response['message'] = "به روز رسانی آیکون انجام شد";
            return $response;
        }
    }


    //  ================================================================CategoryFunctions================================================\\\\\\\\\\\\\\\\\\\\

    public function makeCat($cat_name)
    {
        $upload_path = __DIR__ . '/../uploads/cat/';
        $file_info_icon = pathinfo($_FILES['icon']['name']);
        $randomNum = substr(str_shuffle("0123456789"), 0, 3);
        $last_id = $this->getLastcatId();
        $icon_name_to_store = $last_id . '_' . $randomNum . '.' . $file_info_icon['extension'];

        $file_path_icon = $upload_path . $icon_name_to_store;
        if (!move_uploaded_file($_FILES['icon']['tmp_name'], $file_path_icon)) {

            return getResponse( "خطا در آپلود آیکون ", 401);
        } else {

            $st = $this->conn->prepare("INSERT INTO category (name,icon) values (? ,?) ;");
            $st->bind_param("ss", $cat_name, $icon_name_to_store);
            $st->execute();
            if ($this->conn->affected_rows > 0) {
                $last_id = $this->conn->insert_id;
                $st = $this->conn->prepare("select cat_id,name,icon,active from category where cat_id like ? ");
                $st->bind_param("i", $last_id);
                $st->execute();
                $result = $st->get_result();

                $response['error'] = false;
                $response['status'] = 201;
                $response['message'] = "دسته بندی جدید ذخیره شد";
                $response['category'] = $result->fetch_assoc();

                $st->close();
                return $response;
            } else {
                try {
                    unlink($upload_path . $icon_name_to_store);
                } catch (Exception $e) {
                }
                $st->close();
                return getResponse( "خطا در ساخت دسته بندی", 501);
            }
        }

    }

    //vase gereftan akharin id daste vase inke icon category ro ba in esme zakhrie konim
    public function getLastcatId()
    {
        $st = $this->conn->prepare("select cat_id as max from category ORDER BY  cat_id DESC  limit 1");
        $st->execute();
        $st->bind_result($max);
        $st->store_result();
        if ($st->num_rows > 0) {
            $st->fetch();

            $st->close();
            return $max + 1;

        } else {
            $st->close();
            return 1;
        }
    }

    public function getCats()
    {
        $response = array();

        $st = $this->conn->prepare("select cat_id , name , icon , active   from category ");
        $st->execute();
        $result = $st->get_result();

        if ($result->num_rows == 0) {
            $response['error'] = true;
            $response['message'] = "هیچ دسته ای ثبت نشده است";
            $response['status'] = 403;
            $st->close();
            return $response;

        } else {
            $response['error'] = false;
            $response['status'] = 201;
            $response['cats'] = array();

            while ($single_cat = $result->fetch_assoc()) {
                array_push($response['cats'], $single_cat);
            }
            $st->close();
            return $response;
        }
    }

    public function updateCatname($name, $cat_id)
    {

        $response = array();
        $st = $this->conn->prepare("update category set name = ? where cat_id  = ?");
        $st->bind_param("si", $name, $cat_id);


        $st->execute();
        if ($this->conn->affected_rows > 0) {
            $response['error'] = false;
            $response['message'] = "به روز رسانی انجام شد";
            $response['status'] = 201;
            $st->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message'] = "خطا در به روز رسانی";
            $response['status'] = 501;
            $st->close();
            return $response;
        }

    }

    public function updateCatState($state, $cat_id)
    {
        $response = array();
        $st = $this->conn->prepare("update category set active = ? where cat_id  = ?");
        $st->bind_param("ii", $state, $cat_id);
        $st->execute();
        if ($this->conn->affected_rows > 0) {
            $response['error'] = false;
            $response['message'] = "به روز رسانی انجام شد";
            $response['status'] = 201;
            $st->close();
            return $response;
        } else {
            $response['error'] = true;
            $response['message'] = "خطا در به روز رسانی";
            $response['status'] = 501;
            $st->close();
            return $response;
        }
    }

    public function updateCatIcon($icon_name)
    {
        $response = array();
        $upload_path = __DIR__ . '/../uploads/cat/';
        $file_path_icon = $upload_path . $icon_name;

        if (!move_uploaded_file($_FILES['icon']['tmp_name'], $file_path_icon)) {
            $response['error'] = true;
            $response['status'] = 501;
            $response['message'] = "خطا در آپلود آیکون ";
            return $response;
        } else {

            $response['error'] = false;
            $response['status'] = 200;
            $response['message'] = "به روز رسانی آیکون انجام شد";
            return $response;
        }


    }
    //  ================================================================ChanelFunctions================================================\\\\\\\\\\\\\\\\\\\\
    // gereftane safahati ke montazere taiid hastand
    public function getWatingPages($limit, $last_id)
    {

        $response=array();
        //   vase in migirim ke faqat tedade page haie qeyre active bad az on id idihi ke baresi nashode ro begirim ; agar ham ke last_id ro ke dashtim ke hichi
        if ($last_id == 0) {
            $s = $this->conn->prepare("SELECT MIN(page_id) as last FROM page where active = 0 ");
            $s->bind_result($last);
            $s->execute();
            $s->store_result();
            $s->fetch();
            // agar hich satri yaft nashe num_row 1 barmigardone ama ba meqdare null . pas agar null bood yani hich pagi ke actives sefr bashe nadarim
            if (is_null($last)) {
                return $response;
            }
            $s->close();
            $last_id = $last - 1;
        }

        $s = $this->conn->prepare("SELECT p.page_id ,p.user_id ,p.id , p.name,p.short_des,p.des,p.thumb,p.member ,  s.p_name ,s.prefix , c.name as cat_name   
                                                  from page p join sociall s on p.social_id = s.social_id 
                                                  join category c on p.cat_id = c.cat_id where p.page_id > ? and p.active = 0 ORDER BY p.created_at limit 0,? ");
        $s->bind_param("ii", $last_id,$limit);
        $s->execute();
        $result = $s->get_result();
            while ($single = $result->fetch_assoc()) {
                    $single["screens"]=array();


                    $st=$this->conn->prepare("select ps_id,active,name,thumb_name,page_id,created_at from page_screen where page_id like ? and active=0");
                    $st->bind_param("i",$single["page_id"]);
                    $st->execute();
                    $result_screen=$st->get_result();
                    while ($single_screen=$result_screen->fetch_assoc()){
                        array_push($single["screens"],$single_screen);
                    }
                    array_push($response, $single);

            }


            $s->close();
            return $response;


    }

    //taiid ya rad kardane page ke ba status 1 taiid mishe va -1 rad mishe ba yek payam vase user
    public function confirmPageState( $page_id, $status, $sender_id , $reciver_id, $state, $title, $message)
    {

        $this->conn->begin_transaction();
        $commit = true;
        $screen_dir= __DIR__ . '/../uploads/chanel_screen/';

        try {
            $s1 = $this->conn->prepare("UPDATE page SET active = ? WHERE page_id LIKE ? ");
            $s1->bind_param("ii", $status, $page_id);
            $s1->execute();
            if ($s1->affected_rows < 1) {
                $commit = false;
            }
            $s1->close();
            $s2 = $this->conn->prepare("INSERT INTO message (sender_id,reciver_id,title,message,state) values (?,?,?,?,?)");
            $s2->bind_param("iissi", $sender_id,$reciver_id, $title, $message, $state);
            $s2->execute();
            if ($s2->affected_rows < 1) {
                $commit = false;
            }
            if (!$commit) {
                $this->conn->rollBack();
                return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
            }

            $this->conn->commit();

        } catch (\PDOException $exception) {
            $this->conn->rollBack();
            return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
        }
        $fcm_token = $this->get_fcmToken($reciver_id);

        if ($status == 1) {
           // $this->send_fcm_notification($fcm_token,$title,$message);
            fireBaseNotify($title,$message,$fcm_token,0,0,1);
            return getResponse( "صفحه مورد نظر تایید گردید", 201);
        } else {

            $st=$this->conn->prepare("select ps_id , name , thumb_name from page_screen where page_id = ?");
            $st->bind_param("i",$page_id);
            $st->execute();
            $result= $st->get_result();
            while ($single = $result->fetch_assoc()) {
                @ unlink($screen_dir.$single["name"]);
                @ unlink($screen_dir.$single["thumb_name"]);
            }
            $st=$this->conn->prepare("delete from page_screen where page_id = ?");
            $st->bind_param("i",$page_id);
            $st->execute();
            $st->close();

            return getResponse( "صفحه مورد نظر رد گردید", 201);
        }


    }


    public function confrimScreen($ps_id, $state)
    {
        $screen_dir= __DIR__ . '/../uploads/chanel_screen/';

        $st=$this->conn->prepare("select name,thumb_name from page_screen where ps_id like ?");
        $st->bind_param("i",$ps_id);
        $st->bind_result($name,$thumb_name);
        $st->execute();
        $st->store_result();
        $st->fetch();



       if ($state==1) {

           $st=$this->conn->prepare("update page_screen set active = 1 where ps_id like ?");
           $st->bind_param("i",$ps_id);
           $st->execute();
           if ($this->conn->affected_rows>0) {
               return getResponse("تایید اسکرین انجام شد",200);
           }else {
               return getResponse("خطا در تایید اسکرین",400);
           }

       }else {

           $st=$this->conn->prepare("delete from page_screen where ps_id like ?");
           $st->bind_param("i",$ps_id);
           $st->execute();
           if ($this->conn->affected_rows>0) {
               @ unlink($screen_dir.$name);
               @ unlink($screen_dir.$thumb_name);
               return getResponse("حذف اسکرین انجام شد",200);
           }else {
               return getResponse("خطا در حذف اسکرین",400);
           }

       }


    }

    public function getUpdatedPages($limit, $page ) {

        $start = ($page-1) * $limit ;

        $query = "select pu_id , p.page_id , p.user_id , p.id , prefix, p.name , short_des , des , pic, thumb ,updated_at, 
        s.screens from page_update  p  left join (SELECT page_id , COUNT(ps_id) screens from page_screen where active = 0 GROUP by page_id) s on p.page_id = s.page_id
       order  BY updated_at limit $start,?";

        $s = $this->conn->prepare($query);
        $s->bind_param("i",$limit);
        $s->execute();
        $result = $s->get_result();
        if ($result->num_rows > 0) {
            $response[STATUS] = 200;
            $response['pages'] = array();
            while ($single = $result->fetch_assoc()) {
                $single["screens"]=array();


                $st=$this->conn->prepare("select ps_id,active,name,thumb_name,page_id,created_at from page_screen where page_id like ? and active=0");
                $st->bind_param("i",$single["page_id"]);
                $st->execute();
                $result_screen=$st->get_result();
                while ($single_screen=$result_screen->fetch_assoc()){
                    array_push($single["screens"],$single_screen);
                }



                array_push($response['pages'], $single);
            }
            $s->close();
            return $response;
        } else {
            return getResponse( "صفحه دیگری موجود نیست", 404);
        }
    }

    public function confrimUpdatedPage($pu_id ,$page_id , $user_id, $acept , $message , $title , $sender_id) {
        //vase in in 2ta ro migrim ke agar to jadvade page_update aks null nabood ba akse felie page jaygozin konim
        $pic_name = null ;
        $thumb_name = null;
        $page_pic_dir= __DIR__ . '/../uploads/chanel_pic/';
        $s=$this->conn->prepare("SELECT pic,thumb FROM page_update WHERE pu_id = ? ");
        $s->bind_param("i",$pu_id);
        $s->bind_result($pic_name,$thumb_name);
        $s->execute();
        $s->store_result();
        $s->fetch();
        //agar taiid shod
        if ($acept==1) {
           
            //check mikonim bebinim to qesmate update aks nulle ya na age nulle ke faqat etelat ro be roz resani mikonim bedone taqir dadane aks
            if ($pic_name==null) {
                $this->conn->begin_transaction();
                $commit = true;
                try {
                    $s1 = $this->conn->prepare("UPDATE page set name = (SELECT name from page_update where page_id = ? ) , short_des = (
                                                            SELECT short_des from page_update where page_id = ?
                                           ) , des=(SELECT des from page_update where page_id = ?) , lastmod=now() WHERE page_id = ?");
                    $s1->bind_param("iiii", $page_id,$page_id,$page_id,$page_id);
                  
                    $s1->execute();
                    if ($this->conn->affected_rows < 1) {
                        $commit=false;

                    }

                    $s2 = $this->conn->prepare("DELETE FROM page_update WHERE pu_id = ?");
                    $s2->bind_param("i", $pu_id);
                    $s2->execute();
                    if ($this->conn->affected_rows < 1) {
                        $commit=false;
                    }
                    //ersale payam
                    $state = 1 ;
                    $s3 = $this->conn->prepare("INSERT INTO message (sender_id, reciver_id,title,message,state) values (?,?,?,?,?)");
                    $s3->bind_param("iissi", $sender_id, $user_id, $title, $message, $state);
                    $s3->execute();
                    if (!$s3->execute()) {
                        $commit = false;
                    }

                    if (!$commit) {
                        return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
                    }
                    $this->conn->commit();

                }catch (\Exception $exception) {
                    return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
                }
                $fcm_token = $this->get_fcmToken($user_id);
               // $this->send_fcm_notification($fcm_token,$title,$message);
                fireBaseNotify($title,$message,$fcm_token,0,0,1);
                return getResponse( "اطلاعات صفحه به روز گردید", 201);

            }
            else {
                //agar toye table page update pic null nabood bayad pic ro ba pic table page jaygozin koim va male page ro hazf konim
                $st = $this->conn->prepare("SELECT pic,thumb FROM page WHERE page_id = ? ");
                $st->bind_param("i", $page_id);
                $st->bind_result($pic_name_to_delete, $thumb_name_to_delete);
                $st->execute();
                $st->store_result();
                $st->fetch();
                $s->close();
                $this->conn->begin_transaction();
                $commit = true;
                try {
                    $s1 = $this->conn->prepare("UPDATE page set name = (SELECT name from page_update where page_id = ? ) , short_des = (
                                                            SELECT short_des from page_update where page_id = ?
                                           ) , des=(SELECT des from page_update where page_id = ?),
                                           pic=(SELECT pic from page_update where page_id = ?) ,
                                           thumb = (SELECT thumb from page_update where page_id = ?) , lastmod=now()
                                           WHERE page_id = ?");
                    $s1->bind_param("iiiiii", $page_id, $page_id, $page_id, $page_id, $page_id, $page_id);
                    $s1->execute();
                    if ($this->conn->affected_rows < 1) {
                        $commit = false;
                    }
                     //pak kardane update bad az mege shodan
                    $s = $this->conn->prepare("DELETE FROM page_update where pu_id = ?");
                    $s->bind_param("i", $pu_id);
                    $s->execute();
                    if ($this->conn->affected_rows < 1) {
                        $commit = false;
                    }
                    //ersale payam
                    $s2 = $this->conn->prepare("INSERT INTO message (sender_id, reciver_id,title,message,state) values (?,?,?,?,?)");
                    $state = 2 ;
                    $s2->bind_param("iissi", $sender_id, $user_id, $title, $message, $state);
                    $s2->execute();
                    if ($s2->affected_rows < 1) {
                        $commit = false;
                    }
                    if (!$commit) {
                        return getResponse("خطا در به روز رسانی : دوباره تلاش نمایید", 401);
                    }
                    $this->conn->commit();

                } catch (\Exception $exception) {
                    return getResponse("خطا در به روز رسانی : دوباره تلاش نمایید", 401);
                }

                try {
                    /// dar nahayt bad az inke pic az table page_update jaygozin shod miam o pic asli page ro hazf mikonim
                    @  unlink($page_pic_dir . $pic_name_to_delete);
                    @  unlink($page_pic_dir . $thumb_name_to_delete);
                   } catch (\Exception $exception) {
                }

                $fcm_token = $this->get_fcmToken($user_id);
               // $this->send_fcm_notification($fcm_token,$title,$message);
                fireBaseNotify($title,$message,$fcm_token,0,0,1);
                return getResponse( "اطلاعات صفحه به روز گردید", 201);
            }

        }
        //agar taiid nashod
        else {
            $this->conn->begin_transaction();
            $commit = true;
            try {
                $s = $this->conn->prepare("DELETE FROM page_update where pu_id = ?");
                $s->bind_param("i", $pu_id);
                $s->execute();
                if ($this->conn->affected_rows < 1) {
                    $commit=false;
                }
                //ersale payam
                $s2 = $this->conn->prepare("INSERT INTO message (sender_id, reciver_id,title,message,state) values (?,?,?,?,?)");
                $state = 2 ;
                $s2->bind_param("iissi", $sender_id, $user_id, $title, $message, $state);
                $s2->execute();
                if ($s2->affected_rows < 1) {
                    $commit = false;
                }

                if (!$commit) {
                    return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
                }
                $this->conn->commit();

            }catch (\Exception $exception) {

                return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
            }

            if ($pic_name!=null) {
                try {
                    //dar nahayat agar thumb 1 bood yanim ham thumb ham profile null mishan pas akseshonam pak mishe
                    @  unlink($page_pic_dir.$pic_name);
                    @  unlink($page_pic_dir.$thumb_name);
                   }catch (\Exception $exception) {
                }
            }

            return getResponse( "به روز رسانی صفحه لغو گردید", 201);
        }

    }

    // gereftan profilhaie ke update shodan va bayad baresi beshand bar asase baze zamani . yani harki zoodtar update shode zoodtar gerefte mishe
    public function getWatingProfiles($limit, $last_timestamp )
    {
        // vase in in boolean ro dar nazar migirm ke agar requeste avalin  safhe bood betonim bahash query bezanim vase satrhaie ke dateshon >= ba min date
        $first_request = false;
        // yani inke to avalin safhast pas mire az kamtarin meghdari shoro kone ke update codesh 1 e
        if ($last_timestamp == 'f') {
            $first_request = true;
            $s = $this->conn->prepare("SELECT MIN(updated_at) as first FROM user_info where updated_code = 1 ");
            $s->bind_result($minDate);
            $s->execute();
            $s->store_result();
            $s->fetch();
            // agar hich satri yaft nashe num_row 1 barmigardone ama ba meqdare null . pas agar null bood yani hich pagi ke actives sefr bashe nadarim
            if (is_null($minDate)) {
                return getResponse( "هیچ پروفایل تازه ای به روز نگردیده است", 400);
            }
            $last_timestamp = $minDate;
            $s->close();
        }

        // dar halate adi mishe onai ro begir ke az akharin time bozorgtarand vali vase safhe aaval mishe onai ke bozorgtar mosavie avalian
        $query = "select user_id , username , thumb , bio , pass_count , name, updated_at from user_info where updated_code = 1 and updated_at > ?    ORDER  BY updated_at limit 0,? ";
        if ($first_request) {
            $query = "select user_id , username , thumb , bio , pass_count , name ,  updated_at from user_info where  updated_code = 1  and updated_at >= ?  ORDER  BY updated_at limit 0,? ";
        }
        $s = $this->conn->prepare($query);
        $s->bind_param("si", $last_timestamp,$limit);
        $s->execute();
        $result = $s->get_result();
        if ($result->num_rows > 0) {
            $response[STATUS] = 200;
            $response['profiles'] = array();
            while ($single = $result->fetch_assoc()) {
                array_push($response['profiles'], $single);
            }
            $s->close();
            return $response;
        } else {
            return getResponse( "پروفایل دیگری موچود نیست", 404);
        }

    }
    //taiie profileha
    public function confirmProfileState($user_id, $accept, $username, $thumb, $bio, $name , $state, $title, $message,$sender_id)
    {
        // manteq be in shekle ke agar acept 1 bood updated_code az 1 yani dar hale entezar be 2 taqir mikone va hich taqire digei ijad nemishe va payam ham send nemishe
        //   vase karbar ama agar 0 bood yani bayad fildai ke meqdareshon 1 hastesh ro barabar ba null qarar bedim  va ye message bedim be karbar begim felan filda pak shod
         $profile_dir = __DIR__ . '/../uploads/profile/';

         //Accept = 1 ==> yani profile taiid mishe va faqat update code 2  mishe
        if ($accept == 1) {
            $s = $this->conn->prepare("UPDATE user_info SET updated_code = 2 WHERE user_id = ? ");
            $s->bind_param("i", $user_id);
            $s->execute();
            if ($this->conn->affected_rows > 0) {
                return getResponse( "تایید حساب کاربری انجام شد", 200);
            } else {
                return getResponse( "خطا در تایید حساب کاربری", 500);
            }
        } else {

        //    onai ke meqdareshon 1 hastesh toye query meqdareshon null mishe
            $parametrs = array();
            if ($username == 1){
                $parametrs [] = 'username = null';
            }
            if ($bio == 1) {
                $parametrs [] = 'bio = null';
            }
            if ($name == 1) {
                $parametrs [] = 'name = null';
            }
            if ($thumb == 1) {
                $parametrs [] = 'profile = null , thumb = null ';

                //inja agar thumb 1 bood bayad maqadiresh ro begirim ke bad az null shodaneshon pakeshon ham bokonim
                $st=$this->conn->prepare("select profile , thumb from user_info where user_id like ? ");
                $st->bind_param("i",$user_id);
                $st->bind_result($profile_,$thumb_);
                $st->execute();
                $st->store_result();
                $st->fetch();
                $st->close();
            }

            $this->conn->begin_transaction();
            $commit = true;
            try {
                // update kardane  on sotonhai  ke bayad null beshan : pass count yani ta hala chand dafe rad shode ke ba har bar rad shodan ye done ezaf mishe
                $sql = 'UPDATE user_info set ' . implode(', ', $parametrs) . ' , pass_count = pass_count +1 , updated_code = 3  where user_id like ? ';
                $s = $this->conn->prepare($sql);
                $s->bind_param("i", $user_id);
                $s->execute();
                if ($this->conn->affected_rows < 1) {
                    $commit=false;
                }

                if (!$this->sendMessage($sender_id,$user_id,$title,$message,2)) {
                    $commit=false;
                }
                if (!$commit) {
                    return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
                }
                $this->conn->commit();

            }catch (\Exception $exception) {
                $commit = false;
                return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
            }
            if ($thumb==1) {
                try {
                    //dar nahayat agar thumb 1 bood yanim ham thumb ham profile null mishan pas akseshonam pak mishe
               @  unlink($profile_dir.$thumb_);
               @  unlink($profile_dir.$profile_);
                }catch (\Exception $exception) {
                }
            }
            return getResponse("پروفایل کاربر به حالت قبلی بازگشت",200);
           }


}

   public  function getWatingsCm($limit , $page) {
        $response=array();
        $offset = ($page-1) * $limit;
        $s=$this->conn->prepare("SELECT comment_id,text,reply,active,reply_active,updated_at FROM comment 
                                  where active = 0 or reply_active = 0 ORDER BY updated_at ASC LIMIT $offset,$limit");
         $s->execute();
         $result  = $s->get_result();
         if ($result->num_rows > 0 ) {
             $response[STATUS] =200;
             $response["comments"] = array();
             while ($single = $result->fetch_assoc()) {
                 array_push($response["comments"], $single);
             }
             return $response;

         }else{
             if ($page==1) {
                 return getResponse("هیج نظری یافت نشد", 400);
             }else{
                 return getResponse("نظر دیگری یافت نشد", 404);
             }

         }



   }

   public function confrimComment($comment_id ,$acept , $type){
        if ($type=="user") {
            if ($acept==1) {
                $s=$this->conn->prepare("UPDATE comment set active = 1 where comment_id = ?");
                $s->bind_param("i",$comment_id);
                $s->execute();
                if ($s->affected_rows > 0) {
                    return getResponse("به روز رسانی انجام و نظر تاید گردید",201);
                }else {
                    return getResponse("خطا در تایید نظر",201);
                }
            }else {
                $s=$this->conn->prepare("DELETE FROM comment where comment_id = ?");
                $s->bind_param("i",$comment_id);
                $s->execute();
                if ($s->affected_rows > 0) {
                    return getResponse("به روز رسانی انجام و نظر حذف گردید",201);
                }else {
                    return getResponse("خطا در حذف نظر",201);
                }
            }

        }else {
            if ($acept==1) {
                $s=$this->conn->prepare("UPDATE comment set reply_active = 1 where comment_id = ?");
                $s->bind_param("i",$comment_id);
                $s->execute();
                if ($s->affected_rows > 0) {
                    return getResponse("به روز رسانی انجام و پاسخ تاید گردید",201);
                }else {
                    return getResponse("خطا در تایید پاسخ",201);
                }
            }else {
                // asasan onai ke replyeshon nulle reply activeshon -1 hastesh
                $s=$this->conn->prepare("UPDATE comment set reply = null , reply_active = -1 where comment_id = ?");
                $s->bind_param("i",$comment_id);
                $s->execute();
                if ($s->affected_rows > 0) {
                    return getResponse("به روز رسانی انجام و پاسخ حذف گردید",201);
                }else {
                    return getResponse("خطا در حذف پاسخ",201);
                }
            }
        }
   }

    // gerftane tedade chizhaie ka bayad taiid beshan va namayd to list mesle
    public function confirmCount()
    {
        $response_page = array();
        $has_page = true;
        // tedae page haye dar entezare taiid
        $s = $this->conn->prepare("SELECT MIN(page_id) as last FROM page where active = 0 ");
        $s->bind_result($last_wating_pages);
        $s->execute();
        $s->store_result();
        $s->fetch();
        if (is_null($last_wating_pages)) {
            $count_wating_pages = 0;
            $has_page = false;
        }
        $s->close();
        if ($has_page) {
            $last_wating_pages = $last_wating_pages - 1;
            $s = $this->conn->prepare("select count(*) count from page where page_id > ? and active = 0");
            $s->bind_param("i", $last_wating_pages);
            $s->bind_result($count_wating_pages);
            $s->execute();
            $s->store_result();
            $s->fetch();
            $s->close();
        }
        $response_page['title'] = "صفحات در انتظار تایید";
        $response_page['count'] = $count_wating_pages;


        $response_userinfo = array();
        $s = $this->conn->prepare("select count(*) as count from user_info where updated_code = 1");
        $s->bind_result($count_wating_profiles);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $s->close();
        $response_userinfo['title'] = "پروفایل های به روز شده";
        $response_userinfo['count'] = $count_wating_profiles;


        $response_updated_page=array();
        $s=$this->conn->prepare("select count(*) as count from page_update");
        $s->bind_result($count_updated_pages);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $s->close();
        $response_updated_page["title"] = "صفحات به روز شده";
        $response_updated_page["count"] = $count_updated_pages;

        $response_banner=array();
        $s=$this->conn->prepare("select count(*) as count from page_banner");
        $s->bind_result($count_banners);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $s->close();
        $response_banner["title"] = "بنر های در انتظار";
        $response_banner["count"] = $count_banners;

        $response_cm=array();
        $s=$this->conn->prepare("select count(*) as count from comment where active = 0 or reply_active = 0");
        $s->bind_result($count_cm);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $s->close();
        $response_cm["title"] = "نظرات در انتظار";
        $response_cm["count"] = $count_cm;

        $final = array();
        array_push($final, $response_page);
        array_push($final,$response_updated_page);
        array_push($final, $response_userinfo);
        array_push($final, $response_banner);
        array_push($final, $response_cm);
        return $final;

    }

    //gereftane payamhye sefareshi
    public function customMessages($parent_id)
    {

        $response = array();
        $st = $this->conn->prepare("SELECT title , message, state FROM custom_message WHERE parent_id = ?");
        $st->bind_param("i", $parent_id);
        $st->execute();
        $result = $st->get_result();


        while ($single = $result->fetch_assoc()) {
            array_push($response, $single);
        }
        return $response;


    }
    public function getWatingBanners($limit , $last_id) {
        $condition = $last_id == 0 ? "pb_id >= ?" : "pb_id > ?";

        if ($last_id==0) {
         $st=$this->conn->prepare("SELECT min(pb_id) as last from page_banner");
            $st->bind_result($last_id);
            $st->execute();
            $st->store_result();
            $st->fetch();
            // agar hich satri yaft nashe num_row 1 barmigardone ama ba meqdare null . pas agar null bood yani hich pagi ke actives sefr bashe nadarim
            if (is_null($last_id)) {
                return getResponse( "هیچ بنری یافت نشد", 400);
            }
            $st->close();
        }
        $s = $this->conn->prepare("select b.pb_id, b.page_id ,b.user_id, b.banner,p.id,s.prefix,s.p_name from page_banner b join page p on b.page_id = p.page_id
                        join sociall s on p.social_id = s.social_id
                         where ".$condition . " order by pb_id limit 0,?");
        $s->bind_param("ii", $last_id,$limit);
        $s->execute();
        $result = $s->get_result();
        if ($result->num_rows > 0) {
            $response[STATUS] = 200;
            $response['banners'] = array();
            while ($single = $result->fetch_assoc()) {
                array_push($response['banners'], $single);
            }
            $s->close();


            return $response;
        } else {
            $s->close();
            return getResponse( "بنر دیگری یافت نگردید", 404);
        }

    }
    public function confirmBanner($pb_id,$banner,$page_id, $status,$sender_id,$reciver_id,$title,$message) {
        $profile_dir = __DIR__ . '/../uploads/banner/';

        //rad shodane banner
        if ($status==0) {
            $this->conn->begin_transaction();
            $commit = true;
          try {
              $s1 = $this->conn->prepare("DELETE FROM page_banner where pb_id = ?");
              $s1->bind_param("i", $pb_id);
              $s1->execute();
              if ($this->conn->affected_rows < 1) {
                  $commit = false;
              }
              $s1->close();
              if (!$this->sendMessage($sender_id,$reciver_id,$title,$message,2)) {
                  $commit=false;
              }
              if (!$commit) {
                  $this->conn->rollback();
                  return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
              }
              $this->conn->commit();
          }catch (\PDOException $e) {
              $this->conn->rollback();
              return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
          }
          try {
            @  unlink($profile_dir.$banner);
          }catch (\Exception $e)
            {
            }

            return getResponse( "بنر مورد نظر تایید نشد و حذف گردید", 201);
        }else {
           // vase in migirim ke check konim nulle ya na
          $st=$this->conn->prepare("SELECT banner FROM page where page_id = ?");
          $st->bind_param("i",$page_id);
          $st->bind_result($page_banner);
          $st->execute();
          $st->store_result();
          $st->fetch();

          $commit = true;
            try {
                $s1 = $this->conn->prepare("UPDATE page set banner = ? where page_id = ?");
                $s1->bind_param("si", $banner,$page_id);
                $s1->execute();
                if ($this->conn->affected_rows < 1) {
                    $commit = false;
                }
                $s1->close();

                $s1 = $this->conn->prepare("DELETE FROM page_banner where pb_id = ?");
                $s1->bind_param("i", $pb_id);
                $s1->execute();
                if ($this->conn->affected_rows < 1) {
                    $commit = false;
                }
                $s1->close();

                if (!$this->sendMessage($sender_id,$reciver_id,$title,$message,1)) {
                    $commit=false;
                }
                if (!$commit) {
                    $this->conn->rollback();
                    return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
                }
                $this->conn->commit();
            }catch (\PDOException $e) {
                $this->conn->rollback();
                return getResponse( "خطا در به روز رسانی : دوباره تلاش نمایید", 401);
            }

            if (!is_null($page_banner)) {
                try {
                    @  unlink($profile_dir.$page_banner);
                }catch (\Exception $e)
                {
                }
            }
            $fcm_token = $this->get_fcmToken($reciver_id);
          //  $this->send_fcm_notification($fcm_token,$title,$message);
            fireBaseNotify($title,$message,$fcm_token,0,0,1);   
            return getResponse( "بنر مورد نظر تایید گردید ", 201);
        }
    }
    // reciver id hamoon user_id hastesh
    public function sendMessage($sender_id, $reciver_id,$title,$message,$state) {
        $s2 = $this->conn->prepare("INSERT INTO message (sender_id, reciver_id,title,message,state) values (?,?,?,?,?)");
        $s2->bind_param("iissi", $sender_id, $reciver_id, $title, $message, $state);
        $result =  $s2->execute();
        $s2->close();
        return $result;
    }

    /////////////////////////////////////////////////////////////////////Special\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    // gereftane etelate avali ke vase estelamo ina lazeme
    public function getPurchaseNeeds($special_id , $page_id) {
        $needs=array();
        $st=$this->conn->prepare("select is_main,banner_need,max from special where special_id = ?");
        $st->bind_param("i",$special_id);
        $st->bind_result($is_main,$banner_need,$max);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $needs["is_main"]=$is_main;
        $needs["max"]=$max;
        $needs["banner_need"]=$banner_need;

        $st=$this->conn->prepare("select p.user_id,p.cat_id,p.social_id,s.prefix from page p join sociall s on p.social_id = s.social_id where page_id = ?");
        $st->bind_param("i",$page_id);
        $st->bind_result($user_id,$cat_id,$social_id,$prefix);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        $needs["user_id"]=$user_id;
        $needs["cat_id"]=$cat_id;
        $needs["social_id"]=$social_id;
        $needs["prefix"]=$prefix;
        return $needs;

    }
    public function reservByUs($max,$cat_id,$is_main,$social_id,$page_id,$special_id,$user_id,$prefix,$banner_need) {
        $sr_id = 0 ;
        $so_id = 0 ;
        //chon vase daste asli bayad ba cat_id null zakhire she check mikoinm bebin agar is main 1 bood pas cat_id ro null mikonim
        if ($is_main==1) {
            $cat_id=null;
        }
        if ($banner_need==1) {
            if ($this->checkPageHasNotBanner($page_id)) {
                return getResponse("برای رزور این جایگاه ابتدا باید بنر " . $prefix . " را ثبت نمایید",402);
            }

        }
        // marhale be marhale mirim jolo agar jai moshkel dasht ba ye error return mikonim vagar na ta akhare reserve ro hamin ja mirim


        //check kardane inke qablan reserve nashode bashe
        $st= $this->conn->prepare("select sr_id from special_r where page_id = ? and special_id = ?");
        $st->bind_param("ii",$page_id,$special_id);
        $st->execute();
        $result = $st->get_result();
        if ($result->num_rows > 0) {
            return getResponse($prefix . " مورد نظر قبلا در این جایگاه رزرو گردیده است",422); // yani in page qablan toye in jaygah sabt shode
        }

        $this->conn->begin_transaction();
        try {
            //chon date objecte va string nist bayad zaman hamoon ro tabdin be string koinm
            $reserved_flag=2;

            // in marhale aval miam ye reserv ebtedai ba flage 0  mikonim va badesh mibnim ke aya tedadesh az tedade maxe special bishtar mishe ya na
            $st=$this->conn->prepare("INSERT INTO special_r (page_id , user_id , cat_id , social_id, special_id, reserved_flag)
                  values (?,?,?,?,?,?)");
            $st->bind_param("iiiiii",$page_id,$user_id,$cat_id,$social_id,$special_id,$reserved_flag );


            if (!$st->execute()) {
                $this->conn->rollBack();
                getResponse($prefix . "خطا در ثبت  .کد خطا : 1 . لطفا دوباره تلاش کنید ",400); // khata to insert kardan reserve initial
            }



            if ($is_main) {
                $st=$this->conn->prepare("SELECT count(*) as count  from special_r where social_id = ? and special_id = ? and reserved_flag = 2");
                $st->bind_param("ii",$social_id,$special_id);
            }else{
                $st=$this->conn->prepare("SELECT count(*) as count  from special_r where cat_id = ? and social_id = ? and special_id = ? and reserved_flag = 2");
                $st->bind_param("iii",$cat_id,$social_id,$special_id);
            }
            $st->bind_result($count);
            $st->execute();
            $st->store_result();
            $st->fetch();
            if ($count > $max) {
                $this->conn->rollBack();
                return getResponse("متاسفانه ظرفیت این جایگاه در حال حاضر تکمیل  شده است .", 409);  // tedade jaygaha vase in qestmat por shode
            }


            // dar kol agar hameye inserta va updata dorost bashan pas commit mikonim va peyqam moafaqiat amiz boodane reserv ro midim
            $this->conn->commit();
            return getResponse("رزرو با موفقیت انجام شد",201);

        }catch (\PDOException $e) {
            $this->conn->rollBack();
            return     getResponse($prefix . "خطا در ثبت  . لطفا دوباره تلاش کنید ",400);

        }

    }
    public function checkPageHasNotBanner($page_id){
        $st=$this->conn->prepare("SELECT banner from page where page_id = ?");
        $st->bind_param("i",$page_id);
        $st->bind_result($banner);
        $st->execute();
        $st->store_result();
        $st->fetch();

        return is_null($banner);
    }
    public function get_fcmToken($user_id) {
        $st=$this->conn->prepare("select fcm_token from users WHERE user_id like ?");
        $st->bind_param("i",$user_id);
        $st->bind_result($user_id);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        return $user_id;
    }
    public function send_fcm_notification($to, $title,$body) {

        $payload = array(
            "to"=>$to,
            "priority"=>"high",
            "mutable_content"=>true,
            "data"=>array(
                "title"=>"'".$title."'"  ,
                "body"=>"'".$body."'",
                "flag"=>1 ,
                "timestamp" => "'".date('Y-m-d G:i:s')."'"

            )
        );

//        $payload = {
//            "to" : $to,
//            "priority" : "high",
//            "mutable_content" : true,
//            "data" : {
//                "title" : $title,
//                "body" :   $body ,
//                "sound" : "default"
//		}
//	};


        $headers = array(
            'Authorization:key='.SERVER_KEY_FCM,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $payload ) );
        $result = curl_exec($ch );
        curl_close( $ch );



    }
    /////////////////////////////////////////////////////////////////////Activation\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function adLog($mobile,$password)
    {
          
            $st = $this->conn->prepare("SELECT  password,apikey,active  FROM users WHERE mobile LIKE ?");
            $st->bind_param("s", $mobile);
            $st->bind_result($password_hash, $apikey, $active);
            $st->execute();
            $st->store_result();
            if ($st->num_rows > 0) {
 
                $st->fetch();
                if (password_verify($password,$password_hash)) {
                    if ($active >= 5) {
                        $response = array();
                        $response[STATUS] = 200;
                        $response["apikey"] = $apikey;
                        return $response;
                    } else {
                        return getResponse(" اطلاعات وارد شده صحیح نمیباشد", 403);
                    }
                }else {
                    return getResponse(" اطلاعات وارد شده صحیح نمیباشد", 403);
                }
            }



    }
    public function getUserByMobile($mobile){

        $response = array();
        $st=$this->conn->prepare("select user_id , mobile , inventory , active from users where mobile like ?");
        $st->bind_param("s",$mobile);
        $st->execute();
        $result  =   $st->get_result();
        if ($result->num_rows > 0 ) {
          
            $user= $result->fetch_assoc();
            $response[STATUS] = 200;
            $response["user"] = $user;
        }else {
            $response[STATUS] = 404;
            $response[MESSAGE] = "کاربر مورد نظر یافت نشد";
        }
        $st->close();
        return $response;
    }
    public function getUserTransactioon($page,$limit,$user_id) {
        $response =array();
        $start = ($page-1) * $limit;
        $st=$this->conn->prepare("select o.go_id ,o.inventory,o.trans_id ,o.state,o.code,o.updated_at,g.title from gold_order o
                                        join gold g on o.gold_id = g.gold_id where 
                             user_id like ? and o.state = 1 or o.state=6 order by go_id DESC LIMIT  $start,$limit");
        $st->bind_param("i",$user_id);
        $st->execute();
        $result = $st->get_result();
        if ($result->num_rows > 0) {
            $response["transactions"] = array();
            while ($single = $result->fetch_assoc()) {
                array_push($response["transactions"],$single);
            }
            $response[STATUS]=200;
            return $response;


        }else {
            if ($page==1) {
                $response[STATUS]=404;
                $response[MESSAGE]="هیچ تراکنشی توسط این کاربر ثبت نشده است";
                return $response;
            }else {
                $response[STATUS]=404;
                $response[MESSAGE]="تراکنش دیگری یافت نشد";
                return $response;
            }
        }
    }
    public function activateUser($user_id,$active) {
        $response = array();
        $st=$this->conn->prepare("update users set active = ? where user_id like ?");
        $st->bind_param("ii",$active,$user_id);
        $st->execute();

        if ($this->conn->affected_rows > 0 ) {
            $response[STATUS] = 200;
            $response[MESSAGE] =  $active == 1 ? "کاربر مورد نظر فعال شد " : "حساب کاربری کاربر به حالت تعلیق درآمد";
            return $response;
        }else {
            $response[STATUS] = 400;
            $response[MESSAGE] = "خطا در به روز رسانی اطلاعت کاربر : دوباره تلاش کنید";
        }
        $st->close();
        return $response;
    }
    public function increaseInventory($user_id,$inventory) {
        $response = array();
        $st=$this->conn->prepare("update users set inventory = inventory + ? where user_id like ?");
        $st->bind_param("ii",$inventory,$user_id);
        $st->execute();

        if ($this->conn->affected_rows > 0 ) {
            $response[STATUS] = 200;
            $response[MESSAGE] = "افزایش سکه به میزان " . $inventory . "عدد انجام گردید";
            return $response;
        }else {
            $response[STATUS] = 400;
            $response[MESSAGE] = "خطا در به روز رسانی ر : دوباره تلاش کنید";
        }
        $st->close();
        return $response;
    }

    public function getSocialByPageid($page_id){
        $stmt= $this->conn->prepare("select social_id from page where page_id like ? ");
        $stmt->bind_param("i",$page_id);
        $stmt->bind_result($social_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->fetch();
        return $social_id;
    }

    public function getCatIdByPageId($page_id){

        $stmt= $this->conn->prepare("select cat_id from page where page_id like ? ");
        $stmt->bind_param("i",$page_id);
        $stmt->bind_result($cat_id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->fetch();
        return $cat_id;

    }

}



