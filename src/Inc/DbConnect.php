<?php
namespace Src\Inc;

/**
 * Created by PhpStorm.
 * User: hamid
 * Date: 5/2/2018
 * Time: 3:56 PM
 */
class DbConnect {
private $conn ;


public function __construct()
{
}

function connect() {


    require_once __DIR__  . '/Config.php';

    $this->conn = new \mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
    $this->conn->set_charset("utf8mb4");

    if (mysqli_connect_errno()) {
       $response = ['error'=>"faild" , 'message' =>'faild to connect to database'];
       die(json_encode($response));

    }else {
        return $this->conn;
    }
}




}
