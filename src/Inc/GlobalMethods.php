<?php

function isTheseParametersAvailable($required_fields)
{
    $error = false;
    $error_fields = "";
    $request_params = $_REQUEST;

    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        $response = array();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echo json_encode($response);
        return false;
    }
    return true;
}

function getResponse($message,$status) {
    $respose=array();
    $respose[MESSAGE]=$message;
    $respose[STATUS]=$status;
    return $respose;
}

function sendVerifySms($mobile , $otp , $templeate){
    $query=http_build_query(array('receptor' => $mobile , 'token' => $otp , 'template' => $templeate) , null, "&", PHP_QUERY_RFC3986);
    CallAPI("get",'v1/30727A4E2F4175644B7967416D4E7378306D7362573379303362615A6E764773/verify/lookup.json?'.$query ,array(),array());
}



function sendSmsRegister($mobile ,$otp) {
    $message = "به پوشکا مرجع شبکه های اجتماعی کشور خوش آمدید کد تاید شما :  " . $otp ;
    $query=http_build_query(array('receptor' => $mobile , 'message' => $message), null, "&", PHP_QUERY_RFC3986);
    CallAPI("get",'v1/30727A4E2F4175644B7967416D4E7378306D7362573379303362615A6E764773/sms/send.json?'.$query ,array(),array());

//    try {
//
//
//        date_default_timezone_set("Asia/Tehran");
//        $message = "به  نور الصالحین خوش آمدید رمز تایید حساب : " . $otp ;
//
//        // your sms.ir panel configuration
//        $APIKey = "b47d87e6a117f558c5617e17";
//        $SecretKey = "&&5212tsts";
//        $LineNumber = "50002015008386";
//
//        // your mobile numbers
//        $MobileNumbers = array($mobile);
//
//        // your text messages
//        $Messages = array($message);
//
//
//
//        $SmsIR_SendMessage = new SmsIR_SendMessage($APIKey,$SecretKey,$LineNumber);
//        $SendMessage = $SmsIR_SendMessage->SendMessage($MobileNumbers,$Messages);
//     //   var_dump($SendMessage);
//
//    } catch (Exeption $e) {
//        echo 'Error SendMessage : '.$e->getMessage();
//    }


}

function sendSmsReset($mobile ,$otp) {
    $message = " کد بازیابی رمز عبور شما در پوشکا  : " . $otp ;
    $query=http_build_query(array('receptor' => $mobile , 'message' => $message), null, "&", PHP_QUERY_RFC3986);
    CallAPI("get",'v1/30727A4E2F4175644B7967416D4E7378306D7362573379303362615A6E764773/sms/send.json?'.$query ,array(),array());


}
function CallAPI($method, $api, $data, $headers) {
    $url = SITEURL . "/" . $api;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    switch ($method) {
        case "GET":
            curl_setopt($curl,CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            break;
        case "POST":
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl,CURLOPT_POST, count($data));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            break;
        case "DELETE":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            break;
    }


    $response = curl_exec($curl);




    /* Check for 404 (file not found). */
//    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//    // Check the HTTP Status code
//    switch ($httpCode) {
//        case 200:
//            $error_status = "200: Success";
//            return ($data);
//            break;
//        case 404:
//            $error_status = "404: API Not found";
//            break;
//        case 500:
//            $error_status = "500: servers replied with an error.";
//            break;
//        case 502:
//            $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
//            break;
//        case 503:
//            $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
//            break;
//        default:
//            $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
//            break;
//    }
//    curl_close($curl);
//    echo $error_status;
//    die;
}
function fixString($string) {
    return "'".$string."'";
}
function fireBaseNotify($title , $body , $reciver , $topic , $admin , $flag){
    $params=array();
    $params["title"] = fixString($title);
    $params["body"] =  fixString($body);
    $params["reciver"] = $reciver;
    $params["flag"] = $flag;
    $params["admin"] = $admin;
    $params["topic"] = $topic;

    $url="http://poushka.com/api/v1/sendNotification";

    foreach ($params as $key => &$val) {
        if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);

    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:80,
        $errno, $errstr, 30);

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)) $out.= $post_string;

    fwrite($fp, $out);
    fclose($fp);



}
