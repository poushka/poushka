<?php
namespace Src\Inc;

class MCrypt
{
    private $iv = 'm1i1251mnb142jm1';
    private $key = 'son412ucem1cnun1';

//    private $iv = 'fedcba9876543210'; #Same as in JAVA
//    private $key = '0123456789abcdef'; #Same as in JAVA


    function __construct()
    {

    }




    function encrypt2($str) {
       // aes-128-cbc
        $encrypted = openssl_encrypt($str, 'aes-128-cbc', $this->key, 0, $this->iv);
    //    $encrypted = openssl_encrypt($str, 'aes-128-cbc', $this->key, OPENSSL_RAW_DATA, $this->iv);
        return bin2hex($encrypted);
    }

    function decrypt2($code) {
     //aes-128-cbc
        $code = $this->hex2bin($code);
       // $decrypted = openssl_decrypt($code, 'aes-128-cbc', $this->key, OPENSSL_RAW_DATA |  OPENSSL_ZERO_PADDING, $this->iv);
        $decrypted = openssl_decrypt($code, 'aes-128-cbc', $this->key,0, $this->iv);


        return trim($decrypted);
    }




    function encrypt3($str,$apikey) {
        // aes-128-cbc
        $encrypted = openssl_encrypt($str, 'aes-128-cbc', $apikey, 0, $this->iv);
        //    $encrypted = openssl_encrypt($str, 'aes-128-cbc', $this->key, OPENSSL_RAW_DATA, $this->iv);
        return bin2hex($encrypted);
    }

    function decrypt3($code,$apikey) {
        //aes-128-cbc
        $code = $this->hex2bin($code);
        // $decrypted = openssl_decrypt($code, 'aes-128-cbc', $this->key, OPENSSL_RAW_DATA |  OPENSSL_ZERO_PADDING, $this->iv);
        $decrypted = openssl_decrypt($code, 'aes-128-cbc', $apikey,0, $this->iv);
        return trim($decrypted);
    }




    protected function hex2bin($hexdata) {
        $bindata = '';

        for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
        }

        return $bindata;
    }

}