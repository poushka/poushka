<?php

namespace Src\Inc;

use DOMDocument;

class User_Handler
{
    private $conn;

    function __construct($con)
    {
        $this->conn = $con;
    }

    function __destruct()
    {
        mysqli_close($this->conn);
        $this->conn = null;
    }


    public function getInfo()
    {
        $test = "test";
        $st = $this->conn->prepare("SELECT   version_code  , version_name , s_update,update_message, des ,title, dl_link from info order by info_id DESC LIMIT  1");
        $st->execute();
        /*     $result = $st->get_result();
             $info = $result->fetch_assoc();*/

        $st->store_result();
        $info = $this->fetchAssocStatement($st);

        $st->close();
        return $info;


    }


    public function getFaq()
    {

        $response=array();

        $st = $this->conn->prepare("SELECT   faq_id  , title , description from faq ");
        $st->execute();
        $result =  $st->get_result();

        while ($single = $result->fetch_assoc()) {
          array_push($response,$single);
        }
       return $response;

    }


    public function getSc()
    {

        $response = array();
        $response["categories"]=array();
        $response["socials"]=array();

        $st = $this->conn->prepare("SELECT cat_id , name ,en_name,  icon   FROM category WHERE active > 0 GROUP BY sort");
        $st->execute();
        $result = $st->get_result();
        while ($single_cat = $result->fetch_assoc()) {
            array_push($response["categories"], $single_cat);
        }

        $st = $this->conn->prepare("select social_id ,p_name, e_name, uri ,package,web
                ,description ,pic, icon ,active,filtered, prefix,prefix_p,member_prefix, created_at from sociall where active = 1;");

        $st->execute();
        $result = $st->get_result();
            while ($single_social = $result->fetch_assoc()) {
                array_push($response['socials'], $single_social);
            }
         $st->close();
          return $response;

    }


    public function support($object, $user_id)
    {
        $date_folder = date("y-m-d");
        $directory = __DIR__ . "/../uploads/support/" . $date_folder . "/";
        $pic_name_to_store = null;
        if (isset($_FILES["pic"])) {
            if (!is_dir($directory)) {
                mkdir($directory);
            }
            $pic_info = pathinfo($_FILES['pic']['name']);
            $extension = $pic_info['extension'];
            $pic_name = rand(10, 100) . substr(abs(crc32(uniqid())), 0, 7) . '.' . $extension;
            $pic_name_to_store = $date_folder . "/" . $pic_name;
            $pic_path = $directory . $pic_name;
            if (!move_uploaded_file($_FILES["pic"]["tmp_name"], $pic_path)) {
                return getResponse("خطا در آپلود تصویر . لطفا دوباره امتحان کنید", 400);
            }
        }
        $st = $this->conn->prepare("INSERT INTO support (st_id, user_id , title , message, mobile,  pic ) values (?,?,?,?,?,?)");
        $st->bind_param("iissss", $object["st_id"], $user_id, $object["title"], $object["message"], $object["mobile"], $pic_name_to_store);
        if ($st->execute()) {
            return getResponse(" درخواست شما با موفقیت ثبت و پیگیری خواهد شد. ممنون از همکاری شما ", 201);
        } else {
            try {
                @   unlink($pic_path);

            } catch (\Exception $e) {

            }
            return getResponse("خطا در ثبت درخواست پشتیبانی . لطفا دوباره امتحان کنید ", 201);

        }


    }

    public function getSupportTypes()
    {

        $response = array();
        $st = $this->conn->prepare("SELECT st_id,text from support_types");
        $st->execute();
        $result = $st->get_result();
        while ($single = $result->fetch_assoc()) {
            array_push($response, $single);
        }
        return $response;

    }

    public function getUserIdByApikey($apikey){
        $st = $this->conn->prepare("select   user_id from users where apikey = ?  ");
        $st->bind_param("s" , $apikey);
        $st->execute();
        $st->bind_result($user_id);
        $st->store_result();
        if ($st->num_rows > 0 ) {
            $st->fetch();
             return $user_id;
            }

        else {
            return -1;
        }
    }


    public function createUser($mobile, $otp)
    {


        if ($this->userExists_Active($mobile)) {
            return getResponse("این شماره قبلا ثبت شده است", 400);

        } else {

            $st = $this->conn->prepare("SELECT created_at from sms_codes where mobile like ? ");
            $st->bind_param("s", $mobile);
            $st->bind_result($created_at);
            $st->execute();
            $st->store_result();
            if ($st->num_rows > 0) {
                $st->fetch();
                $old = new \DateTime($created_at);
                $now = new \DateTime(date("Y-m-d H:i:s"));
                $diff = $now->getTimestamp() - $old->getTimestamp(); // $diff be saniast
                if ($diff < 30) {
                    $accept = 30 - $diff;
                    return getResponse("در خواست ارسال پیامک هر 30 ثانیه یک بار قابل اجرا میباشد . زمان باقی مانده تا درخواست بعدی  " . $accept . " ثانیه", 401);
                }
            }


            if ($this->userExests($mobile)) {
                $id = $this->getUserIdByMobile($mobile);
                if ($this->create_sms_code($id, $otp, $mobile)) {
                    sendVerifySms($mobile, $otp, "verifyp");
                    return getResponse("پیامک حاوی کد ورود ارسال گردید", 201);
                } else {
                    return getResponse("خطا در ارسال پیامک . لطفا دوباره امتحان کنید", 400);
                }
            }

            $apikey = $this->generateApiKey();
            //    $hashed_password = password_hash($password,PASSWORD_DEFAULT);
            $stmt = $this->conn->prepare("INSERT INTO users(mobile,apikey) VALUES (?,?)");

            $stmt->bind_param("ss", $mobile, $apikey);
            $result = $stmt->execute();
            $stmt->close();
            if ($result) {

                $inserted_id = $this->conn->insert_id;
                if ($this->create_sms_code($inserted_id, $otp, $mobile)) {

                    sendVerifySms($mobile, $otp, "verifyp");
                    return getResponse("پیامک حاوی کد ورود ارسال گردید", 201);

                } else {
                    return getResponse("خطا در ارسال پیامک . لطفا دوباره امتحان کنید", 400);
                }

            } else {
                return getResponse("خطا در ساخت کاربر ! دوباره تلاش کنید", 500);
            }


        }

    }

    public function create_sms_code($user_id, $otp, $mobile)
    {
        // delete the old otp if exists
        $stmt = $this->conn->prepare("DELETE FROM sms_codes WHERE user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $stmt = $this->conn->prepare("INSERT INTO sms_codes (user_id,code,mobile)  VALUES (?,?,?)");

        $stmt->bind_param("iss", $user_id, $otp, $mobile);
        $result = $stmt->execute();
        $stmt->close();
        return $result;


    }

    public function activeUser($mobile, $otp, $password, $moaref)
    {

        if ($this->userExists_Active($mobile)) {
            return getResponse("کاربر مورد نظر قبلا فعال گردیده است", 400);
        } else {
            // in yani neshon mide ke to marhale 2e
            if ($password == null) {

                $st = $this->conn->prepare("SELECT  s.created_at  FROM users u JOIN sms_codes s ON u.user_id = s.user_id WHERE s.mobile = ? AND s.code = ?");
                $st->bind_param("ss", $mobile, $otp);
                $st->execute();
                $st->bind_result($created_at);
                $st->store_result();

                if ($st->num_rows > 0) {
                    $st->fetch();
                    $st->close();
                    $old = new \DateTime($created_at);
                    $now = new \DateTime(date("Y-m-d H:i:s"));
                    $diff = $now->getTimestamp() - $old->getTimestamp();

                    if ($diff >= 6000) {
                        return getResponse("کد ارسال شده منقضی شده است", 403);

                    } else {
                        return getResponse("رمز عبور خود را انتخاب نمایید", 200);
                    }


                } else {
                    return getResponse("کد ارسال شده صحیح نمیباشد", 400);
                }

            } else {
                $st = $this->conn->prepare("SELECT u.user_id ,s.created_at  FROM users u JOIN sms_codes s ON u.user_id = s.user_id WHERE s.mobile = ? AND s.code = ?");
                $st->bind_param("ss", $mobile, $otp);
                $st->execute();
                $st->bind_result($user_id, $created_at);
                $st->store_result();

                if ($st->num_rows > 0) {
                    if ($moaref != null) {
                        if (!$this->moarefExists($moaref)) {
                            $st->close();
                            return getResponse("کد معرف وارد شده صحیح نمیباشد", 400);
                        }

                    }
                    $st->fetch();
                    $st->close();
                    $old = new \DateTime($created_at);
                    $now = new \DateTime(date("Y-m-d H:i:s"));
                    $diff = $now->getTimestamp() - $old->getTimestamp();

                    if ($diff >= 6000) {
                        return getResponse("کد ارسال شده منقضی شده است", 403);

                    } else {
                        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
                        $this->conn->begin_transaction();
                        $commit = true;
                        try {
                            $st = $this->conn->prepare("UPDATE users  SET status = 1 , password = ? WHERE user_id LIKE ?");
                            $st->bind_param("si", $hashed_password, $user_id);
                            $st->execute();
                            if ($st->affected_rows < 1) {
                                $commit = false;
                            }

                            $st = $this->conn->prepare("UPDATE sms_codes  SET status = 1 WHERE user_id LIKE ?");
                            $st->bind_param("i", $user_id);
                            $st->execute();

                            if ($st->affected_rows < 1) {
                                $commit = false;
                            }

                            $st = $this->conn->prepare("INSERT INTO user_info (user_id,mobile) VALUES (?,?)");
                            $st->bind_param("is", $user_id, $mobile);
                            $st->execute();

                            if ($st->affected_rows < 1) {
                                $commit = false;
                            }


                            if ($moaref != null) {
                                $st = $this->conn->prepare("INSERT INTO user_code (user_code,user_id) VALUES (?,?)");
                                $st->bind_param("ii", $moaref, $user_id);
                                $st->execute();
                                if ($st->affected_rows < 1) {
                                    $commit = false;
                                }

                                $st = $this->conn->prepare("UPDATE users SET inventory = inventory + 20 WHERE user_id = (SELECT user_id FROM
                                                             user_info WHERE user_info.user_code = ?)");
                                $st->bind_param("i", $moaref);
                                $st->execute();
                                if ($st->affected_rows < 1) {
                                    $commit = false;
                                }


                            }
                            if (!$commit) {
                                $st->close();
                                $this->conn->rollBack();
                                return getResponse("خطا در ساخت کاربر . لطفا دوباره تلاش کنید", 500);
                            }

                            $this->conn->commit();

                        } catch (\PDOException $exception) {
                            $this->conn->rollBack();
                            $commit = false;


                        }
                        if ($commit) {

                            $user=$this->getUserinfoByMobile($mobile);
                            $response = array();
                            $response[MESSAGE] = "ثبت نام با موفقیت انجام گردید";
                            $response[STATUS] = 201;
                            $response['user'] = $user;
                            return $response;

                        } else {
                            return getResponse("خطا در ساخت کاربر . لطفا دوباره تلاش کنید", 500);
                        }

                    }


                } else {
                    return getResponse("کد ارسال شده صحیح نمیباشد", 400);
                }
            }


        }


    }

    public function loginUser($mobile, $password)
    {

        if (!$this->userExists_Active($mobile, $password)) {
            return getResponse("شمار موبایل وارد شده یا رمز عبور صحیح نمی باشد", 401);
        } else {
            $st = $this->conn->prepare("SELECT password  FROM users WHERE mobile = ?");

            $st->bind_param("s", $mobile);

            $st->execute();

            $st->bind_result($password_hash);

            $st->store_result();
            $st->fetch();

            if (password_verify($password, $password_hash)) {
                $user = $this->getUserinfoByMobile($mobile);
                $response = array();
                $response['user'] = $user;
                $response[STATUS] = 200;
                return $response;

            } else {
                $st->close();
                return getResponse("شماره موبایل وارد شده یا رمز عبور صحیح نمیباشد نمیباشد", 401);

            }

        }

    }

    public function reqReset($mobile, $otp)
    {

        if (!$this->userExists_Active($mobile)) {
            return getResponse("شماره وارد شده موجود نمیباشد ", 401);
        } else {
            /// aval check mikonim to database hast ya na age nabood ye jadid misazim age bood check mikoim bebinim az darkhaste qable 30 sanie gozashte ya na
            // age na ke behesh payam midim ke vaise age are update mikonim
            $st = $this->conn->prepare("SELECT updated_at from reset_request where mobile like ? ");
            $st->bind_param("s", $mobile);
            $st->bind_result($updated_at);
            $st->execute();
            $st->store_result();
            if ($st->num_rows > 0) {
                $st->fetch();
                $old = new \DateTime($updated_at);
                $now = new \DateTime(date("Y-m-d H:i:s"));
                $diff = $now->getTimestamp() - $old->getTimestamp(); // $diff be saniast
                if ($diff > 30) {
                    $st = $this->conn->prepare("update reset_request set code = ? where mobile like ?");
                    $st->bind_param("ss", $otp, $mobile);
                    $st->execute();
                    if ($st->affected_rows > 0) {
                        $st->close();
                        sendVerifySms($mobile, $otp, "forgetp");
                        return getResponse("رمز بازیابی به شماره " . $mobile . " ارسال گردید", 201);
                    } else {
                        $st->close();
                        return getResponse("خطا .. لطفا دوباره تلاش کنید", 401);
                    }

                } else {
                    $accept = 30 - $diff;
                    return getResponse("در خواست بازیابی رمز عبور هر 30 ثانیه یک بار قابل اجرا میباشد . زمانی باقی مانده تا درخواست بعدی  " . $accept . " ثانیه", 401);
                }


            } else {
                $st = $this->conn->prepare("INSERT INTO reset_request (mobile,code) VALUES (?,?)");
                $st->bind_param("ss", $mobile, $otp);
                $st->execute();
                if ($st->affected_rows > 0) {
                    $st->close();
                    sendVerifySms($mobile, $otp, "forgetp");
                    return getResponse("رمز بازیابی به شماره " . $mobile . " ارسال گردید", 201);
                } else {
                    $st->close();
                    return getResponse("خطا .. لطفا دوباره تلاش کنید", 401);
                }
            }


        }

    }

    public function resetPassword($mobile, $code, $password)
    {


        if (!$this->userExists_Active($mobile)) {
            return getResponse("شماره موبایل وارد شده صحیح نمیباشد ", 401);
        } else {
            if ($password == null) {

                $st = $this->conn->prepare("SELECT updated_at FROM reset_request WHERE mobile LIKE ? AND code LIKE ?");
                $st->bind_param("ss", $mobile, $code);
                $st->execute();
                $st->bind_result($updated_at);
                $st->store_result();
                if ($st->num_rows > 0) {
                    $st->fetch();
                    $st->close();
                    $old = new \DateTime($updated_at);
                    $now = new \DateTime(date("Y-m-d H:i:s"));
                    $diff = $now->getTimestamp() - $old->getTimestamp();
                    if ($diff >= 6000) {
                        return getResponse("کد ارسال شده منقضی شده است", 403);
                    } else {
                        return getResponse("رمز عبور جدید خود را وارد نمایید", 202);
                    }
                } else {
                    $st->close();
                    return getResponse("کد ارسال شده صحیح نمیباشد", 401);
                }
            } else {
                $st = $this->conn->prepare("SELECT updated_at FROM reset_request WHERE mobile LIKE ? AND code LIKE ?");
                $st->bind_param("ss", $mobile, $code);
                $st->execute();
                $st->bind_result($updated_at);
                $st->store_result();
                if ($st->num_rows > 0) {
                    $st->fetch();
                    $st->close();
                    $old = new \DateTime($updated_at);
                    $now = new \DateTime(date("Y-m-d H:i:s"));
                    $diff = $now->getTimestamp() - $old->getTimestamp();
                    if ($diff >= 6000) {
                        return getResponse("کد ارسال شده منقضی شده است", 403);
                    } else {
                        if (strlen($password) < 6) {
                            return getResponse("رمز عبور باید دارای حداقل 6 کاراکتر باشد", 400);
                        } else {
                            $hashed_password = password_hash($password, PASSWORD_DEFAULT);
                            $st = $this->conn->prepare("UPDATE users SET users.password = ? WHERE mobile = ? ");
                            $st->bind_param("ss", $hashed_password, $mobile);
                            $st->execute();
                            $affected = $st->affected_rows;
                            $st->close();
                            if ($affected > 0) {
                                $user=$this->getUserinfoByMobile($mobile);
                                $response = array();
                                $response[STATUS] = 200;
                                $response['message'] = "به روز رسانی رمز عبور با موفقیت انجام گردید";
                                $response['user'] = $user;
                                return $response;
                            } else {
                                return getResponse("خطا لطفا دوباره تلاش کنید", 500);
                            }
                        }
                    }
                } else {
                    $st->close();
                    return getResponse("کد ارسال شده صحیح نمیباشد", 401);
                }
            }


        }

    }


    private function getUserinfoByMobile($mobile) {
        $mycrypt = new MCrypt();
        $st = $this->conn->prepare("SELECT u.user_id,u.mobile,u.apikey,u.inventory, u.active,u.created_at, i.user_code
                                                          , i.username,i.name,i.email,i.t_id,i.instagram, i.profile,i.thumb,i.bio,i.sex ,i.updated_code,i.read_message FROM users 
                                                         u JOIN user_info i ON u.user_id = i.user_id WHERE u.mobile = ?");
        $st->bind_param("s", $mobile);
        $st->execute();
        $result = $st->get_result();
        $user = $result->fetch_assoc();
        $user["apikey"] = $mycrypt->encrypt2($user["apikey"]);
        $st->close();
        return $user;
    }

    public function updateUser($user_id)
    {
        $st = $this->conn->prepare("UPDATE users SET status = 1 WHERE user_id = ?");
        $st->bind_param("i", $user_id);
        $st->execute();
        $st = $this->conn->prepare("UPDATE sms_codes SET status = 1 WHERE user_id = ?");
        $st->bind_param("i", $user_id);
        $st->execute();
        $st->close();
    }

    public function userExists_Active($mobile)
    {
        $st = $this->conn->prepare("SELECT user_id FROM users WHERE mobile LIKE ? AND status LIKE 1");
        $st->bind_param("s", $mobile);
        $st->execute();
        $st->store_result();
        $num_rows = $st->num_rows;
        $st->close();
        return $num_rows > 0;


    }

    public function userExests($mobile)
    {
        $st = $this->conn->prepare("SELECT user_id FROM users WHERE mobile LIKE ?");
        $st->bind_param("s", $mobile);
        $st->execute();
        $st->store_result();
        $num_rows = $st->num_rows;
        $st->close();
        return $num_rows > 0;

    }

    public function moarefExists($moaref)
    {
        $st = $this->conn->prepare("SELECT user_code FROM user_info WHERE user_code LIKE ?");
        $st->bind_param("i", $moaref);
        $st->execute();
        $st->store_result();
        $num_rows = $st->num_rows;
        $st->close();
        return $num_rows > 0;
    }

    public function getUserIdByMobile($mobile)
    {
        $st = $this->conn->prepare("SELECT user_id FROM users WHERE mobile LIKE ? ");
        $st->bind_param("s", $mobile);
        $st->execute();
        $st->bind_result($user_id);
        $st->store_result();
        $st->fetch();
        $st->close();
        return $user_id;
    }

    public function updateUserinfo($object, $hastImage, $user_id)
    {
        $date_folder = date("y-m-d");
        $directory = __DIR__ . '/../uploads/profile/';
        $updated_code = 1;  // flagi ke to mysql moshakhas mikone update shode
        $hasUsernameChanged = $object['huc'];  // yani miam check mikonim bebinim ke user nami ke ferestade taqir karde ya na
        if ($hastImage) {
            if (!is_dir($directory . $date_folder . "/")) {
                mkdir($directory . $date_folder . "/");
            }
            $new_width = "250";
            $new_height = "250";
            $quality = 90;
            $pic_info = pathinfo($_FILES['pic']['name']);
            $extension = $pic_info['extension'];
            $pic_name = rand(10, 100) . substr(abs(crc32(uniqid())), 0, 7) . '.' . $extension;
            $pic_name_to_store = $date_folder . '/pic_' . $pic_name;
            $thumb_name_to_store = $date_folder . '/thumb_' . $pic_name;
            $pic_path = $directory . $pic_name_to_store;
            $thumb_path = $directory . $thumb_name_to_store;
            $filetype = $_FILES['pic']['type'];

            list($width_orig, $height_orig) = getimagesize($_FILES['pic']['tmp_name']);
            $ratio_orig = $width_orig / $height_orig;
            if ($new_width / $new_height > $ratio_orig) {
                $new_width = $new_height * $ratio_orig;
            } else {
                $new_height = $new_width / $ratio_orig;
            }


            if ($filetype == "image/jpeg" || $filetype == "image/jpg") {
                $imagecreate = "imagecreatefromjpeg";
                $imageformat = "imagejpeg";
            }
     else if ($filetype == "image/png") {
            $imagecreate = "imagecreatefrompng";
            $imageformat = "imagepng";
              $quality = 9;
                 }

            else if ($filetype == "image/gif") {
                $imagecreate = "imagecreatefromgif";
                $imageformat = "imagegif";
            } else {
                $imagecreate = "imagecreatefromjpeg";
                $imageformat = "imagejpeg";
            }


            $image_new = imagecreatetruecolor($new_width, $new_height);
            $uploadedfile = $_FILES['pic']['tmp_name'];
            $image = $imagecreate($uploadedfile);
            // vase inke age png bood back groundesh siah nashe
            if ($extension == "gif" or $extension == "png") {
                imagecolortransparent($image_new, imagecolorallocatealpha($image_new, 0, 0, 0, 127));
                imagealphablending($image_new, false);
                imagesavealpha($image_new, true);
            }
            imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            $imageformat($image_new, $thumb_path, $quality);
            imagedestroy($image_new);
            $imageStored = move_uploaded_file($uploadedfile, $pic_path);
        }
        $st = $this->conn->prepare("SELECT profile , thumb FROM user_info WHERE user_id LIKE ? ");
        $st->bind_param("i", $user_id);
        $st->bind_result($pr_profile, $pr_thumb);
        $st->execute();
        $st->store_result();
        $st->fetch();
        // check mishe ke age image dare va username ro taqir dade ino bere
        if ($hastImage) {
            if ($hasUsernameChanged) {
                //age user name tekrari bood miaim o aksai ke zakhire kardimo pak mikonim
                if ($this->usernameExists($object['username'])) {
                    $st->close();
                    try {
                        @     unlink($pic_path);
                        @     unlink($thumb_path);
                    } catch (\Exception $e) {
                    }
                    return getResponse("این نام کابری قبلا انتخاب شده است لطفا نام کاربری دیگری انتخاب نمایید", 409);
                }
            }
            if ($imageStored) {
                $mcrypt = new MCrypt();
                $st = $this->conn->prepare('UPDATE user_info SET username = ? , name =? , email = ? , t_id = ?,instagram = ? ,  profile = ? , thumb = ? , bio = ? , sex = ?  , updated_code = ? 
                                        WHERE user_id LIKE ? ');
                $st->bind_param("ssssssssiii", $object['username'], $object['name'], $object['email'],
                    $object['t_id'], $object['instagram'], $pic_name_to_store, $thumb_name_to_store, $object['bio'],
                    $object['sex'], $updated_code, $user_id);
                $st->execute();
                if ($this->conn->affected_rows > 0) {
                    // akse qablie profile ro pak mikonim
                    try {
                        @     unlink($directory . $pr_profile);
                        @     unlink($directory . $pr_thumb);
                    } catch (\Exception $e) {
                    }
                    $st = $this->conn->prepare("SELECT u.user_id,u.mobile,u.apikey,u.inventory, u.active,u.created_at, i.user_code , i.username,i.name,i.email,i.t_id,i.instagram,
                                                 i.profile,i.thumb,
                                              i.bio,i.sex ,i.updated_code ,i.read_message FROM users u JOIN user_info i ON u.user_id = i.user_id WHERE u.user_id = ?");
                    $st->bind_param("i", $user_id);
                    $st->execute();
                    $result = $st->get_result();
                    $response = array();
                    $user = $result->fetch_assoc();
                    $user["apikey"] = $mcrypt->encrypt2($user["apikey"]);
                    $response['userinfo'] = $user;
                    $response[STATUS] = 200;
                    $st->close();
                    fireBaseNotify("پروفایل", "یک پروفایل به روز رسانی شد", "confrim", 1, 1, 1);
                    return $response;


                } else {
                    try {
                        @    unlink($pic_path);
                        @     unlink($thumb_path);
                    } catch (\Exception $e) {
                    }
                    return getResponse("خطا در به روز رسانی ! دوباره تلاش کنید ", 500);
                }

            } else {
                return getResponse("خطا در اپلود عکس پروفایل ", 500);
            }
            // image nadare o faqat etelat update mishe
        } else {
            $mcrypt = new MCrypt();
            if ($hasUsernameChanged) {
                if ($this->usernameExists($object['username'])) {
                    $st->close();
                    return getResponse("این نام کابری قبلا انتخاب شده است لطفا نام کاربری دیگری انتخاب نمایید", 409);
                }
            }
            $st = $this->conn->prepare('UPDATE user_info SET username = ? , name =? , email = ? ,t_id = ? ,instagram = ? ,   bio = ? , sex = ?  , updated_code = ? 
                                       WHERE user_id LIKE ? ');
            $st->bind_param("ssssssiii", $object['username'], $object['name'], $object['email'],
                $object['t_id'], $object['instagram'], $object['bio'],
                $object['sex'], $updated_code, $user_id);
            $st->execute();
            if ($this->conn->affected_rows > 0) {
                $st = $this->conn->prepare("SELECT u.user_id,u.mobile,u.apikey,u.inventory, u.active,u.created_at, i.user_code , i.username,i.name,i.email,i.t_id,i.instagram, 
                                               i.profile,i.thumb,
                                              i.bio,i.sex ,i.updated_code ,i.read_message FROM users u JOIN user_info i ON u.user_id = i.user_id WHERE u.user_id = ?");
                $st->bind_param("i", $user_id);
                $st->execute();
                $result = $st->get_result();
                $response = array();
                $user = $result->fetch_assoc();
                $user["apikey"] = $mcrypt->encrypt2($user["apikey"]);
                $response['userinfo'] = $user;
                $response[STATUS] = 200;
                $st->close();
                fireBaseNotify("پروفایل", "یک پروفایل به روز رسانی شد", "confrim", 1, 1, 1);
                return $response;
            } else {
                return getResponse("خطا در به روز رسانی ! دوباره تلاش کنید ", 500);
            }
        }


    } //!!

    public function updateFcmToken($user_id, $fcm_token)
    {
        $st = $this->conn->prepare('update users set fcm_token = ? where user_id = ?');
        $st->bind_param("si", $fcm_token, $user_id);
        $st->execute();
        if ($this->conn->affected_rows > 0) {
            return getResponse("fcm token updated", 201);
        } else {
            return getResponse("failed fcm token update", 400);
        }
    }


    //vase taiine vaziat inke aya userinfo taidesh anjam shode va vaziatesh chie
    public function getUserUpdatedCode($user_id)
    {
        $st = $this->conn->prepare("SELECT updated_code FROM user_info WHERE user_id LIKE ?");
        $st->bind_param("i", $user_id);
        $st->bind_result($updated_code);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        return $updated_code;
    }

    public function getUser($user_id)
    {
        $mcrypt = new MCrypt();
        $st = $this->conn->prepare("SELECT u.user_id,u.mobile,u.apikey,u.inventory, u.active,u.created_at, i.user_code , i.username,i.name,i.email,i.t_id, 
                                                i.profile,i.thumb,
                                              i.bio,i.sex ,i.updated_code ,i.read_message FROM users u JOIN user_info i ON u.user_id = i.user_id WHERE u.user_id = ?");
        $st->bind_param("i", $user_id);
        $st->execute();
        $result = $st->get_result();
        $st->close();
        $user = $result->fetch_assoc();
        $user["apikey"] = $mcrypt->encrypt2($user["apikey"]);
        return $user;
    }

    public function getUserCodeById($user_id)
    {
        $st = $this->conn->prepare("SELECT user_code FROM user_info WHERE user_id = ?;");
        $st->bind_param("i", $user_id);
        $st->bind_result($user_code);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        return $user_code;
    }

    public function usernameExists($username)
    {
        $st = $this->conn->prepare("SELECT user_code FROM user_info WHERE username LIKE ?");
        $st->bind_param("s", $username);
        $st->execute();
        $st->store_result();
        $num_rows = $st->num_rows;
        $st->close();
        return $num_rows > 0;


    }

    public function getMessages($user_id, $limit, $last_id)
    {
        if ($last_id == 0) {
            $this->updateReadCount($user_id);
            $s = $this->conn->prepare("SELECT max(message_id) AS last FROM message WHERE reciver_id = ? ");
            $s->bind_param("i", $user_id);
            $s->bind_result($last);
            $s->execute();
            $s->store_result();
            $s->fetch();
            // agar hich satri yaft nashe num_row 1 barmigardone ama ba meqdare null . pas agar null bood yani hich pagi ke actives sefr bashe nadarim
            if (is_null($last)) {
                $s->close();
                $response[STATUS] = 200;
                $response['messages'] = array();
                return $response;
            }

            $last_id = $last + 1;

        }

        $s = $this->conn->prepare("SELECT m.message_id, m.sender_id,m.message,m.title,m.state,m.created_at,u.active,  
                                 REPLACE(i.mobile,SUBSTRING(i.mobile,5,4),'****') AS mobile,i.username,i.thumb FROM message m
                                JOIN users u ON m.sender_id = u.user_id
                                JOIN user_info i ON m.sender_id = i.user_id WHERE m.reciver_id = ? AND m.message_id < ? ORDER BY m.message_id DESC LIMIT 0 , ? ");


        $s->bind_param("iii", $user_id, $last_id, $limit);
        $s->execute();
        $result = $s->get_result();
        $response[STATUS] = 200;
        $response['messages'] = array();
        while ($single = $result->fetch_assoc()) {
            $single["created_at"]=convertTime($single["created_at"]);
            array_push($response['messages'], $single);
        }
        $s->close();
        return $response;


    }

    public function getMessageCount($user_id)
    {


        $s = $this->conn->prepare("SELECT count(*) AS count FROM message WHERE reciver_id LIKE ? ");
        $s->bind_param("i", $user_id);
        $s->bind_result($count_message);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $s->close();
        return $count_message;

    }


    public function updateReadCount($user_id)
    {
        $s = $this->conn->prepare("UPDATE user_info SET read_message = (select count(*) from message where reciver_id = ?) WHERE user_id = ?");
        $s->bind_param("ii", $user_id, $user_id);
        $s->execute();
        if ($this->conn->affected_rows > 0) {
            $s->close();
            return getResponse("به روز رسانی انجام شد", 201);
        } else {
            $s->close();
            return getResponse("خطا در به روز رسانی", 401);
        }
    }

    public function sendMessage($sender_id, $reciver_id, $title, $message, $state)
    {

        $s = $this->conn->prepare("INSERT INTO message (sender_id , reciver_id , title , message , state) VALUES (?,?,?,?,?)");
        $s->bind_param("iissi", $sender_id, $reciver_id, $title, $message, $state);
        if ($s->execute()) {
            $s->close();

            return getResponse("پیام با موفقیت ارسال گردید", 201);
        } else {
            $s->close();

            return getResponse("خطا در ارسال پیام دوباره تلاش نمایید", 401);
        }

    }

    private function generateApiKey()

    {
        return md5(uniqid(rand(), true));
    }

    //////////////////////////////////////////////////////************UserPages**************\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//    public function checkPageExists($social_id , $id) {
//        $st = $this->conn->prepare("select active from page where social_id = ? and id = ? ");
//        $st->bind_param("is",$social_id,$id);
//        $st->bind_result($active);
//        $st->execute();
//        $st->store_result();
//        if ($st->num_rows > 0 ) {
//            $st->fetch();
//            if ($active==0) {
//
//            }
//
//        }
//    }

    public function getMemberCount($social_id, $chanel_id)
    {

        if ($social_id == 1) {

            //@   $raw = file_get_contents('https://www.instagram.com/' . $chanel_id);  // @ vase ignore kardane error\
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, "https://www.instagram.com/".$chanel_id."/?__a=1");
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Cookie: ig_did=46DE3FC5-F800-41DF-ADA0-2FC14B184357; mid=X35kEAALAAHWJ5oOonPk8mL5bv-b; ig_nrcb=1; csrftoken=WRlc4gFf9sCqpnN6gSv6xchchwkRdr91; ds_user_id=1837672676; sessionid=1837672676%3AjobXR7eXnXjdu3%3A28; shbid=16356; ig_cb=2; shbts=1603888449.139517; rur=FTW; urlgen="{\"46.224.141.169\": 56402}:1kYedA:hUOxh7H_S0X43sWOWcIkQfHmMoU"'

            ));
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip');  // sorate load kardan ro mibare bala
            $response = json_decode(curl_exec($ch),true);

            if ($response==null) {
                return -1;
            }else {
                $count = $response["graphql"]["user"]["edge_followed_by"]["count"];
                return $count;
            }
        }



        else if ($social_id == 2) {

            $post = [
                'id' => $chanel_id,
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, 'https://poushka.com/api/v1/teleEstelam');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip');  // sorate load kardan ro mibare bala
            $response = json_decode(curl_exec($ch), true);
            curl_close($ch);
            if ($response == null) {
                return null;
            } else {
                if ($response['ok']) {
                    return $response['result'];
                } else {
                    return -1;

                }
            }

        } else if ($social_id == 3) {
            $url = 'https://www.aparat.com/etc/api/profile/username/' . $chanel_id;
            $response = $this->Inquiry_for_memberCount($url);

            if ($response == null) {
                return null;
            } else {

                if (isset($response['profile']['follower_cnt'])) {

                    return $response['profile']['follower_cnt'];
                } else {
                    return -1;
                }

            }
        } else if ($social_id == 4) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, 'https://sapp.ir/' . $chanel_id);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip');  // sorate load kardan ro mibare bala

            $raw = curl_exec($ch);
            curl_close($ch);


            $dom = new DOMDocument;
            @  $dom->loadHTML($raw);
            if ($h4s = $dom->getElementsByTagName('h4')->length == 0) {
                return -1;
            } else {
                $h4s = $dom->getElementsByTagName('h4');
                $h4 = $h4s[0];
                $count_string = $h4->nodeValue;
                $count = preg_replace("/[^-0-9]+/", '', $count_string);

                if (empty($count)) {

                    return -1;
                } else {
                    return $count;
                }

            }

        }else if ($social_id==5) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, 'https://rubika.ir/' . $chanel_id);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip');  // sorate load kardan ro mibare bala

            $raw = curl_exec($ch);
            curl_close($ch);

            $dom = new DOMDocument;
            @  $dom->loadHTML($raw);
            $innerHTML = '';
            $finder = new \DOMXPath($dom);
            $classname="number-persian";
            $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
            if (count($nodes)==0)  {
                return -1;
            };


           foreach ($nodes as $node) {
              return  $node->textContent;
           }


        }else if ($social_id==6) {
            //as we cant directly get youtube chanel count by username first we get its id from username so get count by id
            $key="AIzaSyCOQP0teVXfFgsRfRqhCvh6p1ZItUSCvsY";
            $temp = "https://www.googleapis.com/youtube/v3/search?part=id&maxResults=1&q=$chanel_id&type=channel&key=$key";
            $response_temp = $this->Inquiry_for_memberCount($temp);


            if (count($response_temp["items"])==0) {
                return -1;
            }else {
                $chanel_id = $response_temp["items"][0]["id"]["channelId"];
                $url = "https://www.googleapis.com/youtube/v3/channels?part=statistics&id=".$chanel_id."&key=$key";
                $response = $this->Inquiry_for_memberCount($url);
                if ($response["items"][0]["statistics"]["hiddenSubscriberCount"]) {
                    header("HTTP/1.1 404 Not Found");
                    die(json_encode(getResponse("کانال مورد نظر تعداد مشترکان را مخفی کرده و استعلام مقدور نمیباشد",404)));
                }else {
                    return $response["items"][0]["statistics"]["subscriberCount"];
                }

            }

        }
    }

    public function Inquiry_for_memberCount($url)
    {

        $curl = curl_init($url);

        //     $proxy = 's1.yektavip.biz';
        //$proxyauth = 'w4720:6951';
        //   $port = '49718';

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //   curl_setopt($curl, CURLOPT_PROXY, $proxy);
        //  curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_HTTP_1_0);

        //  curl_setopt($curl, CURLOPT_PROXYPORT, $port);
        //    curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyauth);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array());
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array());
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");

        //  $response= curl_exec($curl);
        $response = json_decode(curl_exec($curl), true);



        //  die(var_dump($response));


        if (curl_errno($curl)) {
            die(curl_error($curl));

            return null;

        } else {

            return $response;
        }

    }

    public function getEstelamNeeds($page_id) {
        $st=$this->conn->prepare("select p.social_id,id,member,s.member_prefix from page p join sociall s on p.social_id = s.social_id where page_id = ?");

        $st->bind_param("i",$page_id);
        $st->execute();
        return $st->get_result()->fetch_assoc();
    }


    function getHttpCode($http_response_header)
    {
        if (is_array($http_response_header)) {
            $parts = explode(' ', $http_response_header[0]);
            if (count($parts) > 1) //HTTP/1.0 <code> <text>
                return intval($parts[1]); //Get code
        }
        return 0;
    }

    public function updateMemberCount($page_id, $member_count)
    {
        $response = array();
        $s = $this->conn->prepare("UPDATE page SET member = ? WHERE page_id = ?");
        $s->bind_param("ii", $member_count, $page_id);
        $s->execute();
        if ($this->conn->affected_rows > 0) {
            $response[MESSAGE] = "به روز رسانی انجام شد";
            $response["member_cnt"] = $member_count;
            $response[STATUS] = 201;
            $s->close();
            return $response;
        } else {
            $s->close();
            return getResponse("خطا در به روز رسانی", 400);
        }
    }

    // yani estelam gereftan :)


    public function screenUpload()
    {

        $new_width = "350";
        $new_height = "350";
        $quality = 95;
        $date_folder = date("y-m-d");
        $directory = __DIR__ . '/../uploads/chanel_screen/' . $date_folder . '/';
        $directory_root = __DIR__ . '/../uploads/chanel_screen/';
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        $pic_info = pathinfo($_FILES['pic']['name']);
        $extension = $pic_info['extension'];
        $pic_name = rand(10, 100) . substr(abs(crc32(uniqid())), 0, 6) . '.' . $extension;

        $pic_name_to_store = $date_folder . '/pic_' . $pic_name;
        $thumb_name_to_store = $date_folder . '/thumb_' . $pic_name;

        $pic_path = $directory_root . $pic_name_to_store;
        $thumb_path = $directory_root . $thumb_name_to_store;
        $filetype = $_FILES['pic']['type'];
        list($width_orig, $height_orig) = getimagesize($_FILES['pic']['tmp_name']);
        $ratio_orig = $width_orig / $height_orig;
        if ($new_width / $new_height > $ratio_orig) {
            $new_width = $new_height * $ratio_orig;
        } else {
            $new_height = $new_width / $ratio_orig;
        }
        if ($filetype == "image/jpeg" || $filetype == "image/jpg") {
            $imagecreate = "imagecreatefromjpeg";
            $imageformat = "imagejpeg";
        } else if ($filetype == "image/png") {
            $imagecreate = "imagecreatefrompng";
            $imageformat = "imagepng";
            $quality = 9;
        } else if ($filetype == "image/gif") {
            $imagecreate = "imagecreatefromgif";
            $imageformat = "imagegif";
        } else {
            $imagecreate = "imagecreatefromjpeg";
            $imageformat = "imagejpeg";
        }


        $image_new = imagecreatetruecolor($new_width, $new_height);
        $uploadedfile = $_FILES['pic']['tmp_name'];
        $image = $imagecreate($uploadedfile);
        // vase inke age png bood back groundesh siah nashe
        if ($extension == "gif" or $extension == "png") {
            imagecolortransparent($image_new, imagecolorallocatealpha($image_new, 0, 0, 0, 127));
            imagealphablending($image_new, false);
            imagesavealpha($image_new, true);
        }
        imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
        $imageformat($image_new, $thumb_path, $quality);
        imagedestroy($image_new);

        if (move_uploaded_file($uploadedfile, $pic_path)) {

            $st = $this->conn->prepare("insert into page_screen (name,thumb_name) values (?,?)");
            $st->bind_param("ss", $pic_name_to_store, $thumb_name_to_store);
            if ($st->execute()) {
                $st->close();
                return getResponse($this->conn->insert_id, 201);

            } else {
                try {
                    @   unlink($pic_path);
                    @  unlink($thumb_path);
                    } catch (\Exception $e) {
                }
                return getResponse("خطا در ذخیره تصویر!", 400);
            }


        } else {
            return getResponse("خطا در ثبت تصویر ! لطفا دوباره تلاش کنید", 500);
        }

    }


    public function makePage($social_id, $user_id, $membercount, $object, $aparat, $tags, $screens)
    {
        $st = $this->conn->prepare("SELECT active , page_id FROM page WHERE social_id = ? AND id = ?");
        $st->bind_param("is", $social_id, $object['id']);
        $st->bind_result($active, $page_id);
        $st->execute();
        $st->store_result();
        if ($st->num_rows > 0) {
            $st->fetch();
            if ($active == 0) {
                return getResponse($object["prefix"] . " مورد نظر قبلا ثبت گردیده و در انتظار تایید میباشد", 409);
            } else if ($active == 1 || $active > 2) {   // yani ya qablan sabt shode ya dar entezare javabe berooz resanie
                return getResponse($object["prefix"] . " مورد نظر قبلا ثبت گردیده است", 409);

                // yani ya qablan tavasote ma sabt shode ya inbke qablan sabt shode vali taiid nashode pas bayad update she
            } else if ($active == -2) {
                return getResponse($object["prefix"] . " مورد نظر به حالت تعلیق درآمده و قابل ثبت نیست . برای پیگری از بخش حساب کاربری با پشتیبانی تماس حاصل نمایید", 409);
            } else if ($active == 2 || $active == -1) {
                $result = $this->createPage($social_id, $user_id, $membercount, $object, $page_id, 1, $aparat, $tags, $screens);
                return $result;
            }

        } else {

            $result = $this->createPage($social_id, $user_id, $membercount, $object, 0, 0, $aparat, $tags, $screens);
            return $result;
        }
    }

    //situiation  ; 0 vaghti ke qablan ye hamchin chaneli nadarim  ke  bayad insert she
    //             ; 1 vaghti ke in chanel qablan tavasote ma sabtshode ya qblan sabt shode vali taiid nashode ke bayad update she
    //             ; va halate akhar ke bala check mishe vaghtie ke chanel sabt shode va active

    public function createPage($social_id, $user_id, $membercount, $object, $page_id, $situation, $aparat, $tags, $screens)
    {
        $prefix = $object['prefix'];
        $new_width = "275";
        $new_height = "275";
        $quality = 90;
        $date_folder = date("y-m-d");
        $directory = __DIR__ . '/../uploads/chanel_pic/' . $date_folder . '/';
        $directory_root = __DIR__ . '/../uploads/chanel_pic/';
        $banner_root = __DIR__ . '/../uploads/banner/';
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        $pic_info = pathinfo($_FILES['pic']['name']);
        $extension = $pic_info['extension'];
        $pic_name = rand(10, 100) . substr(abs(crc32(uniqid())), 0, 6) . '.' . $extension;

        $pic_name_to_store = $date_folder . '/pic_' . $pic_name;
        $thumb_name_to_store = $date_folder . '/thumb_' . $pic_name;
        $pic_path = $directory_root . $pic_name_to_store;
        $thumb_path = $directory_root . $thumb_name_to_store;
        $filetype = $_FILES['pic']['type'];
        list($width_orig, $height_orig) = getimagesize($_FILES['pic']['tmp_name']);
        $ratio_orig = $width_orig / $height_orig;
        if ($new_width / $new_height > $ratio_orig) {
            $new_width = $new_height * $ratio_orig;
        } else {
            $new_height = $new_width / $ratio_orig;
        }
        if ($filetype == "image/jpeg" || $filetype == "image/jpg") {
            $imagecreate = "imagecreatefromjpeg";
            $imageformat = "imagejpeg";
        } else if ($filetype == "image/png") {
            $imagecreate = "imagecreatefrompng";
            $imageformat = "imagepng";
            $quality = 9;
        } else if ($filetype == "image/gif") {
            $imagecreate = "imagecreatefromgif";
            $imageformat = "imagegif";
        } else {
            $imagecreate = "imagecreatefromjpeg";
            $imageformat = "imagejpeg";
        }


        $image_new = imagecreatetruecolor($new_width, $new_height);
        $uploadedfile = $_FILES['pic']['tmp_name'];
        $image = $imagecreate($uploadedfile);
        // vase inke age png bood back groundesh siah nashe
        if ($extension == "gif" or $extension == "png") {
            imagecolortransparent($image_new, imagecolorallocatealpha($image_new, 0, 0, 0, 127));
            imagealphablending($image_new, false);
            imagesavealpha($image_new, true);
        }
        imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
        $imageformat($image_new, $thumb_path, $quality);
        imagedestroy($image_new);

        if (move_uploaded_file($uploadedfile, $pic_path)) {
            // yani inke qablan naboode pas bayad sakhte beshe
            if ($situation == 0) {
                $short_url='';
                while (true) {
                    $short_url = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 6)), 0, 6);
                    $smt=$this->conn->prepare("select short_url from page where short_url like ?");
                    $smt->bind_param("s",$short_url);
                    $smt->execute();
                    $result = $smt->get_result();
                    if ($result->num_rows==0) {
                        break ;
                    }
                }


                $st = $this->conn->prepare("INSERT INTO page (social_id , cat_id , user_id ,short_url, id , name , short_des 
                              , des, pic , thumb , member,aparat) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
                $st->bind_param("iiisssssssis", $social_id, $object['cat_id'], $user_id,$short_url, $object['id'], $object['name']
                    , $object['short_des'], $object['des'], $pic_name_to_store, $thumb_name_to_store, $membercount, $aparat
                );
                $st->execute();
                if ($this->conn->affected_rows > 0) {
                    $inserted_id = $this->conn->insert_id;
                    $st->close();
                    $stmt = $this->conn->prepare("insert into tag_page (tag_id,page_id) values (?,?)");
                    foreach ($tags as $tag) {
                        $stmt->bind_param("ii", $tag, $inserted_id);
                        $stmt->execute();
                    }


                    $stmt = $this->conn->prepare("update page_screen set page_id = ? where ps_id like ?");
                    foreach ($screens as $screen) {
                        $stmt->bind_param("ii", $inserted_id, $screen);
                        $stmt->execute();
                    }
                    $stmt->close();

                     fireBaseNotify("صفحه جدید","یک صفحه جدید ثبت شد","confrim",1,1,1);
                    return getResponse($prefix . " شما ثبت و بعد از تایید به نمایش در خواهد آمد", 201);

                } else {
                    try {
                        @   unlink($pic_path);
                        @  unlink($thumb_path);
                    } catch (\Exception $e) {
                    }

                    return getResponse("خطا در ساخت " . $prefix . "  . لطفا دوباره تلاش کنید", 400);
                }


            } else if ($situation == 1) {
                $active = 0;
                //qable inke update konim miam o eteleate ask o thumbe qabli ro migirm ke bade update on qabliaro pak konim
                $st = $this->conn->prepare("SELECT pic , thumb, banner  FROM page WHERE page_id = ?");
                $st->bind_param("i", $page_id);
                $st->bind_result($pic, $thumb, $banner);
                $st->execute();
                $st->store_result();
                $st->fetch();

                $st = $this->conn->prepare("UPDATE page SET cat_id = ? , user_id = ? , name = ? , short_des = ? , des = ? , pic = ? , thumb = ?,
                                             member = ? ,aparat = ?  , active = ?  WHERE page_id = ? ");

                $st->bind_param("iisssssisii", $object['cat_id'], $user_id, $object['name']
                    , $object['short_des'], $object['des'], $pic_name_to_store, $thumb_name_to_store, $membercount, $aparat, $active, $page_id
                );
                $st->execute();
                if ($this->conn->affected_rows < 1) {
                    try {
                        @   unlink($pic_path);
                        @  unlink($thumb_path);

                    } catch (\Exception $e) {
                        return getResponse("خطا در ثبت " . $prefix . " لطفا دوباره تلاش کنید", 400);
                    }


                } else {
                    $st = $this->conn->prepare("update page set banner = null where page_id like ?");
                    $st->bind_param("i", $page_id);
                    $st->execute();

                    $st = $this->conn->prepare("delete from special_r where page_id like ?");
                    $st->bind_param("i", $page_id);
                    $st->execute();

                    try {
                        // akse qabli ke ma zakhire karde boodim pak mishavad
                        @    unlink($directory_root . $pic);
                        @   unlink($directory_root . $thumb);
                        @   unlink($banner_root . $banner);

                    } catch (\Exception $e) {

                    }

                    $stmt = $this->conn->prepare("delete from tag_page where page_id like ? ");
                    $stmt->bind_param('i', $page_id);
                    $stmt->execute();


                    $stmt = $this->conn->prepare("insert into tag_page (tag_id,page_id) values (?,?)");
                    foreach ($tags as $tag) {
                        $stmt->bind_param('ii', $tag, $page_id);
                        $stmt->execute();
                    }
                    $stmt = $this->conn->prepare("update page_screen set page_id = ? where ps_id like ?");
                    foreach ($screens as $screen) {

                        $stmt->bind_param("ii", $page_id, $screen);
                        $stmt->execute();
                    }
                    $stmt->close();

                    fireBaseNotify("صفحه جدید","یک صفحه جدید ثبت شد","confrim",1,1,1);
                    return getResponse($prefix . " شما ثبت شده  و بعد از تایید به نمایش در خواهد آمد", 201);
                }
            }

        } else {
            return getResponse("خطا در ثبت عکس " . $object['prefix'] . " لطفا دوباره تلاش کنید .", 500);
        }
    } //!!






    public function generateString($length)
    {
        $key='';
        $charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for($i=0; $i<$length; $i++)
            $key .= $charset[(mt_rand(0,(strlen($charset)-1)))];

        return $key;
    }


    public function getAparatFragme($aparat)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, 'https://www.aparat.com/etc/api/video/videohash/' . $aparat);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');  // sorate load kardan ro mibare bala
        $response = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if (isset($response["video"]["frame"])) {
            return $response["video"]["frame"];
        } else {
            return null;
        }


    }


    public function screenDelete($ps_id)
    {

        $directory_root = __DIR__ . '/../uploads/chanel_screen/';
        $st = $this->conn->prepare("SELECT name , thumb_name FROM page_screen WHERE ps_id = ?");
        $st->bind_param("i", $ps_id);
        $st->bind_result($name, $thumb_name);
        $st->execute();
        $st->store_result();
        if ($st->num_rows == 0) {
            return getResponse("اسکرین مورد نظر یافت نشد", 401);
        }

        $st->fetch();

        $st = $this->conn->prepare("delete from page_screen where ps_id = ?");
        $st->bind_param("i", $ps_id);
        $st->execute();
        if ($this->conn->affected_rows > 0) {

            unlink($directory_root . $name);
            unlink($directory_root . $thumb_name);

            return getResponse("اسکرین با موفقیت حذف گردید", 200);
        } else {
            return getResponse("خطا در حذف اسکرین شات", 200);
        }


    }



    //be sorate koli manteq ine  : agar page to table nabood insert mishe fildaye pic , thumb meqghdareshon ekhtiare agar aks dashtim ba ye esmi zakhire mishe age
    //na ba  null zakhire mishe  : agar bood ke miam mibinim ke aya pic , thumb meqdareshon nulle ya na age na esmeshon ro migimr va agar akse jadid dashtim
    // ba name qabli zakhire mikonim agar nameshon null bood va aks dashtim ye esme jadid vase aks mizarim

    public function updatePage($page_id, $user_id, $object, $aparat, $tags, $screens)
    {


        //vase in avale kar null mizarim ke chon inserte pic delbekhahie inja va   s_file set nashode pic maqadire in dota to table null bashan
        $pic_name_to_store = null;
        $thumb_name_to_store = null;
        $new_width = "275";
        $new_height = "275";
        $quality = 90;
        $directory_root = __DIR__ . '/../uploads/chanel_pic/';
        $s = $this->conn->prepare("SELECT pic ,thumb FROM page_update WHERE page_id = ?");
        $s->bind_param("i", $page_id);
        $s->bind_result($pic_name_to_store, $thumb_name_to_store);
        $s->execute();
        $s->store_result();
        // aval check mikonim ke page toye table mojode  ya na age na insert age are update
        // pic , thumb ham vase in migirim ke pic jadid ro ba hamoon esme qabli override konim yani dige ye pic jadid nasazim va qabli ro pak konim
        //vali agar null boodan va akse dashtim ye esme jadid mizarim vas aks
        if ($s->num_rows > 0) {
            $s->fetch();
            //check mikonim agar aks dasht aks ro insert konim
            if (isset($_FILES['pic']['name'])) {
                //check mikonim age null boodan yani mogheye insert pic vared nashode bood ye esme vasashon bezarim
                // dar qeyre insorat ba hamoon esme qabli ke toye bind_result darim migrim zakhire koinm
                $pic_info = pathinfo($_FILES['pic']['name']);
                $extension = $pic_info['extension'];
                if ($pic_name_to_store == null || $thumb_name_to_store == null) {
                    $folder = rand(1, 1000);
                    $chanel_pic_path = $chanel_pic_path_temp . $folder;
                    if (!is_dir($chanel_pic_path)) {
                        mkdir($chanel_pic_path);
                    }

                    $pic_name = rand(10, 100) . substr(abs(crc32(uniqid())), 0, 6) . '.' . $extension;
                    $pic_name_to_store = $folder . '/pic_' . $pic_name;
                    $thumb_name_to_store = $folder . '/thumb_' . $pic_name;
                }
                $pic_path = $directory_root . $pic_name_to_store;
                $thumb_path = $directory_root . $thumb_name_to_store;
                $filetype = $_FILES['pic']['type'];
                list($width_orig, $height_orig) = getimagesize($_FILES['pic']['tmp_name']);
                $ratio_orig = $width_orig / $height_orig;
                if ($new_width / $new_height > $ratio_orig) {
                    $new_width = $new_height * $ratio_orig;
                } else {
                    $new_height = $new_width / $ratio_orig;
                }
                if ($filetype == "image/jpeg" || $filetype == "image/jpg") {
                    $imagecreate = "imagecreatefromjpeg";
                    $imageformat = "imagejpeg";
                }
//        else if ($filetype == "image/png") {
//            $imagecreate = "imagecreatefrompng";
//            $imageformat = "imagepng";
//            $quality = 9;
                //     }
                else if ($filetype == "image/gif") {
                    $imagecreate = "imagecreatefromgif";
                    $imageformat = "imagegif";
                } else {
                    $imagecreate = "imagecreatefromjpeg";
                    $imageformat = "imagejpeg";
                }
                $image_new = imagecreatetruecolor($new_width, $new_height);
                $uploadedfile = $_FILES['pic']['tmp_name'];
                $image = $imagecreate($uploadedfile);
                // vase inke age png bood back groundesh siah nashe
                if ($extension == "gif" or $extension == "png") {
                    imagecolortransparent($image_new, imagecolorallocatealpha($image_new, 0, 0, 0, 127));
                    imagealphablending($image_new, false);
                    imagesavealpha($image_new, true);
                }
                imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
                $imageformat($image_new, $thumb_path, $quality);
                imagedestroy($image_new);
                if (!move_uploaded_file($_FILES["pic"]["tmp_name"], $pic_path)) {
                    return getResponse("خطا در بارگزاری تصویر . دوباره امتحان کنید", 501);
                }
            }
            $s = $this->conn->prepare("UPDATE page_update SET name = ? , short_des = ? , des = ? , pic = ? ,thumb = ? , aparat = ? , up_cnt = up_cnt + 1  WHERE page_id = ? ");
            $s->bind_param("ssssssi", $object['name'], $object['short_des'], $object['des'], $pic_name_to_store, $thumb_name_to_store, $aparat, $page_id);
            $s->execute();
            if ($this->conn->affected_rows > 0) {

                //first deleting all previous tags
                $stmt = $this->conn->prepare("delete from tag_page where page_id = ?");
                $stmt->bind_param("i", $page_id);
                $stmt->execute();

                $stmt = $this->conn->prepare("insert into tag_page (tag_id,page_id) values (?,?)");
                foreach ($tags as $tag) {
                    $stmt->bind_param("ii", $tag, $page_id);
                    $stmt->execute();
                }

                $this->handle_sreens($screens, $page_id);

                fireBaseNotify("به روز رسانی صفحه", "یک صفحه به روز رسانی شد", "confrim", 1, 1, 1);
                return getResponse("به روز رسانی انجام و پس از بازبینی تغییرات اعمال خواهد شد", 201);
            } else {

                return getResponse("خطا در به روز رسانی دوباره ", 400);
            }
        } else {
            if (isset($_FILES['pic']['name'])) {
                $date = date("y-m-d");
                $directory = $directory_root . $date . '/';
                $pic_info = pathinfo($_FILES['pic']['name']);
                $extension = $pic_info['extension'];
                if (!is_dir($directory)) {
                    mkdir($directory);
                }
                $pic_name = rand(10, 100) . substr(abs(crc32(uniqid())), 0, 6) . '.' . $extension;
                $pic_name_to_store = $date . '/pic_' . $pic_name;
                $thumb_name_to_store = $date . '/thumb_' . $pic_name;

                $pic_path = $directory_root . $pic_name_to_store;
                $thumb_path = $directory_root . $thumb_name_to_store;

                $filetype = $_FILES['pic']['type'];
                list($width_orig, $height_orig) = getimagesize($_FILES['pic']['tmp_name']);
                $ratio_orig = $width_orig / $height_orig;
                if ($new_width / $new_height > $ratio_orig) {
                    $new_width = $new_height * $ratio_orig;
                } else {
                    $new_height = $new_width / $ratio_orig;
                }
                if ($filetype == "image/jpeg" || $filetype == "image/jpg") {
                    $imagecreate = "imagecreatefromjpeg";
                    $imageformat = "imagejpeg";
                }
//        else if ($filetype == "image/png") {
//            $imagecreate = "imagecreatefrompng";
//            $imageformat = "imagepng";
//            $quality = 9;
                //     }
                else if ($filetype == "image/gif") {
                    $imagecreate = "imagecreatefromgif";
                    $imageformat = "imagegif";
                } else {
                    $imagecreate = "imagecreatefromjpeg";
                    $imageformat = "imagejpeg";
                }

                $image_new = imagecreatetruecolor($new_width, $new_height);
                $uploadedfile = $_FILES['pic']['tmp_name'];
                $image = $imagecreate($uploadedfile);
                // vase inke age png bood back groundesh siah nashe
                if ($extension == "gif" or $extension == "png") {
                    imagecolortransparent($image_new, imagecolorallocatealpha($image_new, 0, 0, 0, 127));
                    imagealphablending($image_new, false);
                    imagesavealpha($image_new, true);
                }
                imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
                $imageformat($image_new, $thumb_path, $quality);
                imagedestroy($image_new);
                if (!move_uploaded_file($_FILES["pic"]["tmp_name"], $pic_path)) {
                    return getResponse("خطا در بارگزاری تصویر . دوباره امتحان کنید", 501);
                }
            }


            $s = $this->conn->prepare("INSERT INTO page_update (page_id , user_id , id , prefix,   name , short_des ,aparat, des , pic, thumb) VALUES (?,?,?,?,?,?,?,?,?,?)");
            $s->bind_param("iissssssss", $page_id, $user_id, $object['id'], $object['prefix'], $object['name'], $object['short_des'], $aparat, $object['des'], $pic_name_to_store, $thumb_name_to_store);
            if ($s->execute()) {


                $stmt = $this->conn->prepare("delete from tag_page where page_id = ?");
                $stmt->bind_param("i", $page_id);
                $stmt->execute();

                $stmt = $this->conn->prepare("insert into tag_page (tag_id,page_id) values (?,?)");
                foreach ($tags as $tag) {
                    $stmt->bind_param("ii", $tag, $page_id);
                    $stmt->execute();
                }

                $this->handle_sreens($screens, $page_id);

                fireBaseNotify("به روز رسانی صفحه", "یک صفحه به روز رسانی شد", "confrim", 1, 1, 1);
                return getResponse("به روز رسانی انجام و پس از بازبینی تغییرات اعمال خواهد شد", 201);
            } else {

                return getResponse("خطا در به روز رسانی", 400);
            }
        }
    }     //!!


    public function handle_sreens($screens, $page_id)
    {

        $directory_root = __DIR__ . '/../uploads/chanel_screen/';

        //the screends that user alredy have
        $my_screens = array();
        $st = $this->conn->prepare("select ps_id from page_screen where page_id = ?");
        $st->bind_param("i", $page_id);
        $st->execute();
        $result = $st->get_result();
        while ($single = $result->fetch_assoc()) {
            array_push($my_screens, $single["ps_id"]);
        }

        //yani screen hai ro joda mikone ke qablan boodan vali to update jadid nistan
        $should_delte = array_values(array_diff($my_screens, $screens));

        //yani screen hai ro joda mikone ke jdidan hastan va qablan naboodan va bayad ezafe beshan
        $should_insert = array_values(array_diff($screens, $my_screens));


        $stmt = $this->conn->prepare("update page_screen set page_id = ? where ps_id like ?");
        foreach ($should_insert as $screen) {
            $stmt->bind_param("ii", $page_id, $screen);
            $stmt->execute();
        }
        $stmt->close();

        foreach ($should_delte as $screen) {
            $st = $this->conn->prepare("select name , thumb_name from page_screen where ps_id = ?");
            $st->bind_param("i", $screen);
            $st->bind_result($name, $thumb_name);
            $st->execute();
            $st->store_result();
            $st->fetch();
            @unlink($directory_root . $name);
            @unlink($directory_root . $thumb_name);
            $st = $this->conn->prepare("delete from page_screen where ps_id like ?");
            $st->bind_param("i", $screen);
            $st->execute();

        }


    }

    public function getBanner($page_id) {

        $st = $this->conn->prepare("select p.name , p.banner , s.prefix from page p join sociall s 
                  on p.social_id = s.social_id where page_id = ?");
        $st->bind_param("i", $page_id);
        $st->execute();
        $result = $st->get_result();
        $response = $result->fetch_assoc();

        return $response;

    }

    public function updateBanner($page_id, $user_id)
    {
        $root_directory = __DIR__ . '/../uploads/banner/';
        //check mikonim mibinim age banner qablan dasht ke ro hamoon zakhire mikoim dar vaqe faqat ask ro taqir midim va table hamoon mimone va esme aks taqir nemikone
        $st = $this->conn->prepare("SELECT banner FROM page_banner WHERE page_id = ?");
        $st->bind_param("i", $page_id);
        $st->bind_result($banner);
        $st->execute();
        $st->store_result();
        $banner_info = pathinfo($_FILES['banner']['name']);
        $extension = $banner_info['extension'];

        if ($st->num_rows > 0) {
            $st->fetch();
            $banner_name = substr($banner, 0, stripos($banner, "."));
            $banner_path = $root_directory . $banner_name . "." . $extension;
            $st->fetch();
            if (move_uploaded_file($_FILES["banner"]['tmp_name'], $banner_path)) {
                fireBaseNotify("بنر جدید", "یک بنر جدید به روز رسانی شد", "confrim", 1, 1, 1);
                return getResponse("بنر شما با موفقیت به روز رسانی و در صف تایید قرار گرفت", 201);
            } else {
                return getResponse("خطا در آپلود بنر", 400);
            }
        } else {
            //bannner nadarim pas yeki misazim vva zakhire mikonim

            $date_folder = date("y-m-d");
            if (!is_dir($root_directory . $date_folder . '/')) {
                mkdir($root_directory . $date_folder . '/');
            }
            //  $banner_name_to_store =  rand(10,1000). '_' . uniqid() .'.'. $extension;
            $banner_name_to_store = $date_folder . '/' . $page_id . "_" . uniqid() . '.' . $extension;
            $banner_path = $root_directory . $banner_name_to_store;
            if (move_uploaded_file($_FILES["banner"]["tmp_name"], $banner_path)) {
                $st = $this->conn->prepare("INSERT INTO page_banner (page_id,user_id,banner) VALUE (?,?,?)");
                $st->bind_param("iis", $page_id, $user_id, $banner_name_to_store);
                if ($st->execute()) {
                    fireBaseNotify("بنر جدید", "یک بنر جدید به روز رسانی شد", "confrim", 1, 1, 1);
                    return getResponse("بنر شما با موفقیت ثبت گردید و پس از تایید  شما میتوانید صفحه یا کانال خود را در جایگاه تبلیغات بنری رزرو نمایید", 201);
                } else {
                    try {
                        @ unlink($banner_path);
                    } catch (\exception $e) {
                    }
                    return getResponse("خطا در ساخت بنر", 400);
                }
            } else {
                return getResponse("خطا در آپلود بنر", 400);
            }
        }
    }

    public function getChanelById($chanel_id)
    {
        $st = $this->conn->prepare("SELECT page_id, social_id,cat_id,user_id,id ,name,short_des,des,pic,thumb,view,member,updated_at FROM page 
                                      WHERE chanel_id LIKE ?");
        $st->bind_param("i", $chanel_id);
        $st->execute();
        $result = $st->get_result();
        $st->close();
        return $result->fetch_assoc();

    }

    public function getUserPages($user_id, $last_id, $limit)
    {

        $condition = $last_id == 0 ? ' <= ? ' : ' < ? ';
        $query = "SELECT p.page_id,p.short_url, p.id,p.name,p.short_des,p.des, p.pic,p.thumb,p.banner, p.member,p.active,p.view ,p.created_at ,s.social_id, s.p_name AS  social_name , 
                                                s.icon  AS social_icon , s.e_name,s.prefix ,s.member_prefix,c.cat_id, c.name AS cat_name ,c.icon  AS cat_icon
                                                FROM page p JOIN sociall s ON p.social_id = s.social_id
                                                JOIN category c ON p.cat_id = c.cat_id
                                                WHERE  p.user_id = ? AND p.page_id" . $condition . "ORDER BY page_id DESC LIMIT 0,?";


        if ($last_id == 0) {
            $s = $this->conn->prepare("SELECT max(page_id) AS last FROM page WHERE user_id = ? ");
            $s->bind_param('i', $user_id);
            $s->bind_result($last);
            $s->execute();
            $s->store_result();
            $s->fetch();
            // agar hich satri yaft nashe num_row 1 barmigardone ama ba meqdare null . pas agar null bood yani hich pagi ke actives sefr bashe nadarim
            if (is_null($last)) {
                $response[STATUS] = 200;
                $response['pages'] = array();
                return $response;
            }
            $s->close();
            $last_id = $last;
        }
        $s = $this->conn->prepare($query);
        $s->bind_param("iii", $user_id, $last_id, $limit);
        $s->execute();
        $result = $s->get_result();

        $response[STATUS] = 200;
        $response['pages'] = array();
        while ($single = $result->fetch_assoc()) {
            array_push($response['pages'], $single);
        }
        $s->close();
        return $response;

    }

    public function getTags()
    {
        $s = $this->conn->prepare("select tag_id , name from tags");
        $s->execute();
        $result = $s->get_result();
        $response = array();
        while ($single = $result->fetch_assoc()) {
            array_push($response, $single);
        }
        return $response;

    }


    public function getPageDetails($user_id, $page_id)
    {
        $response = array();
        $s = $this->conn->prepare("SELECT p.initial ,l.like_cnt ,l.mylike FROM page p  JOIN (SELECT page_id ,
                                COUNT(*) like_cnt , IF((SELECT COUNT(*) FROM likes WHERE page_id = ? AND user_id = ? )  , 1 , 0) AS mylike
                                            FROM likes WHERE page_id = ? ) l ON p.page_id = l.page_id ");
        $s->bind_param("iii", $page_id, $user_id, $page_id);
        $s->bind_result($initial, $like_cnt, $mylike);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $likes = $initial + $like_cnt;
        //gereftan tedade jam like inital va like // agar like null bashe hamoon 0 hesab mishe va ba inital jam mishe
        $response['likes_cnt'] = $likes;
        $response['user_like'] = $mylike;
        $s = $this->conn->prepare("SELECT COUNT(*) AS cm_cnt FROM comment WHERE page_id = ? AND active = 1");
        $s->bind_param("i", $page_id);
        $s->bind_result($cm_cnt);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $response['comments_cnt'] = $cm_cnt;
        return $response;

    }


    public function getPage($user_id, $page_id)
    {


        $s = $this->conn->prepare("select user_id from page where page_id = ? and active > 0");
        $s->bind_param("i", $page_id);
        $s->bind_result($user_id_query);
        $s->execute();
        $s->store_result();
        $s->fetch();


        //to check if the apikey is the owner of the page and if page id exists
        if ($s->num_rows < 0 || $user_id != $user_id_query) {
            return null;
        }
        $response = array();


        $s = $this->conn->prepare("select p.id,p.member,p.pic,p.thumb,p.cat_id,p.name,p.short_des,p.des,p.aparat,s.prefix, s.member_prefix,c.name as cat_name
                       from page p join sociall s on p.social_id = s.social_id join category c on p.cat_id = c.cat_id  where p.page_id = ?");

        $s->bind_param("i", $page_id);
        $s->execute();
        $result = $s->get_result();
        $response["page"] = $result->fetch_assoc();

        $tags = array();
        $s = $this->conn->prepare("select tag_id,name from tags where tag_id in(select tag_id from tag_page where page_id like ?)");
        $s->bind_param("i", $page_id);
        $s->execute();
        $result = $s->get_result();
        while ($single_tag = $result->fetch_assoc()) {
            array_push($tags, $single_tag);
        }
        $response["tags"] = $tags;


        $screens = array();
        $s = $this->conn->prepare("select ps_id , name , thumb_name from page_screen where page_id like ?");
        $s->bind_param("i", $page_id);
        $s->execute();
        $result = $s->get_result();
        while ($single_screen = $result->fetch_assoc()) {
            array_push($screens, $single_screen);
        }
        $response["screens"] = $screens;
        return $response;


    }



    public function insertShortrandoms() {
        $affected = 0 ;
        $st = $this->conn->prepare("select page_id from page");
        $st->execute();
        $result = $st->get_result();
        $response = array();

        while ($single = $result->fetch_assoc()) {
            array_push($response,$single);

        }

        foreach ($response as $page_id) {
            while (true) {
                $random = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 6)), 0, 6);
                $smt=$this->conn->prepare("select short_url from page where short_url like ?");
                $smt->bind_param("s",$random);
                $smt->execute();
                $result = $smt->get_result();
                if ($result->num_rows==0) {

                    $smt=$this->conn->prepare("update page set short_url = ? where page_id = ?");
                    $smt->bind_param("si",$random,$page_id["page_id"]);
                    $smt->execute();
                    $affected++;
                    break ;
                }

            }
        }

        die($affected . " hamid numbers of page affected ");

    }


    public function getComments($page_id, $last_id, $limit)
    {
        $condition = $last_id == 0 ? ' <= ? ' : ' < ? ';
        $query = "SELECT c.comment_id,c.user_id,c.text,if(reply_active = 1 ,reply,null) as reply ,c.created_at , c.reply_time, REPLACE(u.mobile,SUBSTRING(u.mobile,5,4),'****') 
                      AS mobile,u.username,u.profile,u.thumb,u.view_able
                     FROM comment c JOIN user_info u ON c.user_id = u.user_id 
                     WHERE c.page_id = ? AND c.active =1 AND c.comment_id" . $condition . "ORDER BY comment_id DESC LIMIT 0,$limit";
        if ($last_id == 0) {
            $s = $this->conn->prepare("SELECT max(comment_id) AS last FROM comment WHERE page_id = ? AND active = 1 ");
            $s->bind_param('i', $page_id);

            $s->bind_result($last);
            $s->execute();
            $s->store_result();
            $s->fetch();
            // agar hich satri yaft nashe num_row 1 barmigardone ama ba meqdare null . pas agar null bood yani hich pagi ke actives sefr bashe nadarim
            $s->close();
            $last_id = $last;
        }

        $s = $this->conn->prepare($query);

        $s->bind_param("ii", $page_id, $last_id);
        $s->execute();
        $result = $s->get_result();

            $response = array();
            while ($single = $result->fetch_assoc()) {
                $single["persian_time"] = convertTime($single["created_at"]);
                $single["persian_time_reply"] = convertTime($single["reply_time"]);
                array_push($response, $single);
            }
            $s->close();
            return $response;



    }


    public function updateCommentReadCount($user_id)
    {
        $s = $this->conn->prepare("UPDATE user_info SET read_comments = (select count(*) from comment where user_id = ? and active > 0) WHERE user_id = ?");
        $s->bind_param("ii", $user_id, $user_id);
        $s->execute();
        if ($this->conn->affected_rows > 0) {
            $s->close();
            return getResponse("به روز رسانی انجام شد", 201);
        } else {
            $s->close();
            return getResponse("خطا در به روز رسانی", 401);
        }
    }




    public function commentOnPage($user_id, $page_id, $text)
    {
        $s=$this->conn->prepare("select user_id , name  from page where page_id like ? ");
        $s->bind_param("i",$page_id);
        $s->bind_result($reciver_id,$name);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $s = $this->conn->prepare("INSERT INTO comment(page_id , user_id , text ) VALUES (?,?,?)");
        $s->bind_param("iis", $page_id, $user_id, $text);
        if ($s->execute()) {
            fireBaseNotify("نظر جدید", "یک نظر جدید ارسال شد", "confrim", 1, 1, 1);
            return getResponse("نظر شما ثبت و پس از تایید به نمایش در خواهد آمد", 201);
        } else {
            return getResponse("خطا در ثبت نظر ", 401);
        }

    }

    public function replyComment($user_id, $reply, $comment_id)
    {
        //vase inke motmaen shim faqat admine canal mitone javabe comment bede
        $s = $this->conn->prepare("SELECT page_id FROM page WHERE page_id = (SELECT  page_id FROM comment WHERE comment_id = ?) AND user_id = ?");
        $s->bind_param("ii", $comment_id, $user_id);
        $s->execute();
        $result = $s->get_result();
        if ($result->num_rows > 0) {
            $s = $this->conn->prepare("UPDATE comment SET reply  = ? , reply_time = now(), reply_active = 0 WHERE comment_id = ? ");
            $s->bind_param("si", $reply, $comment_id);
            $s->execute();
            if ($this->conn->affected_rows > 0) {
                return getResponse("پاسخ ارسال گردید و پس از تایید به نمایش در خواهد آمد", 201);
            } else {
                return getResponse("خطا در ارسال پاسخ", 401);
            }

        } else {
            return getResponse("فقط مالک صفحه یا کانال قادر به پاسخ میباشد", 401);
        }

    }


    public function likePage($user_id, $page_id)
    {
        $s = $this->conn->prepare("INSERT INTO likes (page_id , user_id ) VALUES (?,?)");
        $s->bind_param("ii", $page_id, $user_id);
        if ($s->execute()) {
            $s->close();
            return getResponse("پسندیده شد", 201);
        } else {
            $s->close();
            return getResponse("خطا در ثبت پسندیدن ", 401);
        }
    }





    public function unlikePage($user_id, $page_id)
    {
        $s = $this->conn->prepare("DELETE FROM likes WHERE page_id = ? AND user_id = ?");
        $s->bind_param("ii", $page_id, $user_id);
        $s->execute();
        if ($this->conn->affected_rows > 0) {
            return getResponse("لغو پسندیدن", 201);
        } else {
            return getResponse("خطا در لغو پسندیدن", 401);
        }

    }


    public function ratePage($user_id, $page_id,$rate)
    {
        $st=$this->conn->prepare("select page_id from page_rate where page_id = ? and user_id = ?");
        $st->bind_param("ii",$page_id,$user_id);
        $st->execute();
        $result=$st->get_result();
        $response=array();

        if ($result->num_rows>0) {
            $st->close();
            $smt = $this->conn->prepare("update page_rate set rate = ? where user_id = ? and page_id = ?");
            $smt->bind_param("iii",$rate, $user_id, $page_id);
            $smt->execute();

            if ($smt->affected_rows > 0 ){

                $response[MESSAGE]="به روز رسانی امتیاز انجام شد";
                $response[STATUS]=201;
                $st= $this->conn->prepare("SELECT  Round(AVG(rate),1) rate , COUNT(*) as voters ,IF ((SELECT COUNT(*) FROM page_rate
                                                   WHERE page_id = ? AND user_id = ? ) ,(SELECT rate from page_rate WHERE user_id = ? and page_id = ?
                                           ) , null) AS myrate  FROM page_rate where page_id = ?");
                $st->bind_param("iiiii",$page_id,$user_id,$user_id,$page_id,$page_id);
                $st->execute();
                $rating=$st->get_result()->fetch_assoc();
                $response["rating"] =$rating;
                return $response;

            } else {
                $response[MESSAGE]="خطا در به روز رسانی امتیاز";
                $response[STATUS]=201;
                $response["rate"]=$rate;
                return $response;

            }

        }else {
           $st->close();
            $stmt = $this->conn->prepare("INSERT INTO page_rate (page_id,user_id,rate) VALUES (?,?,?)");
            $stmt->bind_param("iii", $page_id, $user_id,$rate);

            if ($stmt->execute()) {
                $stmt->close();

                $response[MESSAGE]="ثبت امتیاز انجام شد";
                $response[STATUS]=201;

                $st= $this->conn->prepare("SELECT  Round(AVG(rate),1) rate , COUNT(*) as voters ,IF ((SELECT COUNT(*) FROM page_rate
                                                   WHERE page_id = ? AND user_id = ? ) ,(SELECT rate from page_rate WHERE user_id = ? and page_id = ?
                                           ) , null) AS myrate  FROM page_rate where page_id = ?");
                $st->bind_param("iiiii",$page_id,$user_id,$user_id,$page_id,$page_id);
                $st->execute();
                $rating=$st->get_result()->fetch_assoc();
                $response["rating"] =$rating;


                return $response;

            } else {
                $stmt->close();
                $response[MESSAGE]="خطا در ثبت امتیاز";
                $response[STATUS]=400;
                $response["rate"]=$rate;
                return $response;
            }
        }


    }


    public function increaseView($page_id)
    {
        $st = $this->conn->prepare("UPDATE page SET view = view + 1 WHERE page_id LIKE ?");
        $st->bind_param("i", $page_id);
        $st->execute();
        $st->close();
    }

    public function reportPage($page_id, $user_id, $rt_id, $prefix)
    {
        $st = $this->conn->prepare("SELECT report_id FROM page_report WHERE page_id LIKE ? AND user_id LIKE ?");
        $st->bind_param("ii", $page_id, $user_id);
        $st->execute();
        $result = $st->get_result();
        if ($result->num_rows > 0) {
            $st->close();
            return getResponse("این " . $prefix . " قبلا توسط شما گزارش شده است", 400);
        } else {
            $st = $this->conn->prepare("INSERT INTO page_report (page_id,user_id,rt_id) VALUES (?,?,?)");
            $st->bind_param("iii", $page_id, $user_id, $rt_id);
            if ($st->execute()) {
                $st->close();
                return getResponse($prefix . " مورد نظر گزارش شد", 201);
            } else {
                return getResponse("خطا در گزارش " . $prefix . " دوباره تلاش کنید", 400);
            }
        }

    }

    public function report_cm($user_id, $comment_id, $admin)
    {
        $st = $this->conn->prepare("select * from report_cm_new where user_id like  ? and comment_id like ?  and admin like ?");
        $st->bind_param("iii", $user_id, $comment_id, $admin);
        $st->execute();
        $result = $st->get_result();
        if ($result->num_rows > 0) {
            $st->close();
            return getResponse("این نظر قبلا توسط شما گزارش شده است", 401);
        } else {
            $st = $this->conn->prepare("insert into report_cm_new(user_id,comment_id,admin) values (?,?,?)");
            $st->bind_param("iii", $user_id, $comment_id, $admin);
            if ($st->execute()) {
                return getResponse("گزارش شما ارسال و پیگیری خواهد شد . با تشکر", 201);
            } else {
                return getResponse("خطا در گزارش نظر", 401);
            }


        }


    }

////////////////////////////////////////////////////////////////////goldPurchase\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    public function getGoldList($user_id)
    {
        $stmt = $this->conn->prepare("select inventory from users where user_id like ?");
        $stmt->bind_param("i", $user_id);
        $stmt->bind_result($inventory);
        $stmt->execute();
        $stmt->store_result();
        $stmt->fetch();


        $response = array();
        $response["golds"] = array();
        $response["inventory"] = $inventory;

        $st = $this->conn->prepare("SELECT gold_id,cnt,price,currency,title,des,dis FROM gold WHERE active = 1");
        $st->execute();
        $result = $st->get_result();
        while ($single = $result->fetch_assoc()) {

            array_push($response["golds"], $single);
        }

        return $response;


    }

    //taiid vaziate state gold order
    public function updateGoldState($state, $order_id)
    {
        $stmt = $this->conn->prepare("UPDATE gold_order SET state = ? WHERE go_id = ?");
        $stmt->bind_param("ii", $state, $order_id);
        $stmt->execute();
    }

    //taiid vaziate code gold order
    public function updateGoldCode($code, $order_id)
    {
        $stmt = $this->conn->prepare("UPDATE gold_order SET code = ? WHERE go_id = ?");
        $stmt->bind_param("ii", $code, $order_id);
        $stmt->execute();
    }

    public function makeItinialGoldFaktor($gold_id, $user_id)
    {
        $response = array();
        $stmt = $this->conn->prepare("SELECT cnt,price,dis FROM gold WHERE gold_id = ? AND active = 1");
        $stmt->bind_param("i", $gold_id);
        $stmt->bind_result($cnt, $price, $dis);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->fetch();
            if ($dis > 0) {
                // aval takhfifesho dar miare bad kolesho azash kam mikone
                $price = $price - (($price * $dis) / 100);
            }

            $stmt = $this->conn->prepare("SELECT inventory from users where user_id like ?");
            $stmt->bind_param("i", $user_id);
            $stmt->bind_result($inventory);
            $stmt->execute();
            $stmt->store_result();
            $stmt->fetch();

            $stmt = $this->conn->prepare("INSERT INTO gold_order (user_id ,gold_id,inventory ,cnt,price,dis) VALUE (?,?,?,?,?,?)");
            $stmt->bind_param("iiiiii", $user_id, $gold_id, $inventory, $cnt, $price, $dis);
            if ($stmt->execute()) {
                $order_id = $this->conn->insert_id;
                $response["order_id"] = $order_id;
                $response["price"] = $price;
                return $response;
            } else {
                // mamooolan vaqti etefaq miofte ke user_id dorost nabashe va insert nashe dige dalie khasi nadare
                $response["order_id"] = null;
                $response[MESSAGE] = "خطا در ساخت فاکتور";
                $response[STATUS] = 400;
                return $response;
            }

        } else {
            //vaqti ke id gold eshtebah vard shode bashe yani dasti vard shode bashe
            $response["order_id"] = null;
            $response[MESSAGE] = "محصول مورد نظر یافت نشد";
            $response[STATUS] = 404;
            return $response;
        }
    }

    public function goldFaktorTransUpdate($trans_id, $order_id)
    {
        $state = 1;
        $stmt = $this->conn->prepare("UPDATE gold_order SET trans_id = ? , state = ? WHERE go_id = ?");
        $stmt->bind_param("sii", $trans_id, $state, $order_id);
        $stmt->execute();
        return $this->conn->affected_rows > 0;

    }

    //gereftan qeymat toye call back ke moshakhas mikone aya taracnesh motabare va ya qablan vaziatesh moshakhas nashode ;
    public function checkGoldTransaction($trans_id, $order_id)
    {
        $response = array();
        $stmt = $this->conn->prepare("SELECT user_id, cnt , price , state  FROM gold_order WHERE trans_id LIKE ? AND go_id = ?");

        $stmt->bind_param("si", $trans_id, $order_id);
        $stmt->bind_result($user_id, $cnt, $price, $state);
        $stmt->execute();
        $stmt->store_result();

        if ($stmt->num_rows > 0) {

            $stmt->fetch();
            if ($state == 6) {
                $response["state"] = 1;  // yani vaziaate trakonesh qablan moshakhas shode;

                return $response;
            } else {
                $response["state"] = 2;  // yani vaziatesh moshakhsh nashode va mojode ke mirim moshakhasesh koinm
                $response["user_id"] = $user_id;
                $response["price"] = $price;
                $response["cnt"] = $cnt;
                return $response;
            }

        } else {
            $response["state"] = 0;  // yani taraconesh namotabare va nist to jadval;
            return $response;
        }
    }

    //vase vaqtie ke code bargashti 0 hastesh pas ma miam aval be user gold ro ezafe mikonim badesh vaziat haro to jadval be state 6 va code 0  taqir midim
    public function goldPurSuccess($user_id, $cnt, $order_id, $code)
    {
        $this->conn->begin_transaction();
        $commit = true;

        try {
            $st1 = $this->conn->prepare("UPDATE users SET inventory = inventory + ? WHERE user_id = ?");
            $st1->bind_param("ii", $cnt, $user_id);
            $st1->execute();
            if ($st1->affected_rows < 1) {
                $commit = false;
            }

            $st2 = $this->conn->prepare("UPDATE gold_order SET code = ? , state = 6 WHERE go_id = ?");
            $st2->bind_param("ii", $code, $order_id);
            $st2->execute();

            if ($st2->affected_rows < 1) {
                $commit = false;
            }


            if (!$commit) {
                $this->conn->rollBack();
                return false;
            }

            $this->conn->commit();

        } catch (\PDOException $exception) {
            $this->conn->rollBack();
            $commit = false;
            return false;
        }

        if ($commit) {
            return true;
        } else {
            return false;
        }

    } //!!

    ////////////////////////////////////////////////////////////////////specialReserv\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function getSpecials($page_id, $user_id)
    {

        $response = array();
        $inventory = $this->getInventory($user_id);
        $response["specials"] = array();
        $response["inventory"] = $inventory;


        $stmt = $this->conn->prepare("select  p.cat_id ,p.social_id, p.name page_name,banner, s.p_name social_name ,s.prefix,s.prefix_p,c.name cat_name
               from page p join sociall s on p.social_id =s.social_id join category c on p.cat_id=c.cat_id  where page_id like ? ");
        $stmt->bind_param("i", $page_id);
        $stmt->execute();
        $result = $stmt->get_result();
        $info = $result->fetch_assoc();
        $response["name"] = $info["page_name"];
        $response["prefix"] = $info["prefix"];

        // to in qesmat ma miam va special hari migrim ke reserved flageshon kochektar az 2 yani tasavsote karbar sabt shode hlaa ya 0 hastesh ke yani felan reserv shode
        //ya 1 hastesh yani kharid anjam shode ;
        //
        // tedade reserve special 1 va 2 az tariqe social id moshakhash mishe ke mamoolan sabete va 3 ,4 az tariqe social_id + cat_id moshakhas mishe ke mamoolan motaqaire
        //
        $st = $this->conn->prepare("SELECT s.special_id, s.banner_need, s.type,s.is_main,s.title,s.pic,s.price,s.gold,s.max,s.day ,IFNULL(r.count,0) reserved
                              FROM special s LEFT JOIN (SELECT special_id,COUNT(special_id) as  count  FROM special_r WHERE social_id = ? AND
                          (cat_id = ? OR cat_id IS NULL) AND reserved_flag < 2 GROUP BY special_id) r ON s.special_id = r.special_id");

        $st->bind_param("ii", $info["social_id"], $info["cat_id"]);
        $st->execute();
        $result = $st->get_result();
        while ($single = $result->fetch_assoc()) {
            $single["hasBanner"] = is_null($info["banner"]) ? false : true;
            $stmt = $this->conn->prepare("select page_id from special_r where special_id = ? and page_id = ? and reserved_flag = 1");
            $stmt->bind_param("ii", $single["special_id"], $page_id);
            $stmt->execute();
            $result_resevred = $stmt->get_result();
            $single["isReserved"] = $result_resevred->num_rows > 0 ? true : false;

            if ($single["is_main"] == 1) {
                $single["title"] = $single["title"] . " " . $info["social_name"];
                if ($single["banner_need"] == 1) {
                    $single["message"] = "با رزرو این جایگاه بنر " . $info["prefix"] . " شما به مدت " . $single["day"] * 24 . " ساعت در صفحه اصلی " . $info["social_name"]
                        . " به نمایش خواهد در آمد توجه داشته باشید برای رزرو این جایگاه ابتدا باید بنر " . $info["prefix"] . " خود را ثبت کرده باشید";
                } else {
                    $single["message"] = "با رزرو این جایگاه " . $info["prefix"] . " شما به مدت " . $single["day"] * 24 . " ساعت در بخش ویژه های صفحه اصلی "
                        . $info["social_name"] . " و همچینین داخل تمامی " . $info["prefix_p"] . " ثبت شده " . $info["social_name"] . " به نمایش در خواهد آمد";
                }
            } else {
                $single["title"] = $single["title"] . " " . $info["cat_name"] . " " . $info["social_name"];
                if ($single["banner_need"] == 1) {
                    $single["message"] = "با رزرو این جایگاه بنر " . $info["prefix"] . " شما به مدت " . $single["day"] * 24 . " ساعت در دسته بندی " . $info["cat_name"] . " " . $info["social_name"]
                        . " به نمایش خواهد در آمد توجه داشته باشید برای رزرو این جایگاه ابتدا باید بنر " . $info["prefix"] . " خود را ثبت کرده باشید";
                } else {
                    $single["message"] = "با رزرو این جایگاه " . $info["prefix"] . " شما به مدت " . $single["day"] * 24 . " ساعت در بخش ویژه های دسته بندی " .
                        $info["cat_name"] . " " . $info["social_name"] . " و همچینین داخل تمامی " . $info["prefix_p"] . " ثبت شده دسته بندی " .
                        $info["cat_name"] . " " . $info["social_name"] . " به نمایش در خواهد آمد";
                }


            }



            array_push($response["specials"], $single);
        }
        $response["wordInfo"] = array("day"=>2,"price"=>10000,"gold"=>10);
        return $response;

    }


    public function getUserspecials($user_id)
    {


        // to in qesmat ma miam va special hari migrim ke reserved flageshon kochektar az 2 yani tasavsote karbar sabt shode hlaa ya 0 hastesh ke yani felan reserv shode
        //ya 1 hastesh yani kharid anjam shode ;
        //
        // tedade reserve special 1 va 2 az tariqe social id moshakhash mishe ke mamoolan sabete va 3 ,4 az tariqe social_id + cat_id moshakhas mishe ke mamoolan motaqaire
        //
        $specials = array();
        $now = new \DateTime(date("Y-m-d H:i:s"));

        $st = $this->conn->prepare("Select special_id ,is_main, title,pic from special");
        $st->execute();
        $result = $st->get_result();
        $i = 0;
        while ($single = $result->fetch_assoc()) {
            $single["type"] = 0; // the type 0 means it is special (banner and specia)
            $i++;

            $single["items"] = array();

            $pages = array();

            $st = $this->conn->prepare("select sr_id,exp_time,start_time,p.name,p.id,p.short_des,p.thumb,p.banner,p.member,c.name cat_name,s.p_name,
                                      s.prefix,s.member_prefix from special_r r join page p on r.page_id=p.page_id join category c on p.cat_id = c.cat_id
                                      join sociall s on p.social_id=s.social_id WHERE r.special_id = ? and r.user_id = ? and r.reserved_flag > 0 order by reserved_time desc 
                                       ");
            $st->bind_param("ii", $single["special_id"], $user_id);
            $st->execute();
            $result_pages = $st->get_result();
            while ($singlee = $result_pages->fetch_assoc()) {
                array_push($pages, $singlee);
            }

            foreach ($pages as $single_page) {
                $single_page["start_time_p"] = convertTime($single_page["start_time"]);
                $single_page["exp_time_p"] = convertTime($single_page["exp_time"]);

                $exp = new \DateTime($single_page["exp_time"]);



                $single_page["diff"] = $exp->getTimestamp() - $now->getTimestamp();

                array_push($single["items"], $single_page);
            }

            array_push($specials, $single);
        }


        $words_section = array("special_id" => 5, "is_main" => 1, "title" => "لغات رزرو شده برای نتایج جستجو", "pic" => "temp.png",
            "type" => 1);
        $words_section["items"] = array();

        $word_pages_items = array();

        $st = $this->conn->prepare("  select DISTINCT p.page_id, p.name,p.id,p.short_des,p.thumb,p.banner,p.member,c.name
                         cat_name,s.p_name, s.prefix,s.member_prefix from word_r w join page p on w.page_id=p.page_id join category c on p.cat_id = 
        c.cat_id join sociall s on p.social_id=s.social_id WHERE w.user_id = ?");
        $st->bind_param("i", $user_id);
        $st->execute();
        $words_pages = $st->get_result();


        while ($single_page = $words_pages->fetch_assoc()) {
            array_push($word_pages_items, $single_page);
        }
        $stt = $this->conn->prepare("SELECT wr_id,word,start_time,exp_time from word_r where page_id like ? and reserved_flag > 0");
        foreach ($word_pages_items as $single_word_page) {
            $single_word_page["words"] = array();
            $stt->bind_param("i", $single_word_page["page_id"]);
            $stt->execute();
            $result = $stt->get_result();


            while ($single_word = $result->fetch_assoc()) {

                $single_word["start_time_p"] = convertTime($single_word["start_time"]);
                $single_word["exp_time_p"] = convertTime($single_word["exp_time"]);

                $exp_word = new \DateTime($single_word["exp_time"]);



                $single_word["diff"] = $exp_word->getTimestamp() - $now->getTimestamp();

                array_push($single_word_page["words"], $single_word);
            }

            array_push($words_section["items"], $single_word_page);

        }

        array_push($specials, $words_section);


        return $specials;


    }


    // gereftane etelate avali ke vase estelamo ina lazeme
    public function getPurchaseNeeds($special_id, $page_id)
    {
        $needs = array();
        $st = $this->conn->prepare("SELECT is_main,banner_need,message,price,gold,max,day FROM special WHERE special_id = ?");
        $st->bind_param("i", $special_id);
        $st->bind_result($is_main, $banner_need, $message, $price, $gold, $max, $day);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $needs["is_main"] = $is_main;
        $needs["message"] = $message;
        $needs["gold"] = $gold;
        $needs["price"] = $price;
        $needs["max"] = $max;
        $needs["day"] = $day;
        $needs["banner_need"] = $banner_need;

        $st = $this->conn->prepare("SELECT p . user_id,p . cat_id,p . social_id,s . prefix FROM page p JOIN sociall s ON p . social_id = s . social_id WHERE page_id = ?");
        $st->bind_param("i", $page_id);
        $st->bind_result($user_id, $cat_id, $social_id, $prefix);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        $needs["user_id"] = $user_id;
        $needs["cat_id"] = $cat_id;
        $needs["social_id"] = $social_id;
        $needs["prefix"] = $prefix;
        return $needs;

    }

    public function getPurchaseNeedsInback($special_id, $page_id)
    {
        $needs = array();
        $st = $this->conn->prepare("SELECT message,day FROM special WHERE special_id = ?");
        $st->bind_param("i", $special_id);
        $st->bind_result($message, $day);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $needs["message"] = $message;
        $needs["day"] = $day;

        $st = $this->conn->prepare("SELECT p.name,s.prefix,s.p_name,c.name FROM page p JOIN sociall s ON p.social_id = s.social_id 
                                            join category c on p.cat_id = c.cat_id WHERE page_id = ?");
        $st->bind_param("i", $page_id);
        $st->bind_result($page_name, $prefix, $p_name, $cat_name);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        $needs["prefix"] = $prefix;
        $needs["p_name"] = $p_name;
        $needs["cat_name"] = $cat_name;
        $needs["page_name"] = $page_name;

        return $needs;

    }

    public function reservByGold($max, $cat_id, $is_main, $social_id, $page_id, $special_id, $user_id, $gold, $prefix, $day, $message, $banner_need)
    {
        $sr_id = 0;
        $so_id = 0;
        //chon vase daste asli bayad ba cat_id null zakhire she check mikoinm bebin agar is main 1 bood pas cat_id ro null mikonim
        if ($is_main == 1) {
            $cat_id = null;
        }
        if ($banner_need == 1) {
            if ($this->checkPageHasNotBanner($page_id)) {
                return getResponse("برای رزور این جایگاه ابتدا باید بنر " . $prefix . " را از قسمت مدیریت کانال ها و صفحات ثبت نمایید", 402);
            }
        }
        $inventory = $this->getInventory($user_id);
        if ($inventory < $gold) {
            return getResponse("موجودی سکه برای رزرو این جایگاه کافی نیست ", 402);  // yani mojordie gold vase reserv kafi nist
        }
        //check kardane inke qablan reserve nashode bashe
        $st = $this->conn->prepare("SELECT sr_id FROM special_r WHERE page_id = ? AND special_id = ?");
        $st->bind_param("ii", $page_id, $special_id);
        $st->execute();
        $result = $st->get_result();
        if ($result->num_rows > 0) {
            return getResponse($prefix . " مورد نظر در حال حاضر در این جایگاه رزرو میباشد", 422); // yani in page qablan toye in jaygah sabt shode
        }

        $this->conn->begin_transaction();
        try {
            //chon date objecte va string nist bayad zaman hamoon ro tabdin be string koinm
            $reserved_flag = 0;
            $resereved_time_obj = new \DateTime(date("Y-m-d H:i:s"));
            $reserved_time = $resereved_time_obj->format("Y-m-d H:i:s");
            $reserved_exp_obj = $resereved_time_obj->modify(" + 11 minutes");
            $reserved_exp = $reserved_exp_obj->format("Y-m-d H:i:s");
            // in marhale aval miam ye reserv ebtedai ba flage 0  mikonim va badesh mibnim ke aya tedadesh az tedade maxe special bishtar mishe ya na
            $st = $this->conn->prepare("INSERT INTO special_r(page_id, user_id, cat_id, social_id, special_id, reserved_flag, reserved_time, reserved_exp)
                  VALUES(?,?,?,?,?,?,?,?)");
            $st->bind_param("iiiiiiss", $page_id, $user_id, $cat_id, $social_id, $special_id, $reserved_flag, $reserved_time, $reserved_exp);

            if (!$st->execute()) {
                $this->conn->rollBack();
                getResponse($prefix . "خطا در ثبت . کد خطا : 1 . لطفا دوباره تلاش کنید ", 400); // khata to insert kardan reserve initial
            }


            $sr_id = $this->conn->insert_id;
            //age tedad bishtar shod pas kolan roll back mishe va peyqam takmil zarfiat midim
            if ($is_main) {
                $st = $this->conn->prepare("SELECT count(*) AS count  FROM special_r WHERE social_id = ? AND special_id = ? AND reserved_flag < 2");
                $st->bind_param("ii", $social_id, $special_id);
            } else {
                $st = $this->conn->prepare("SELECT count(*) AS count  FROM special_r WHERE cat_id = ? AND social_id = ? AND special_id = ? AND reserved_flag < 2");
                $st->bind_param("iii", $cat_id, $social_id, $special_id);
            }
            $st->bind_result($count);
            $st->execute();
            $st->store_result();
            $st->fetch();
            if ($count > $max) {

                $this->conn->rollBack();
                return getResponse("متاسفانه ظرفیت این جایگاه در حال حاضر تکمیل  شده است . ", 409);  // tedade jaygaha vase in qestmat por shode
            }
            // age na mirim sorqe edame va taqriban dige moshkeli nabayd bashe vase reserve
            // khob inja noe pardakht ro 1 mizarim yani az gold pardakht mishe
            $pay_type = 1;
            $st = $this->conn->prepare("INSERT INTO special_order(page_id, user_id, special_id, pay_type, gold) VALUE(?,?,?,?,?)");
            $st->bind_param("iiiii", $page_id, $user_id, $special_id, $pay_type, $gold);
            if (!$st->execute()) {
                $this->conn->rollBack();
                getResponse($prefix . "خطا در ثبت . کد خطا : 2  لطفا دوباره تلاش کنید ", 400); // khata to insert kardan reserve initial
            }
            //khob age moshkeli nabashe flage reserve ro 1 mikonim ke yani reserr kamel shode va tarikhhaye shoro va enqeza ro moshakhas mikonim
            // reserve more nazar to jadval ro be rooz mikonim
            $reserved_flag = 1;
            $so_id = $this->conn->insert_id;
            $start_time_obj = new \DateTime(date("Y-m-d H:i:s"));
            $start_time = $start_time_obj->format("Y-m-d H:i:s");
            $hours = $day * 24;
            $exp_time_obj = $start_time_obj->modify(" + $hours hours");
            $exp_time = $exp_time_obj->format("Y-m-d H:i:s");

            $st = $this->conn->prepare("UPDATE special_r SET so_id = ? ,reserved_flag = ? ,start_time = ? , exp_time = ?  WHERE sr_id = ?");
            $st->bind_param("iissi", $so_id, $reserved_flag, $start_time, $exp_time, $sr_id);
            $st->execute();
            if ($st->affected_rows < 0) {
                $this->conn->rollBack();
                getResponse($prefix . "خطا در ثبت . کد خطا : 3  لطفا دوباره تلاش کنید ", 400);
            }

            //dar akhar ham gold ro az hesabe karbar kam mikonim
            $st = $this->conn->prepare("UPDATE users SET inventory = inventory - ? WHERE user_id LIKE ? ");
            $st->bind_param("ii", $gold, $user_id);
            $st->execute();
            if ($st->affected_rows < 0) {
                $this->conn->rollBack();
                getResponse($prefix . "خطا در ثبت . کد خطا : 4  لطفا دوباره تلاش کنید ", 400);
            }


            // dar kol agar hameye inserta va updata dorost bashan pas commit mikonim va peyqam moafaqiat amiz boodane reserv ro midim
            $this->conn->commit();

            return getResponse($prefix .  " شما با موفقیت رزرو گردید برای اطلاع از زمان باقی مانده در حساب کاربری به بخش ویژه شده ها مراجعه نمایید", 201);

        } catch (\PDOException $e) {
            $this->conn->rollBack();
            return getResponse($prefix . "خطا در ثبت . لطفا دوباره تلاش کنید ", 400);

        }

    }

    public function reservByCache($max, $cat_id, $prefix, $is_main, $social_id, $page_id, $special_id, $user_id, $price, $banner_need)
    {
        $sr_id = 0;
        $so_id = 0;
        $respond = array();
        //chon vase daste asli bayad ba cat_id null zakhire she check mikoinm bebin agar is main 1 bood pas cat_id ro null mikonim
        if ($is_main == 1) {
            $cat_id = null;
        }
        if ($banner_need == 1) {

            if ($this->checkPageHasNotBanner($page_id)) {
                $respond["state"] = -5;
                return $respond;
            }

        }

        //first we need to delete previous order that has the sate of 0 so we can make new order
        $st = $this->conn->prepare("Delete from special_r where page_id = ? AND special_id = ? AND reserved_flag = 0");
        $st->bind_param("ii", $page_id, $special_id);
        $st->execute();


        // marhale be marhale mirim jolo agar jai moshkel dasht ba ye error return mikonim vagar na ta akhare reserve ro hamin ja miri
        //check kardane inke qablan reserve nashode bashe
        $st = $this->conn->prepare("SELECT sr_id FROM special_r WHERE page_id = ? AND special_id = ? AND reserved_flag = 1");
        $st->bind_param("ii", $page_id, $special_id);
        $st->execute();
        $result = $st->get_result();
        if ($result->num_rows > 0) {
            $respond["state"] = -1;
            return $respond; // yani in page qablan toye in jaygah sabt shode
        }

        $this->conn->begin_transaction();
        try {
            //chon date objecte va string nist bayad zaman hamoon ro tabdin be string koinm
            $reserved_flag = 0;
            $resereved_time_obj = new \DateTime(date("Y-m-d H:i:s"));
            $reserved_time = $resereved_time_obj->format("Y-m-d H:i:s");
            $reserved_exp_obj = $resereved_time_obj->modify(" + 11 minutes");
            $reserved_exp = $reserved_exp_obj->format("Y-m-d H:i:s");

            // in marhale aval miam ye reserv ebtedai ba flage 0  mikonim va badesh mibnim ke aya tedadesh az tedade maxe special bishtar mishe ya na
            $st = $this->conn->prepare("INSERT INTO special_r(page_id, user_id, cat_id, social_id, special_id, reserved_flag, reserved_time, reserved_exp)
                  VALUES(?,?,?,?,?,?,?,?)");
            $st->bind_param("iiiiiiss", $page_id, $user_id, $cat_id, $social_id, $special_id, $reserved_flag, $reserved_time, $reserved_exp);
            if (!$st->execute()) {
                $this->conn->rollBack();
                $respond["state"] = -2;
                return $respond;
                // khata to insert kardan reserve initial
            }
            $sr_id = $this->conn->insert_id;

            //age tedad bishtar shod pas kolan roll back mishe va peyqam takmil zarfiat midim

            if ($is_main) {
                $st = $this->conn->prepare("SELECT count(*) AS count  FROM special_r WHERE social_id = ? AND special_id = ? AND reserved_flag < 2");
                $st->bind_param("ii", $social_id, $special_id);
            } else {
                $st = $this->conn->prepare("SELECT count(*) AS count  FROM special_r WHERE cat_id = ? AND social_id = ? AND special_id = ? AND reserved_flag < 2");
                $st->bind_param("iii", $cat_id, $social_id, $special_id);
            }

            $st->bind_result($count);
            $st->execute();
            $st->store_result();
            $st->fetch();
            if ($count > $max) {
                $this->conn->rollBack();
                $respond["state"] = -3;
                return $respond; // tedade jaygaha vase in qestmat por shode
            }
            // age na mirim sorqe edame va moshkeli baraye rezerv vojod nadarad
            // khob inja noe pardakht ro 1 mizarim yani az gold pardakht mishe
            $pay_type = 2;
            $st = $this->conn->prepare("INSERT INTO special_order(page_id, user_id, special_id, pay_type, price) VALUE(?,?,?,?,?)");
            $st->bind_param("iiiii", $page_id, $user_id, $special_id, $pay_type, $price);
            if (!$st->execute()) {
                $this->conn->rollBack();
                $respond["state"] = -4;
                return $respond; // khata to insert kardan reserve initial
            }

            //khob age moshkeli nabashe flage reserve ro 1 mikonim ke yani reserr kamel shode va tarikhhaye shoro va enqeza ro moshakhas mikonim
            // reserve more nazar to jadval ro be rooz mikonim
            $so_id = $this->conn->insert_id;
            $this->conn->commit();
            $respond["state"] = 1;
            $respond["so_id"] = $so_id;
            $respond["sr_id"] = $sr_id;
            return $respond;


        } catch (\PDOException $e) {
            $this->conn->rollBack();
            $respond["state"] = -4;
            return $respond;

        }

    }


    public function reserveTransUpdate($trans_id, $order_id, $sr_id)
    {
        $state = 1;
        $stmt = $this->conn->prepare("UPDATE special_order SET trans_id = ? , state = ? WHERE so_id = ?");
        $stmt->bind_param("sii", $trans_id, $state, $order_id);
        $stmt->execute();

        $stmt = $this->conn->prepare("UPDATE special_r SET so_id = ?  WHERE sr_id = ?");
        $stmt->bind_param("ii", $order_id, $sr_id);
        $stmt->execute();
        return $this->conn->affected_rows > 0;
    }


    public function checkReserveTransaction($trans_id, $order_id)
    {
        $response = array();
        $stmt = $this->conn->prepare("SELECT  special_id, page_id , price , state  FROM special_order WHERE trans_id LIKE ? AND so_id = ?");
        $stmt->bind_param("si", $trans_id, $order_id);
        $stmt->bind_result($special_id, $page_id, $price, $state);
        $stmt->execute();
        $stmt->store_result();


        if ($stmt->num_rows > 0) {

            $stmt->fetch();
            if ($state == 6) {
                $response["state"] = 1;  // yani vaziaate trakonesh qablan moshakhas shode;

                return $response;
            } else {

                $stmt = $this->conn->prepare("SELECT title from special where special_id like ?");
                $stmt->bind_param("i", $special_id);
                $stmt->bind_result($title);
                $stmt->execute();
                $stmt->store_result();
                $stmt->fetch();


                $response["state"] = 2;  // yani vaziatesh moshakhsh nashode va mojode ke mirim moshakhasesh koinm
                $response["special_id"] = $special_id;
                $response["page_id"] = $page_id;
                $response["price"] = $price;
                $response["title"] = $title;
                return $response;
            }

        } else {
            $response["state"] = 0;  // yani taraconesh namotabare va nist to jadval;
            return $response;
        }
    }

    public function reserveSuccess($order_id, $day)
    {
        $response = array();
        $reserved_flag = 1;
        $sr_id = $this->getSr_id($order_id);
        $start_time_obj = new \DateTime(date("Y-m-d H:i:s"));
        $start_time = $start_time_obj->format("Y-m-d H:i:s");
        $hours = $day * 24;
        $exp_time_obj = $start_time_obj->modify(" + $hours hours");
        $exp_time = $exp_time_obj->format("Y-m-d H:i:s");
        $st = $this->conn->prepare("UPDATE special_r SET  reserved_flag = ? ,start_time = ? , exp_time = ?  WHERE sr_id = ?");
        $st->bind_param("issi", $reserved_flag, $start_time, $exp_time, $sr_id);
        $st->execute();
        $response["start_time"] = $start_time_obj->getTimestamp();
        $response["exp_time"] = $exp_time_obj->getTimestamp();
        return $response;
    }

    public function updateReserveState($state, $order_id)
    {
        $stmt = $this->conn->prepare("UPDATE special_order SET state = ? WHERE so_id = ?");
        $stmt->bind_param("ii", $state, $order_id);
        $stmt->execute();
    }

    public function updateReserveCode($code, $order_id)
    {
        $stmt = $this->conn->prepare("UPDATE special_order SET code = ? WHERE so_id = ?");
        $stmt->bind_param("ii", $code, $order_id);
        $stmt->execute();
    }

    // bade inke order id toye call back barmigarde az tariqe on miam o sr_id ro peyda mikonim vase  berooz resani nahai


    // aaz esmesh maloolme
    public function checkPageHasNotBanner($page_id)
    {
        $st = $this->conn->prepare("SELECT banner FROM page WHERE page_id = ?");
        $st->bind_param("i", $page_id);
        $st->bind_result($banner);
        $st->execute();
        $st->store_result();
        $st->fetch();

        return is_null($banner);
    }


    public function getSr_id($order_id)
    {
        $st = $this->conn->prepare("SELECT sr_id FROM special_r WHERE so_id = ?");
        $st->bind_param("i", $order_id);
        $st->bind_result($sr_id);
        $st->execute();
        $st->store_result();
        $st->fetch();
        return $sr_id;
    }

    public function deleteSr($order_id)
    {
        $sr_id = $this->getSr_id($order_id);
        $st = $this->conn->prepare("DELETE FROM special_r WHERE sr_id = ?");
        $st->bind_param("i", $sr_id);
        $st->execute();
    }

    ////////////////////////////////////////////////////////////////////wordReserve\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\



    public function getWordReserveNeeds($page_id,$word)
    {
        $s=$this->conn->prepare("select social_id from page where page_id like ? ");
        $s->bind_param("i",$page_id);
        $s->bind_result($social_id);
        $s->execute();
        $s->store_result();
        $s->fetch();



        $s = $this->conn->prepare("select wr_id from word_r where word like ? and social_id like ?");
        $s->bind_param("si",$word,$social_id );
        $s->execute();
        $result = $s->get_result();

        if ($result->num_rows> 0 ) {
            return 0;
        }else {
            return $social_id;
        }


    }

    public function reserveWord($page_id, $social_id, $user_id, $word, $gold, $day)
    {

        if (!$this->isPageActive($page_id)) {
            return getResponse("صفحه یا کانال مورد نظر هنوز تایید نشده است", 401);
        }
        $inventory = $this->getInventory($user_id);

        if ($inventory < $gold) {
            return getResponse("موجودی برای رزرو جایگاه کافی نیست", 401);
        }
        $this->conn->begin_transaction();
        try {
            $start_time_obj = new \DateTime(date("Y-m-d H:i:s"));
            $start_time = $start_time_obj->format("Y-m-d H:i:s");
            $hours = $day * 24;
            $exp_time_obj = $start_time_obj->modify(" + $hours hours");
            $exp_time = $exp_time_obj->format("Y-m-d H:i:s");
            $s = $this->conn->prepare("INSERT INTO word_order(page_id, user_id, word, gold, day, start_time, exp_time) VALUE(?,?,?,?,?,?,?)");
            $s->bind_param("iisiiss", $page_id, $user_id, $word, $gold, $day, $start_time, $exp_time);
            if (!$s->execute()) {
                $this->conn->rollBack();
                return getResponse("خطا در ثبت !لطفا دوباره تلاش کنید", 400);
            }
            $wo_id = $this->conn->insert_id;
            $s = $this->conn->prepare("INSERT INTO word_r(page_id,user_id, social_id, wo_id, word, start_time, exp_time) VALUE(?,?,?,?,?,?,?)");
            $s->bind_param("iiiisss", $page_id, $user_id, $social_id, $wo_id, $word, $start_time, $exp_time);
            if (!$s->execute()) {
                $this->conn->rollBack();
                return getResponse("خطا در رزرو جایگاه", 400);
            }

            $s = $this->conn->prepare("UPDATE users SET inventory = inventory - ? WHERE user_id LIKE ? ");
            $s->bind_param("ii", $gold, $user_id);
            $s->execute();
            if ($s->affected_rows < 0) {
                $this->conn->rollBack();
                getResponse("خطا در رزرو جایگاه . کد خطا : 4  لطفا دوباره تلاش کنید ", 400);
            }
            $this->conn->commit();
            $response = array();
            $response[STATUS] = 200;
            $response["exp_time"] = $exp_time;
            $response["start_time"] = $start_time;
            $response["gold"] = $gold;
            $response["word"] = $word;
            return $response;

        } catch (\PDOException $e) {
            $this->conn->rollBack();
            return getResponse("خطا در ثبت . لطفا دوباره تلاش کنید ", 400);
        }

    }


    public function reserveWordByCache($social_id, $page_id, $user_id, $price,$word,$day)
    {
        $wr_id = 0;
        $wo_id = 0;
        $respond = array();
        //chon vase daste asli bayad ba cat_id null zakhire she check mikoinm bebin agar is main 1 bood pas cat_id ro null mikonim

        //first we need to delete previous order that has the sate of 0 so we can make new order
        $st = $this->conn->prepare("Delete from word_r where page_id = ? and word like ? AND reserved_flag = 0");
        $st->bind_param("is", $page_id, $word);
        $st->execute();


        // marhale be marhale mirim jolo agar jai moshkel dasht ba ye error return mikonim vagar na ta akhare reserve ro hamin ja miri
        //check kardane inke qablan reserve nashode bashe
        $st = $this->conn->prepare("SELECT wr_id FROM word_r WHERE word = ? AND social_id = ? AND reserved_flag = 1");
        $st->bind_param("si", $word, $social_id);
        $st->execute();
        $result = $st->get_result();
        if ($result->num_rows > 0) {
            $respond["state"] = -1;
            return $respond; // the word is already reserved
        }

        $this->conn->begin_transaction();
        try {
            //chon date objecte va string nist bayad zaman hamoon ro tabdin be string koinm
            $reserved_flag = 0;
            $reserved_bank_obj = new \DateTime(date("Y-m-d H:i:s"));
            $reserverd_bank = $reserved_bank_obj->format("Y-m-d H:i:s");
            $exp_bank_obj = $reserved_bank_obj->modify(" + 11 minutes");
            $exp_bank = $exp_bank_obj->format("Y-m-d H:i:s");

            // in marhale aval miam ye reserv ebtedai ba flage 0  mikonim va badesh mibnim ke aya tedadesh az tedade maxe special bishtar mishe ya na
            $st = $this->conn->prepare("INSERT INTO word_r(page_id, user_id, social_id,word, reserved_flag, start_bank, finish_bank)
                  VALUES(?,?,?,?,?,?,?)");
            $st->bind_param("iiisiss", $page_id, $user_id, $social_id,$word, $reserved_flag, $reserverd_bank, $exp_bank);



            if (!$st->execute()) {
                $this->conn->rollBack();
                $respond["state"] = -2;
                return $respond;
                // khata to insert kardan reserve initial
            }
            $wr_id = $this->conn->insert_id;


            // age na mirim sorqe edame va moshkeli baraye rezerv vojod nadarad
            // khob inja noe pardakht ro 1 mizarim yani az gold pardakht mishe
            $pay_type = 2;
            $st = $this->conn->prepare("INSERT INTO word_order(page_id, user_id, word, pay_type, price,day) VALUE(?,?,?,?,?,?)");
            $st->bind_param("iisiii", $page_id, $user_id,$word, $pay_type, $price,$day);
            if (!$st->execute()) {
                $this->conn->rollBack();
                $respond["state"] = -4;
                return $respond; // khata to insert kardan reserve initial
            }

            //khob age moshkeli nabashe flage reserve ro 1 mikonim ke yani reserr kamel shode va tarikhhaye shoro va enqeza ro moshakhas mikonim
            // reserve more nazar to jadval ro be rooz mikonim
            $wo_id = $this->conn->insert_id;
            $this->conn->commit();

            $respond["state"] = 1;
            $respond["wo_id"] = $wo_id;
            $respond["wr_id"] = $wr_id;

            return $respond;


        } catch (\PDOException $e) {
            $this->conn->rollBack();
            $respond["state"] = -4;
            return $respond;

        }

    }

    public function reserveWordTransUpdate($trans_id, $wo_id, $wr_id)
    {
        $state = 1;
        $stmt = $this->conn->prepare("UPDATE word_order SET trans_id = ? , state = ? WHERE wo_id = ?");
        $stmt->bind_param("sii", $trans_id, $state, $wo_id);
        $stmt->execute();

        $stmt = $this->conn->prepare("UPDATE word_r SET wo_id = ?  WHERE wr_id = ?");
        $stmt->bind_param("ii", $wo_id, $wr_id);
        $stmt->execute();
        return $this->conn->affected_rows > 0;
    }


    public function getWr_id($order_id)
    {
        $st = $this->conn->prepare("SELECT wr_id FROM word_r WHERE wo_id = ?");
        $st->bind_param("i", $order_id);
        $st->bind_result($wr_id);
        $st->execute();
        $st->store_result();
        $st->fetch();
        return $wr_id;
    }

    public function deleteWr($order_id)
    {
        $wr_id = $this->getWr_id($order_id);
        $st = $this->conn->prepare("DELETE FROM word_r WHERE wr_id = ?");
        $st->bind_param("i", $wr_id);
        $st->execute();
    }

    public function updateWordReserveState($state, $order_id)
    {
        $stmt = $this->conn->prepare("UPDATE word_order SET state = ? WHERE wo_id = ?");
        $stmt->bind_param("ii", $state, $order_id);
        $stmt->execute();
    }

    public function updateWordReserveCode($code, $order_id)
    {
        $stmt = $this->conn->prepare("UPDATE word_order SET code = ? WHERE wo_id = ?");
        $stmt->bind_param("ii", $code, $order_id);
        $stmt->execute();
    }

    public function checkWordReserveTransaction($trans_id, $order_id)
    {
        $response = array();
        $stmt = $this->conn->prepare("SELECT   page_id , price , state  FROM word_order WHERE trans_id LIKE ? AND wo_id = ?");
        $stmt->bind_param("si", $trans_id, $order_id);
        $stmt->bind_result( $page_id, $price, $state);
        $stmt->execute();
        $stmt->store_result();


        if ($stmt->num_rows > 0) {

            $stmt->fetch();
            if ($state == 6) {
                $response["state"] = 1;  // yani vaziaate trakonesh qablan moshakhas shode;
                return $response;
            } else {

                $response["state"] = 2;  // yani vaziatesh moshakhsh nashode va mojode ke mirim moshakhasesh koinm
                $response["page_id"] = $page_id;
                $response["price"] = $price;

                return $response;
            }

        } else {
            $response["state"] = 0;  // yani taraconesh namotabare va nist to jadval;
            return $response;
        }
    }

    public function getWordNeedsInBack( $page_id,$order_id)
    {
        $needs = array();
        $st = $this->conn->prepare("SELECT word,day FROM word_order WHERE wo_id = ?");
        $st->bind_param("i", $order_id);
        $st->bind_result($word, $day);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $needs["word"] = $word;
        $needs["day"] = $day;

        $st = $this->conn->prepare("SELECT p.name,s.prefix FROM page p JOIN sociall s ON p.social_id = s.social_id 
                                            WHERE page_id = ?");
        $st->bind_param("i", $page_id);
        $st->bind_result($page_name, $prefix);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        $needs["prefix"] = $prefix;
        $needs["page_name"] = $page_name;
        return $needs;

    }


    public function reserveWordSuccess($order_id, $day)
    {
        $response = array();
        $reserved_flag = 1;
        $wr_id = $this->getWr_id($order_id);
        $start_time_obj = new \DateTime(date("Y-m-d H:i:s"));
        $start_time = $start_time_obj->format("Y-m-d H:i:s");
        $hours = $day * 24;
        $exp_time_obj = $start_time_obj->modify(" + $hours hours");
        $exp_time = $exp_time_obj->format("Y-m-d H:i:s");
        $st = $this->conn->prepare("UPDATE word_r SET  reserved_flag = ? ,start_time = ? , exp_time = ?  WHERE wr_id = ?");
        $st->bind_param("issi", $reserved_flag, $start_time, $exp_time, $wr_id);
        $st->execute();
        $response["start_time"] = $start_time_obj->getTimestamp();
        $response["exp_time"] = $exp_time_obj->getTimestamp();
        return $response;
    }


    ////////////////////////////////////////////////////RecivingFunction\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function get_P_Profile($user_id)
    {
        $st = $this->conn->prepare("SELECT  REPLACE(mobile, SUBSTRING(mobile, 5, 4), '****') AS mobile ,email,t_id,username , name , profile , thumb, bio, sex  FROM user_info WHERE 
                                                   user_id LIKE ? ");
        $st->bind_param("i", $user_id);
        $st->execute();
        $result = $st->get_result();
        $st->close();
        return $result->fetch_assoc();
    }

    public function get_P_Upage($user_id, $social_id, $limit, $page, $prefix)
    {
        $response = array();

        $offset = ($page - 1) * $limit;
        $s = $this->conn->prepare("SELECT p . page_id,p . user_id,p . name,p . id,p . short_des,p . thumb,p . pic,p . banner,p . member,p . view,
                                      l . social_id,l . uri,l . package,l . p_name,l . prefix,l . member_prefix from page p  
                                        join sociall l   on p . social_id = l . social_id where p . social_id = ? and p . active > 0
            and p . user_id like ?
                limit $offset,$limit");
        $s->bind_param("ii", $social_id, $user_id);
        $s->execute();
        $result = $s->get_result();
        $s->close();
        if ($result->num_rows > 0) {
            $response["ppages"] = array();
            $response[STATUS] = 200;
            while ($single = $result->fetch_assoc()) {
                array_push($response["ppages"], $single);
            }


            return $response;
        } else {
            if ($page == 1) {
                return getResponse("بدون " . $prefix . " ثبت شده توسط این کاربر", 400);
            } else {
                return getResponse($prefix . " دیگری یافت نشد ", 404);
            }

        }

    }

    public function get_P_Specials($page_id)
    {

        $final = array();
        $st = $this->conn->prepare("SELECT s.social_id,s.prefix,s.p_name,c.cat_id,c.name cat_name  
                                   FROM page p join sociall s on p.social_id=s.social_id join category c
                                   on p.cat_id = c.cat_id WHERE page_id LIKE ?");
        $st->bind_param("i", $page_id);
        $st->bind_result($social_id, $prefix, $p_name, $cat_id, $cat_name);
        $st->execute();
        $st->store_result();
        $st->fetch();

        $response = array();
        $response["title"] = $prefix . " های ویژه " . $p_name . " " . $cat_name;
        $response["pages"] = array();

        $st = $this->conn->prepare("SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic, p . banner,p . member,p . view,l . uri,l . package,l . p_name,l . prefix,
                          l . member_prefix  FROM page p JOIN(SELECT  page_id, reserved_flag FROM special_r WHERE reserved_flag > 0 AND special_id = 4
            AND social_id = ? AND cat_id = ? ORDER BY reserved_flag,exp_time DESC LIMIT 0,8) s ON p . page_id = s . page_id JOIN sociall l ON p . social_id = l . social_id
                              where p . active > 0");

        $st->bind_param("ii", $social_id, $cat_id);
        $st->execute();
        $result = $st->get_result();
        while ($single = $result->fetch_assoc()) {
            array_push($response["pages"], $single);
        }
        array_push($final, $response);


        $response = array();
        $response["title"] = $prefix . " های ویژه " . $p_name;
        $response["pages"] = array();

        $st = $this->conn->prepare("SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic, p . banner,p . member,p . view,l . uri,l . package,l . p_name,l . prefix,
                          l . member_prefix  FROM page p JOIN(SELECT  page_id, reserved_flag FROM special_r WHERE reserved_flag > 0 AND special_id = 2
            AND social_id = ?  ORDER BY reserved_flag,exp_time DESC LIMIT 0,8) s ON p . page_id = s . page_id JOIN sociall l ON p . social_id = l . social_id
                              where p . active > 0");

        $st->bind_param("i", $social_id);
        $st->execute();
        $result = $st->get_result();
        while ($single = $result->fetch_assoc()) {
            array_push($response["pages"], $single);
        }
        array_push($final, $response);


        $st->close();
        return $final;

    }

     public function get_P_Detail($page_id, $user_id)
    {
        // 3 marhale

        //1 geretane eletelate category
        $st = $this->conn->prepare("SELECT  c.cat_id, c.name AS cat_name ,c.icon  AS cat_icon , c.en_name                                                                                      
                                                FROM category c WHERE cat_id = (SELECT cat_id FROM page WHERE page_id = ?)");
        $st->bind_param("i", $page_id);
        $st->execute();
        $result = $st->get_result();

        $detail = $result->fetch_assoc();

        //2 geretane des page

        $st = $this->conn->prepare("SELECT short_url,aparat,view,des,initial,user_id,name,id,id_2,short_des,thumb,pic,banner,member FROM page WHERE page_id =? ");
        $st->bind_param("i", $page_id);
        $st->execute();
        $result_detail =  $st->get_result()->fetch_assoc();
        $detail = array_merge($detail,$result_detail);

        //4 geretane baqie detail

        $st= $this->conn->prepare("SELECT  Round(AVG(rate),1) rate , COUNT(*) as voters ,IF ((SELECT COUNT(*) FROM page_rate
                                                   WHERE page_id = ? AND user_id = ? ) ,(SELECT rate from page_rate WHERE user_id = ? and page_id = ?
                                           ) , null) AS myrate  FROM page_rate where page_id = ?");
        $st->bind_param("iiiii",$page_id,$user_id,$user_id,$page_id,$page_id);
        $st->execute();
        $rating=$st->get_result()->fetch_assoc();
        $detail["rating"]=$rating;


        $st = $this->conn->prepare("SELECT 
         COUNT(*) like_cnt , IF ((SELECT COUNT(*) FROM likes WHERE page_id = ? AND user_id = ? )  , 1 , 0) AS mylike
                                            FROM likes WHERE page_id = ?");
        $st->bind_param("iii", $page_id, $user_id, $page_id);
        $st->bind_result($like_cnt, $mylike);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        $likes = $detail["initial"] + $like_cnt;
        $detail['likes_cnt'] = $likes;
        $detail['user_like'] = $mylike;

        $st = $this->conn->prepare("SELECT COUNT(*) AS cm_cnt FROM comment WHERE page_id = ? AND active = 1");
        $st->bind_param("i", $page_id);
        $st->bind_result($cm_cnt);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        $detail['comments_cnt'] = $cm_cnt;


        $st = $this->conn->prepare("SELECT COUNT(*) AS cm_cnt FROM comment WHERE page_id = ? AND active = 1");
        $st->bind_param("i", $page_id);
        $st->bind_result($cm_cnt);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        $detail['comments_cnt'] = $cm_cnt;

        //getting tags
        $st = $this->conn->prepare("select tag_id , name from tags where tag_id in (select tag_id from tag_page where page_id like ?)");
        $st->bind_param("i", $page_id);
        $st->execute();
        $result_tags = $st->get_result();
        $detail['tags'] = array();
        while ($single_tag = $result_tags->fetch_assoc()) {
            array_push($detail['tags'], $single_tag);
        }

        //getting screends
        $st = $this->conn->prepare("select ps_id , name , thumb_name from page_screen where page_id like ? and active = 1");
        $st->bind_param("i", $page_id);
        $st->execute();
        $result_screens = $st->get_result();
        $detail['screens'] = array();
        while ($single_screen = $result_screens->fetch_assoc()) {
            array_push($detail['screens'], $single_screen);
        }


        $final = array();

        $st = $this->conn->prepare("SELECT s.social_id,s.prefix,s.p_name,s.prefix_p
                                   FROM page p join sociall s on p.social_id=s.social_id WHERE page_id LIKE ?");
        $st->bind_param("i", $page_id);
        $st->bind_result($social_id, $prefix, $p_name,$prefix_p);
        $st->execute();
        $st->store_result();
        $st->fetch();

        $detail["prefix"]=$prefix;
        $detail["prefix_p"]=$prefix_p;
        $detail["p_name"]=$p_name;

        $response = array();
        $response["title"] = $prefix . " های ویژه " . $p_name . " " . $detail["cat_name"];
        $response["pages"] = array();
        $st = $this->conn->prepare("SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic, p . banner,p . member,p . view,l . uri,l . package,l . p_name,l . prefix,
                          l . member_prefix  FROM page p JOIN(SELECT  page_id, reserved_flag FROM special_r WHERE reserved_flag > 0 AND special_id = 4
            AND social_id = ? AND cat_id = ? ORDER BY reserved_flag,exp_time DESC LIMIT 0,8) s ON p . page_id = s . page_id JOIN sociall l ON p . social_id = l . social_id
                              where p . active > 0");
        $st->bind_param("ii", $social_id, $detail["cat_id"]);
        $st->execute();
        $result = $st->get_result();
        while ($single = $result->fetch_assoc()) {
            array_push($response["pages"], $single);
        }
        array_push($final, $response);
        $response = array();
        $response["title"] = $prefix . " های ویژه " . $p_name;
        $response["pages"] = array();
        $st = $this->conn->prepare("SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic, p . banner,p . member,p . view,l . uri,l . package,l . p_name,l . prefix,
                          l . member_prefix  FROM page p JOIN(SELECT  page_id, reserved_flag FROM special_r WHERE reserved_flag > 0 AND special_id = 2
            AND social_id = ?  ORDER BY reserved_flag,exp_time DESC LIMIT 0,8) s ON p . page_id = s . page_id JOIN sociall l ON p . social_id = l . social_id
                              where p . active > 0");

        $st->bind_param("i", $social_id);
        $st->execute();
        $result = $st->get_result();
        while ($single = $result->fetch_assoc()) {
            array_push($response["pages"], $single);
        }
        array_push($final, $response);
        $detail["reserved"] = $final;
        $detail["reports"] = array();
        $st = $this->conn->prepare("select rt_id , text from reports_title");
        $st->execute();
        $result = $st->get_result();
        while ($single = $result->fetch_assoc()) {
            array_push($detail["reports"], $single);
        }
        return $detail;
    }

    public function get_P_sections($main, $cat_id, $social_id, $prefix)
    {
        $final = array();


        $st=$this->conn->prepare("select p_name,prefix_p from sociall where social_id like ?");
        $st->bind_param("i",$social_id);
        $st->bind_result($social_name,$prefix_p);
        $st->execute();
        $st->store_result();
        $st->fetch();
        if ($main!=1) {
            $st = $this->conn->prepare("select name from category where cat_id like ?");
            $st->bind_param("i", $cat_id);
            $st->bind_result($cat_name);
            $st->execute();
            $st->store_result();
            $st->fetch();
        }

        //////======================================================================******==================================================================\\\\\\\\\\\
        //gereftane bannerha
        $banners = array();
        // agar main 1 bood bayad onai  ro begirim ke specialshon 1 hast va cat_id nadaran  va social id ke harche ke bood
        // dar qeyre insoort bayad bayad  bannerhai ro begrim ke special 3 daran va cat_id ham daran va social idshon har che ke hast
        if ($main == 1) {
            $query = "SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic,p . banner,p . member,p . view,l . social_id,l . uri,l . package,l . p_name,l . prefix,l . member_prefix 
                     FROM page p JOIN(SELECT  page_id, reserved_flag FROM special_r WHERE reserved_flag > 0 AND special_id = 1
            AND social_id = ? ORDER BY reserved_flag,exp_time DESC LIMIT 0,4) s ON p . page_id = s . page_id JOIN sociall l ON p . social_id = l . social_id 
              where p . active > 0
              ";
        } else {
            $query = "SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic, p . banner,p . member,p . view,l . uri,l . package,l . p_name,l . prefix,l . member_prefix 
                     FROM page p JOIN(SELECT  page_id, reserved_flag FROM special_r WHERE reserved_flag > 0 AND special_id = 3
            AND social_id = ? AND cat_id = ? ORDER BY reserved_flag ,exp_time DESC LIMIT 0,4) s ON p . page_id = s . page_id JOIN sociall l ON p . social_id = l . social_id 
               where p . active > 0
              ";
        }
        $st = $this->conn->prepare($query);
        if ($main == 1) {
            $st->bind_param("i", $social_id);
        } else {
            $st->bind_param("ii", $social_id, $cat_id);
        }
        $st->execute();
        $result = $st->get_result();
        $banners_temp = array();
        while ($single = $result->fetch_assoc()) {
            array_push($banners_temp, $single);
        }
        $banners["title"] = "بنرها";
        $banners["key"] = "banners"; // in vase query vase gereftane hameye list estefade mishe va karbodi vase bannera nadare
        $banners["special"] = 1;   // taiin mikone ke aya jozve vijehast ya na age bashe khob textview همه hide mishe va ya range text view title taqir mikone
        $banners["type"] = 1;  // inam typesh ro moshakhash mokone ke 1 mishe banner ke vase view pager estefde mishe  2 mishe list page
        $banners["pages"] = $banners_temp;
        $banners["detail"] = 1;  // in flag moshakas mikone toye list masalan agar 1 bashe tedade aza neshon dade beshe agar 2 bashe tedade view
        //////======================================================================******==================================================================\\\\\\\\\\\


        /// gerefnae safahati ke bishtarin karbar ro daran

        $max_members = array();
        if ($main == 1) {
            $max_member_query = "SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic,p . banner,p . member,p . view,l . social_id,l . uri,l . package,l . p_name,l . prefix,l . member_prefix 
                     FROM page p  JOIN sociall l ON p . social_id = l . social_id WHERE p . social_id = ? AND p . active > 0 ORDER BY member DESC LIMIT 0,8";
        } else {
            $max_member_query = "SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic, p . banner,p . member,p . view,l . uri,l . package,l . p_name,l . prefix,l . member_prefix 
                     FROM page p  JOIN sociall l ON p . social_id = l . social_id WHERE p . social_id = ? AND p . cat_id = ? AND p . active > 0 ORDER BY member DESC LIMIT 0,8";
        }
        $st = $this->conn->prepare($max_member_query);
        if ($main) {
            $st->bind_param("i", $social_id);
        } else {
            $st->bind_param("ii", $social_id, $cat_id);
        }
        $st->execute();
        $result = $st->get_result();
        $max_member_temp = array();
        while ($single_max_member = $result->fetch_assoc()) {
            array_push($max_member_temp, $single_max_member);
        }

        if ($main==1) {
            $max_members["title"] = "محبوب ترین " . $prefix_p . " "  .$social_name;
        }else {
            $max_members["title"] = "محبوب ترین " . $prefix_p . " " .$cat_name . " ".$social_name ;
        }
        $max_members["key"] = "maxMember";
        $max_members["special"] = 0;
        $max_members["type"] = 2;
        $max_members["pages"] = $max_member_temp;
        $max_members["detail"] = 1;  // in flag moshakas mikone toye list masalan agar 1 bashe tedade aza neshon dade beshe agar 2 bashe tedade view
        //////======================================================================******==================================================================\\\\\\\\\\\
        $specials = array();
        if ($main == 1) {
            $query = "SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic,p . banner,p . member,p . view,l . social_id,l . uri,l . package,l . p_name,l . prefix,l . member_prefix 
                     FROM page p JOIN(SELECT  page_id, reserved_flag FROM special_r WHERE reserved_flag > 0 AND special_id = 2
            AND social_id = ? ORDER BY reserved_flag,exp_time DESC LIMIT 0,8) s ON p . page_id = s . page_id JOIN sociall l ON p . social_id = l . social_id 
               where p . active > 0
              ";
        } else {
            $query = "SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic, p . banner,p . member,p . view,l . uri,l . package,l . p_name,l . prefix,l . member_prefix 
                     FROM page p JOIN(SELECT  page_id, reserved_flag FROM special_r WHERE reserved_flag > 0 AND special_id = 4
            AND social_id = ? AND cat_id = ? ORDER BY reserved_flag,exp_time DESC LIMIT 0,8) s ON p . page_id = s . page_id JOIN sociall l ON p . social_id = l . social_id 
              WHERE p . active > 0
              ";
        }
        $st = $this->conn->prepare($query);
        if ($main == 1) {
            $st->bind_param("i", $social_id);
        } else {
            $st->bind_param("ii", $social_id, $cat_id);
        }
        $st->execute();
        $result = $st->get_result();
        $special_temp = array();
        while ($single = $result->fetch_assoc()) {
            array_push($special_temp, $single);
        }

        if ($main==1) {
            $specials["title"] = $prefix_p . " <mark class='special-color'>ویژه</mark> ".$social_name;
        }else {
            $specials["title"] = $prefix_p . " <mark class='special-color'>ویژه</mark> " .$cat_name. " ". $social_name;
        }
        $specials["key"] = "specials";
        $specials["special"] = 1;   // taiin mikone ke aya jozve vijehast ya na age bashe khob textview همه hide mishe va ya range text view title taqir mikone
        $specials["type"] = 2;  // inam typesh ro moshakhash mokone ke 1 mishe banner ke vase view pager estefde mishe  2 mishe list page
        $specials["pages"] = $special_temp;
        $specials["detail"] = 1;  // in flag moshakas mikone toye list masalan agar 1 bashe tedade aza neshon dade beshe agar 2 bashe tedade view


        /// gerefnae safahati ke bishtarin bazdid  ro daran
        //////======================================================================******==================================================================\\\\\\\\\\\
        //        $max_view = array();
//        if ($main == 1) {
//            $max_view_query = "SELECT p . page_id,p . user_id, p . name,p . id,p . short_des,p . thumb,p . pic,p . banner,p . member,p . view,l . social_id,l . uri,l . package,l . p_name,l . prefix,l . member_prefix
//                     FROM page p  JOIN sociall l ON p.social_id = l.social_id WHERE p.social_id = ? AND p.active > 0 ORDER BY view DESC LIMIT 0,8";
//        } else {
//            $max_view_query = "SELECT p.page_id,p.user_id, p.name,p.id,p.short_des,p.thumb,p.pic, p.banner,p.member,p.view,l.uri,l.package,l.p_name,l.prefix,l.member_prefix
//                     FROM page p  JOIN sociall l ON p.social_id = l.social_id WHERE p.social_id = ? AND p.cat_id = ? AND p.active > 0 ORDER BY view DESC LIMIT 0,8";
//        }
//        $st = $this->conn->prepare($max_view_query);
//        if ($main) {
//            $st->bind_param("i", $social_id);
//        } else {
//            $st->bind_param("ii", $social_id, $cat_id);
//        }
//        $st->execute();
//        $result = $st->get_result();
//        $max_view_temp = array();
//        while ($single_max_view = $result->fetch_assoc()) {
//            array_push($max_view_temp, $single_max_view);
//        }
//        $max_view["title"] = "پربازدیدترین " . $prefix . " ها";
//        $max_view["key"] = "maxView";
//        $max_view["special"] = 0;
//        $max_view["type"] = 2;
//        $max_view["pages"] = $max_view_temp;
//        $max_view["detail"] = 2;  // in flag moshakas mikone toye list masalan agar 1 bashe tedade aza neshon dade beshe agar 2 bashe tedade view
        //////======================================================================******==================================================================\\\\\\\\\\\
        $newest = array();
        if ($main == 1) {
            $max_view_query = "SELECT p.page_id,p.user_id, p.name,p.id,p.short_des,p.thumb,p.pic,p.banner,p.member,p.view,l.social_id,l.uri,l.package,l.p_name,l.prefix,l.member_prefix 
                     FROM page p  JOIN sociall l ON p.social_id = l.social_id WHERE p.social_id = ? AND p.active > 0 ORDER BY p.page_id DESC LIMIT 0,8";
        } else {
            $max_view_query = "SELECT p.page_id,p.user_id, p.name,p.id,p.short_des,p.thumb,p.pic, p.banner,p.member,p.view,l.uri,l.package,l.p_name,l.prefix,l.member_prefix 
                     FROM page p  JOIN sociall l ON p.social_id = l.social_id WHERE p.social_id = ? AND p.cat_id = ? AND p.active > 0 ORDER BY p.page_id DESC LIMIT 0,8";
        }
        $st = $this->conn->prepare($max_view_query);
        if ($main) {
            $st->bind_param("i", $social_id);
        } else {
            $st->bind_param("ii", $social_id, $cat_id);
        }
        $st->execute();
        $result = $st->get_result();
        $newst_temp = array();
        while ($single_max_view = $result->fetch_assoc()) {
            array_push($newst_temp, $single_max_view);
        }
        if ($main==1) {
            $newest["title"] = "جدیدترین ".$prefix_p." ".$social_name;
        }else {
            $newest["title"] = "جدیدترین ".$prefix_p." ".$cat_name . " " .$social_name;
        }
        $newest["key"] = "newest";
        $newest["special"] = 0;
        $newest["type"] = 2;
        $newest["pages"] = $newst_temp;
        $newest["detail"] = 1;  // in flag moshakas mikone toye list masalan agar 1 bashe tedade aza neshon dade beshe agar 2 bashe tedade view


        //////======================================================================******==================================================================\\\\\\\\\\\
        $st->close();
        array_push($final, $banners);
        array_push($final, $max_members);
        array_push($final, $specials);
        //   array_push($final, $max_view);
        array_push($final, $newest);
        return $final;
    }

    public
    function get_P_list($social_id, $key, $main, $cat_id, $page)
    {
        $limit = 15;
        $response = array();
        $title = "";
        // aval check konim vase kodom bakhshe in masalan vase bishtarin karbar
        $order = "";
        if ($key == "maxMember") {
            $order = "p.member";
            $title="محبوب ترین ";
        } else if ($key == "maxView") {
            $order = "p.view";
            $title="پربازدید ترین ";
        } else if ($key == "newest") {
            $order = "p.page_id";
            $title="جدیدترین ";
        } else {
            return getResponse("نوع مشخص شده صحیح نمباشد", 401);
        }

        //to genreate title
        $st=$this->conn->prepare("select p_name,prefix_p from sociall where social_id like ?");
        $st->bind_param("i",$social_id);
        $st->bind_result($social_name,$prefix_p);
        $st->execute();
        $st->store_result();
        $st->fetch();
        if ($main!=1) {
            $st=$this->conn->prepare("select name from category where cat_id like ?");
            $st->bind_param("i",$cat_id);
            $st->bind_result($cat_name);
            $st->execute();
            $st->store_result();
            $st->fetch();
            $title = $title.$prefix_p . " ". $cat_name . " " . $social_name;

        }else {
            $title = $title.$prefix_p   . " " . $social_name;
        }

        //bad miam offset limit ro moshkhas mikoim
        $offset = ($page - 1) * $limit;
        //check monim vase maine ya vase daste bandie khas
        if ($main == 1) {
            $st = $this->conn->prepare("SELECT p.page_id,p.user_id, p.name,p.id,p.short_des,p.thumb,p.pic,p.banner,p.member,p.view,
                                      l.social_id,l.uri,l.package,l.p_name,l.prefix,l.member_prefix from page p  
                                        join sociall l   on p.social_id = l.social_id where p.social_id = ? and p.active > 0 order by $order DESC limit $offset,$limit");
            $st->bind_param("i", $social_id);
        } else {
            $st = $this->conn->prepare("SELECT p.page_id,p.user_id,p.name,p.id,p.short_des,p.thumb,p.pic,p.banner,p.member,p.view,
                                      l.social_id,l.uri,l.package,l.p_name,l.prefix,l.member_prefix from page p  
                                        join sociall l   on p.social_id = l.social_id where p.social_id = ? and  cat_id = ? and p.active > 0 order by $order DESC limit $offset,$limit");
            $st->bind_param("ii", $social_id, $cat_id);
        }
        $st->execute();
        $result = $st->get_result();
        $st->close();


        if ($main==1) {
            $st=$this->conn->prepare("select count(*) from page where social_id = ?");
            $st->bind_param("i",$social_id);
            $st->bind_result($count);
            $st->execute();
            $st->store_result();
            $st->fetch();
        }else {
            $st=$this->conn->prepare("select count(*) from page where social_id = ? and cat_id = ?");
            $st->bind_param("ii",$social_id,$cat_id);
            $st->bind_result($count);
            $st->execute();
            $st->store_result();
            $st->fetch();
        }



            //age item dasht ke barmigardoni
            $response["title"]=$title;
            $response["hasNext"] = $count > $page*$limit ? true : false;
            $response["plist"] = array();
            while ($single = $result->fetch_assoc()) {
                array_push($response["plist"], $single);
            }
            return $response;

    }

    public
    function get_P_search($social_id, $page, $search, $limit)
    {
        $response = array();
        $response["ppages"] = array();
        $has_reserved_word = false;
        if ($page == 1) {
            $st = $this->conn->prepare("SELECT page_id FROM word_r WHERE social_id LIKE ? AND word LIKE ?");
            $st->bind_param("is", $social_id, $search);
            $st->bind_result($page_id);
            $st->execute();
            $st->store_result();
            if ($st->num_rows > 0) {
                $st->fetch();
                $st = $this->conn->prepare("SELECT p.page_id,p.user_id,p.name,p.id,p.short_des,p.thumb,p.pic,p.banner,p.member,p.view,
                                      l.social_id,l.uri,l.package,l.p_name,l.prefix,l.member_prefix FROM page p  
                                        JOIN sociall l  ON p.social_id = l.social_id WHERE p.page_id = ? and p.active > 0");
                $st->bind_param("i", $page_id);
                $st->execute();
                $result = $st->get_result();
                $st->close();
                $single = $result->fetch_assoc();
                $single["ad"] = 1;
                array_push($response["ppages"], $single);
                $has_reserved_word = true;
            }
        }

        $offset = ($page - 1) * $limit;
//        $temp_serch = $search; // vase in negahesh midarim ke toyre respons estefadash konim
//        $search = "%$search%";

        $s = $this->conn->prepare("SELECT p.page_id,p.user_id,p.name,p.id,p.short_des,p.thumb,p.pic,p.banner,p.member,p.view,
                                      l.social_id,l.uri,l.package,l.p_name,l.prefix,l.member_prefix from page p  
                                        join sociall l   on p.social_id = l.social_id where p.social_id = ? and p.active > 0 
                                        and match(name,short_des) against (? in BOOLEAN MODE )
                                        order by p.member  DESC limit $offset,$limit");

        $s->bind_param("is", $social_id, $search);

        $s->execute();
        $result = $s->get_result();
        $s->close();
        while ($single = $result->fetch_assoc()) {
            $single["ad"] = 0;
            array_push($response["ppages"], $single);
        }
        return $response;


    }


    //////////////////////////////////////////////////////************Categories**************\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public
    function getCats()
    {
        $response = array();

        $st = $this->conn->prepare("SELECT cat_id , name ,en_name,  icon   FROM category WHERE active > 0 GROUP BY sort");
        $st->execute();
        $result = $st->get_result();

        if ($result->num_rows == 0) {
            $response['error'] = true;
            $response['message'] = "هیچ دسته ای ثبت نشده است";
            $response['status'] = 403;
            $st->close();
            return $response;

        } else {
            $response['error'] = false;
            $response['status'] = 200;
            $response['cats'] = array();

            while ($single_cat = $result->fetch_assoc()) {
                array_push($response['cats'], $single_cat);
            }
            $st->close();
            return $response;
        }
    }

    public function getPagesWithComments($user_id) {
        $response = array();
        $this->updateCommentReadCount($user_id);
        $s = $this->conn->prepare("SELECT  p.page_id, p.id,p.name , thumb, SUM(CASE WHEN c.active > 0 THEN 1 ELSE 0 END) as count from page p left join comment c
                                                   on p.page_id = c.page_id WHERE p.user_id = ? and p.active > 0  GROUP by p.page_id ");

        $s->bind_param("i", $user_id);
        $s->execute();
        $result = $s->get_result();
        while ($single = $result->fetch_assoc()) {
            array_push($response,$single);
        }
         return $response;

    }


    //////////////////////////////////////////////////////************WEB**************\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    public
    function getDashbord($user_id)
    {

        $s = $this->conn->prepare("SELECT count(*) AS count FROM message WHERE reciver_id LIKE ? ");
        $s->bind_param("i", $user_id);
        $s->bind_result($count_message);
        $s->execute();
        $s->store_result();
        $s->fetch();


        $s = $this->conn->prepare("SELECT count(*) AS comment FROM comment WHERE user_id LIKE ? and active > 0 ");
        $s->bind_param("i", $user_id);
        $s->bind_result($count_comment);
        $s->execute();
        $s->store_result();
        $s->fetch();


        $s = $this->conn->prepare("SELECT read_message,read_comments  FROM user_info WHERE user_id LIKE ? ");
        $s->bind_param("i", $user_id);
        $s->bind_result($read_message,$read_comment);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $s->close();


        $s = $this->conn->prepare("SELECT count(*) AS count FROM page WHERE user_id LIKE ? ");
        $s->bind_param("i", $user_id);
        $s->bind_result($count_pages);
        $s->execute();
        $s->store_result();
        $s->fetch();


        $s = $this->conn->prepare("SELECT inventory  FROM users WHERE user_id LIKE ? ");
        $s->bind_param("i", $user_id);
        $s->bind_result($inventory);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $s->close();


        $response["messages"] = $count_message;
        $response["read"] = $read_message;
        $response["inventory"] = $inventory;
        $response["pages"] = $count_pages;
        $response["comments"] = $count_comment;
        $response["read_comment"] = $read_comment;

        return $response;


    }


    ////////////////////////////////////////////////////////////////////////////////////////Utils\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public
    function isUserOwner($page_id, $user_id)
    {
        $st = $this->conn->prepare("select user_id from page where page_id = ?");
        $st->bind_param("i", $page_id);
        $st->bind_result($user_id_);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        return $user_id == $user_id_;
    }

    public
    function code_error($error_code)
    {
        $error_code = intval($error_code);
        $error_array = array(
            0 => "تراکنش با موفقیت انجام شد",
            -1 => "تراکنش در وضعیت آماده برای ارسال به بانک است",
            -2 => "تراکنش به بانک ارسال شده و درحال پرداخت توسط خریدار است",
            -3 => "هنوز پاسخی در خصوص نتیجه تراکنش از بانک دریافت نشده است",
            -4 => "تراکنش توسط پرداخت کننده کنسل شده است",
            -20 => "کلید مجوزدهی (  ) api_keyارسال نشده است (یا مقدار پارامتر مورد نظر خالی است",
            -21 => "شماره تراکنش (  ) trans_idارسال نشده یا خالی ارسال شده است",
            -22 => "مبلغ ارسال نشده است",
            -23 => "مسیر بازگشت (  ) callback_uriارسال نشده است",
            -24 => "مقدار عددی مبلغ صحیح نیست",
            -25 => "شماره تراکنش ( )trans_idدوباره ارسال شده یا قابل پرداخت نیست",
            -26 => "شماره تراکنش (  ) trans_idارسال نشده است",
            -27 => "شماره فاکتور صحیح نمیباشد",
            -30 => "مبلغ کمتر از  100تومان است",
            -31 => "fund not found",
            -32 => "ساختار مسیر بازگشت صحیح نیست",
            -33 => "کلید مجوزدهی (  ) api_keyصحیح نیست",
            -34 => "شماره تراکنش ( )trans_idصحیح نیست",
            -35 => "نوع کلید مجوزدهی (مانند لینک، مستقیم و .).. صحیح نیست",
            -36 => "شماره سفارش (  ) order_idارسال نشده یا بیش از  32کاراکتر است",
            -37 => "تراکنش موجود نیست",
            -38 => "شماره توکن یافت نشد",
            -39 => "کلید مجوزدهی یافت نشد",
            -40 => "کلید مجوزدهی مسدود شده است",
            -41 => "پارامتر های ارسالی از طرف بانک صحیح نیست",
            -42 => "سیستم پرداخت در نکست پی دچار مشکل شده است",
            -43 => "درگاه پرداختی برای انجام روال بانکی یافت نشده است",
            -44 => "بانک عامل پاسخگو نبوده است",
            -45 => "سیستم پرداخت در نکست پی غیر فعال شده است",
            -46 => "درخواست ارسالی اشتباه است یا در نکست پی تعریف نشده است",
            -47 => "درگاه مورد نظر یافت نشد",
            -48 => "نرخ کمیسیون تعیین نشده است",
            -49 => "tتراکنش یکبار انجام شده و دوباره قابل انجام نیست",
            -50 => "حساب کاربری یافت نشد",
            -51 => "کاربری در سیستم یافت نشد",
            -72 => "پاسخی از بانک دریافت نشد",

        );

        if (array_key_exists($error_code, $error_array)) {
            return $error_array[$error_code];
        } else {

            return " کد خطا : " . $error_code;
        }
    }

    public
    function isPageActive($page_id)
    {

        $s = $this->conn->prepare("SELECT active FROM page WHERE page_id LIKE ?");
        $s->bind_param("i", $page_id);
        $s->bind_result($active);
        $s->execute();
        $s->store_result();
        $s->fetch();
        $s->close();
        return $active > 0;

    }

    public
    function checkPageExists($id, $social_id)
    {
        $st = $this->conn->prepare("SELECT active , page_id FROM page WHERE social_id = ? AND id = ?");
        $st->bind_param("is", $social_id, $id);
        $st->bind_result($active, $page_id);
        $st->execute();
        $st->store_result();
        if ($st->num_rows > 0) {
            $st->fetch();
            $st->close();
            if ($active == 0) {
                // yani qablan sabt shode va dar entezare taiide
                return 0;
            } else if ($active == 1 || $active > 2) {
                // yani qablan sabt shode va taiid shode ya dar entezare taiid be rooz resanie
                return 1;

            } else if ($active == 2 || $active == -1) {
                // yani qablan tavasote ma sabt shode ya rad shode ke baz mishe sabt she
                return 2;
            } else if ($active == -2) {
                return 3;
            }
        } else {
            // yani azad va qabele sabte
            return 2;
        }
    }


    public
    function getApikeyByUserid($user_id)
    {
        $st = $this->conn->prepare("SELECT apikey FROM users WHERE user_id LIKE ?");
        $st->bind_param("i", $user_id);
        $st->bind_result($apikey);
        $st->execute();
        $st->store_result();
        if ($st->num_rows > 0) {
            $st->fetch();
            $st->close();
            return $apikey;
        } else {
            return null;
        }

    }


    public
    function getInventory($user_id)
    {

        $st = $this->conn->prepare("SELECT inventory FROM users WHERE user_id LIKE ?");
        $st->bind_param("i", $user_id);
        $st->bind_result($inventory);
        $st->execute();
        $st->store_result();
        $st->fetch();
        $st->close();
        return $inventory;

    }


    function sendSms($mobile, $otp)
    {
        $message = "به پوشکا خوش آمدید رمز عبور شما :" . $otp;
        $query = http_build_query(array('receptor' => $mobile, 'message' => $message), null, "&", PHP_QUERY_RFC3986);
        CallAPI("get", 'v1/30727A4E2F4175644B7967416D4E7378306D7362573379303362615A6E764773/sms/send.json?' . $query, array(), array());

//    try {
//
//
//        date_default_timezone_set("Asia/Tehran");
//        $message = "به  نور الصالحین خوش آمدید رمز تایید حساب : " . $otp ;
//
//        // your sms.ir panel configuration
//        $APIKey = "b47d87e6a117f558c5617e17";
//        $SecretKey = "&&5212tsts";
//        $LineNumber = "50002015008386";
//
//        // your mobile numbers
//        $MobileNumbers = array($mobile);
//
//        // your text messages
//        $Messages = array($message);
//
//
//
//        $SmsIR_SendMessage = new SmsIR_SendMessage($APIKey,$SecretKey,$LineNumber);
//        $SendMessage = $SmsIR_SendMessage->SendMessage($MobileNumbers,$Messages);
//     //   var_dump($SendMessage);
//
//    } catch (Exeption $e) {
//        echo 'Error SendMessage : '.$e->getMessage();
//    }


    }

    function fetchAssocStatement($stmt)
    {
        if ($stmt->num_rows > 0) {
            $result = array();
            $md = $stmt->result_metadata();
            $params = array();
            while ($field = $md->fetch_field()) {
                $params[] = &$result[$field->name];
            }
            call_user_func_array(array($stmt, 'bind_result'), $params);
            if ($stmt->fetch())
                return $result;
        }

        return null;
    }


    function CallAPI($method, $api, $data, $headers)
    {
        $url = SITEURL . "/" . $api;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        switch ($method) {
            case "GET":
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
                break;
            case "POST":
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_POST, count($data));
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                break;
            case "DELETE":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
        }


        $response = curl_exec($curl);


        /* Check for 404 (file not found). */
//    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//    // Check the HTTP Status code
//    switch ($httpCode) {
//        case 200:
//            $error_status = "200: Success";
//            return ($data);
//            break;
//        case 404:
//            $error_status = "404: API Not found";
//            break;
//        case 500:
//            $error_status = "500: servers replied with an error.";
//            break;
//        case 502:
//            $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
//            break;
//        case 503:
//            $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
//            break;
//        default:
//            $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
//            break;
//    }
//    curl_close($curl);
//    echo $error_status;
//    die;
    }


}