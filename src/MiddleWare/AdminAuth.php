<?php
namespace Src\MiddleWare;
use  Psr\Http\Message\ServerRequestInterface as Request;
use  Psr\Http\Message\ResponseInterface as Response ;
use Src\Inc\MCrypt;

class AdminAuth {
    private $level ;
    private $conn ;

  function __construct($level , $container)
  {
      $this->level=$level;
   //   $this->conn=$conn;
        $this->conn=$container->conn;
  }

    function __invoke(Request $request,Response $response,$next)
    {
         global $admin_id;
         $apikey = $request->getHeader("Authorization")[0];
        if ($apikey==null) {
            return $response->withJson(getResponse("Api Key Missing...",400) , 400);
        }else{
//            $mycrypt = new MCrypt();
//            $apikey = $mycrypt->decrypt2($apikey);
            $st = $this->conn->prepare("select active,user_id from users where apikey = ?  ");
            $st->bind_param("s" , $apikey);
            $st->execute();
            $st->bind_result($active , $admin_id_bind);
            $st->store_result();
            if ($st->num_rows > 0 ) {
                $st->fetch();
                if ($active>=$this->level) {
                    $admin_id=$admin_id_bind;
                    $respons=$next($request,$response);
                    return $respons;
                }  else {
                    return $response->withJson(getResponse("شما اجازه دسترسی به این بخش را ندارید",403),403);
                }

            }else {
                return $response->withJson(getResponse("Invalid  Api Key",401),401);
            }

        }

    }

}

/**
 * Created by PhpStorm.
 * User: hamid
 * Date: 4/25/2019
 * Time: 1:44 AM
 */