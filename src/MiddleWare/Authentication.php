<?php
namespace Src\MiddleWare;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Src\Inc\MCrypt;

class Authentication {
    private $conn ;
    function __construct($container)

    {

       //  $this->conn=$conn;
        $this->conn=$container->conn;
    }

    function __invoke(Request $request , Response $response,$next)
    {

        $mcrypt = new MCrypt();
        global $apikey ;
        global $user_id ;
        $apikey_enc = $request->getHeader('Authorization')[0];
   //     $apikey = $request->getHeader('Authorization')[0];

        if ($apikey_enc==null) {
            return $response->withJson(getResponse("Api Key Missing...",400) , 400);
        }else {
           $apikey = $mcrypt->decrypt2($apikey_enc);
           $st = $this->conn->prepare("select active,  user_id from users where apikey = ?  ");
           $st->bind_param("s" , $apikey);
           $st->execute();
           $st->bind_result($active , $user_id_bind);
           $st->store_result();
           if ($st->num_rows > 0 ) {
              $st->fetch();
              if ($active > 0) {
                  $user_id=$user_id_bind;
              }else {
                      return $response->withJson(getResponse("حساب کاربری شما مسدود شده است",403),403);
              }
           }
           else {
             return $response->withJson(getResponse("Invalid  Api Key",401),401);
           }
        }


        $respons=$next($request,$response);
        return $respons;

    }
}
