<?php
namespace Src\MiddleWare;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Banner_Filter  {

    function __construct()
    {

    }

    function __invoke(Request $request, Response $response, $next)
    {

        define("MAX_SIZE", "1024");
        if (!isset($_FILES['banner']['name'])) {

            return $response->withJson(getResponse( "تصویر  مورد نظر انتخاب نشده", 400), 400);
        }

        if ($_FILES['banner']['error'] != UPLOAD_ERR_OK) {

            return $response->withJson(getResponse( "خطلا در اپلود تصویر ", 400), 400);
        }

        $info_pic = getimagesize($_FILES["banner"]["tmp_name"]);
        if ($info_pic == false) {

            return $response->withJson(getResponse( "خطا در شناسایی فایل", 400), 400);
        }

        $pattern = "#^(image/)[^\s\n<]+$#i";
        if (!preg_match($pattern,$info_pic["mime"])) {
            return $response->withJson(getResponse( "شما فقط به اپلود کردن تصویر مجاز میباشد ..", 400), 400);
        }


        if (($info_pic[2] !== IMAGETYPE_GIF) && ($info_pic[2] !== IMAGETYPE_JPEG) && ($info_pic[2] !== IMAGETYPE_PNG)) {
            return $response->withJson(getResponse( "فرمت تصویر آپلود شده معتبر نمیباشد", 400), 400);
        }


        $pic_size = filesize($_FILES['banner']['tmp_name']);


        if ($pic_size > MAX_SIZE * 512) {
            return $response->withJson(getResponse( "حداکثر اندازه تصویر 512 کیلوبایت میباشد", 411), 411);
        }


        $width = $info_pic[0];
        $height = $info_pic[1];
//        if ($width < 600 ) {
//            return $response->withJson(getResponse( "عرض تصویر نباید کمتر از 600 پیکسل باشد", 411), 411);
//        }

        if ($height > $width) {

          return $response->withJson(getResponse( " ارتفاع تصویر نباید بزرگتر از عرض آن باشد", 411), 411);
        }

        $response = $next($request, $response);
        return $response;

    }


}

?>