<?php
namespace Src\MiddleWare;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Chanel_img_filter
{
  private $checkImageExists ;
    function __construct($checkImageExists)
    {
        $this->checkImageExists=$checkImageExists;
    }

    function __invoke(Request $request, Response $response, $next)
    {

        define("MAX_SIZE", "1000");
        // check mikoinm bebin ke age hatman bayad aks upload beshe in qesmat ejra beshe va baqiash dar qerye insoorat age lozomi nadare aks upload beshe etebar sanjie akso ro
        //bikhial she
        if ($this->checkImageExists) {
            if (!isset($_FILES['pic']['name'])) {

                return $response->withJson(getResponse( "عکس  مورد نظر انتخاب نشده", 400), 400);
            }
        }else {

            if (!isset($_FILES['pic']['name'])) {
                $response = $next($request, $response);
                return $response;
            }
        }

        if ($_FILES['pic']['error'] != UPLOAD_ERR_OK) {


            return $response->withJson(getResponse( "خطلا در اپلود عکس ", 400), 400);
        }

        $info_pic = getimagesize($_FILES["pic"]["tmp_name"]);
        if ($info_pic == false) {

            return $response->withJson(getResponse( "خطا در شناسایی فایل", 400), 400);
        }

        $pattern = "#^(image/)[^\s\n<]+$#i";
        if (!preg_match($pattern,$info_pic["mime"])) {
            return $response->withJson(getResponse( "شما فقط به اپلود کردن عکس مجاز میباشد ..", 400), 400);
        }


        if (($info_pic[2] !== IMAGETYPE_GIF) && ($info_pic[2] !== IMAGETYPE_JPEG) && ($info_pic[2] !== IMAGETYPE_PNG)) {

            return $response->withJson(getResponse( "فرمت عکس آپلود شده معتبر نمیباشد", 400), 400);
        }


        $pic_size = filesize($_FILES['pic']['tmp_name']);
        if ($pic_size > MAX_SIZE * 1024) {

            return $response->withJson(getResponse( "حداکثر اندازه عکس 1 مگابایت میباشد", 411), 411);
        }


            if ($info_pic[0] < 300 || $info_pic[1] < 300) {
                return $response->withJson(getResponse("طول یا عرض تصویر نباید کمتر از 300 پیکسل باشد . لطفا تصویر بزرگتری انتخاب نمایید",400),400);
            }


        $response = $next($request, $response);
        return $response;

    }
}