<?php
namespace Src\MiddleWare;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


 class  MakePage_Filter {

     function __invoke(Request $request, Response $response , $next)
     {

         if (!isset($_POST['object'])) {
             return $response->withJson(getResponse("اطلاعات مورد نظر ارسال نگردیده است",400),400);
         }

         $object_temp = $request->getParsedBody()['object'];
         $object = json_decode($object_temp,true);



         if (isset($object['name'])) {

             if(strpos($object['name'], "\n") !== FALSE) {
                 return $response->withJson(getResponse('نام نمیتواند حاوی سطر جدید باشد' , 400),400);
             }
             if (mb_strlen($object['name']) <= 0 || mb_strlen($object['name']) > 80 ) {
                 return $response->withJson(getResponse('نام  باید حداقل دارای 1 و حداکثر 80 حرف باشد ' , 400),400);
             }


         }else {
             return $response->withJson(getResponse('نام نمیتواند خالی  بماند' , 400),400);
         }

           if (isset($object['short_des'])) {
               if(strpos($object['short_des'], "\n") !== FALSE) {
                   return $response->withJson(getResponse('توضیحات مختصر نمیتواند حاوی سطر جدید باشد' , 400),400);
                }
                if (mb_strlen($object['short_des']) <
                    15 || mb_strlen($object['short_des']) > 60) {
                    return $response->withJson(getResponse('توضیحات مختصر باید حداقل دارای 15 و حداکثر 60 حرف باشد ' , 400),400);
                }
           }else {
               return $response->withJson(getResponse('توضیحات مختصر نمیتواند خالی بماند' , 400),400);
           }

     $response=$next($request,$response);
     return $response;

     }
}