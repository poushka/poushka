<?php
namespace Src\MiddleWare;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class Social_Img_filter {
    private $situation ; // 0 bood yani pic  o icon check she 1 bood yani faqat pic 2 bood yani faqat icon

    function __construct($situation)
    {
        $this->situation=$situation;
    }

    function __invoke(Request $request , Response $responsee,$next )
    {

        $response=array();
        define ("MAX_SIZE","1000");

       if ($this->situation==0) {

            if(!isset($_FILES['icon']['name']) || !isset($_FILES['pic']['name']) )
            {
                $response['error'] =true ;
                $response['message'] = "عکس یا آیکون مورد نظر انتخاب نشده";
                return $responsee->withJson($response,400);

            }

            if ($_FILES['pic']['error'] != UPLOAD_ERR_OK || $_FILES['icon']['error'] != UPLOAD_ERR_OK ) {

                $response['error'] =true ;
                $response['message'] = "خطلا در اپلود عکسها ";
                return $responsee->withJson($response,400);
            }
            $pic_size = filesize($_FILES['pic']['tmp_name']);
            $icon_size = filesize($_FILES['icon']['tmp_name']);
            if ($pic_size > MAX_SIZE*1024  || $icon_size > MAX_SIZE*1024) {
                $response["error"] = true;
                $response["message"] = "حداکثر اندازه عکس 1 مگابایت میباشد" ;
                return $responsee->withJson($response,400);
            }
            $info_pic = getimagesize($_FILES['pic']['tmp_name']);
            $icon_info = getimagesize($_FILES['icon']['tmp_name']);
            if ($info_pic==false || $icon_info==false) {
                $response["error"] = true;
                $response["message"] = "خطا در شناسای نوع فایل" ;
                return $responsee->withJson($response,400);
            }
            if (($info_pic[2] !== IMAGETYPE_GIF) && ($info_pic[2] !== IMAGETYPE_JPEG) && ($info_pic[2] !== IMAGETYPE_PNG)) {
                $response["error"] = true;
                $response["message"] = "فرمت عکس آپلود شده معتبر نمیباشد" ;
                return $responsee->withJson($response,400);
            }

            if (($icon_info[2] !== IMAGETYPE_GIF) && ($icon_info[2] !== IMAGETYPE_JPEG) && ($icon_info[2] !== IMAGETYPE_PNG)) {
                $response["error"] = true;
                $response["message"] = "فرمت آیکون آپلود شده معتبر نمیباشد" ;
                return $responsee->withJson($response,400);
            }

            if ($icon_info[0] > 256 || $icon_info[1] > 256) {
                $response["error"] = true;
                $response["message"] = "طول و عرض ایکون نباید بیشتر از 256 باشد" ;
                return $responsee->withJson($response,400);
            }
        }

        else if ($this->situation==1) {


            if(!isset($_FILES['pic']['name'])  )
            {
                $response['error'] =true ;
                $response['message'] = " عکس مورد نظر انتخاب نشده";
                return $responsee->withJson($response,400);
            }

            if ($_FILES['pic']['error'] != UPLOAD_ERR_OK  ) {
                $response['error'] =true ;
                $response['message'] = "خطلا در اپلود عکس ";
                return $responsee->withJson($response,400);
            }
            $icon_size = filesize($_FILES['pic']['tmp_name']);
            if ( $icon_size > MAX_SIZE*1024) {
                $response["error"] = true;
                $response["message"] = "حداکثر اندازه عکس 1 مگابایت میباشد" ;
                return $responsee->withJson($response,400);
            }

            $icon_info = getimagesize($_FILES['pic']['tmp_name']);
            if ( $icon_info==false) {
                $response["error"] = true;
                $response["message"] = "خطا در شناسای نوع فایل" ;
                return $responsee->withJson($response,400);
            }



            if (($icon_info[2] !== IMAGETYPE_GIF) && ($icon_info[2] !== IMAGETYPE_JPEG) && ($icon_info[2] !== IMAGETYPE_PNG)) {
                $response["error"] = true;
                $response["message"] = "فرمت فایل آپلود شده معتبر نمیباشد" ;
                return $responsee->withJson($response,400);
            }
        }else if ($this->situation==2) {

            if(!isset($_FILES['icon']['name'])  )
            {
                $response['error'] =true ;
                $response['message'] = " آیکون مورد نظر انتخاب نشده";
                return $responsee->withJson($response,400);
            }

            if ($_FILES['icon']['error'] != UPLOAD_ERR_OK  ) {
                $response['error'] =true ;
                $response['message'] = "خطلا در اپلود آیکون ";
                return $responsee->withJson($response,400);
            }
            $icon_size = filesize($_FILES['icon']['tmp_name']);
            if ( $icon_size > MAX_SIZE*1024) {
                $response["error"] = true;
                $response["message"] = "حداکثر اندازه آیکون 1 مگابایت میباشد" ;
                return $responsee->withJson($response,400);;
            }

            $icon_info = getimagesize($_FILES['icon']['tmp_name']);
            if ( $icon_info==false) {
                $response["error"] = true;
                $response["message"] = "خطا در شناسای نوع فایل" ;
                return $responsee->withJson($response,400);
            }

            if ($icon_info[0] > 256 || $icon_info[1] > 256) {
                $response["error"] = true;
                $response["message"] = "طول و عرض ایکون نباید بیشتر از 256 باشد" ;
                return $responsee->withJson($response,400);
            }

            if (($icon_info[2] !== IMAGETYPE_GIF) && ($icon_info[2] !== IMAGETYPE_JPEG) && ($icon_info[2] !== IMAGETYPE_PNG)) {
                $response["error"] = true;
                $response["message"] = "فرمت فایل آپلود شده معتبر نمیباشد" ;
                return $responsee->withJson($response,400);
            }

        }

        $responsee=$next($request,$responsee);
        return $responsee;
    }

}
