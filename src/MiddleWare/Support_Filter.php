<?php
namespace Src\MiddleWare ;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class  Support_Filter {

    function __invoke(Request $request , Response $response,$next )
    {

        if (!isset($_POST['object'])) {
            return $response->withJson(getResponse("اطلاعات مورد نظر ارسال نگردیده است",400),400);
        }

        $object_temp = $request->getParsedBody()['object'];
        $object = json_decode($object_temp,true);

        if (isset($object['mobile'])) {
            if ( !is_null($object['mobile'])) {
                $mobile = $object["mobile"];
                $pattern = "/^(09)\d{9}$/";
                if (!preg_match($pattern, $mobile)) {
                    $respond = getResponse("فرمت شماره موبایل وارد شده صحیح نمیباشد", 400);
                    return $response->withJson($respond, $respond[STATUS]);

                }

            }
        }

        $response=$next($request,$response);
        return $response;
    }

}