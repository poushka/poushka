<?php
namespace Src\MiddleWare ;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class  UserInfo_Filter {

    function __invoke(Request $request , Response $responsee,$next )
    {

        if (!isset($_POST['object'])) {
            return $responsee->withJson(getResponse("اطلاعات مورد نظر ارسال نگردیده است",400),400);
        }

        $object_temp = $request->getParsedBody()['object'];
        $object = json_decode($object_temp,true);
        //check mikonim bebinim age username taqir karde pas etebar sanji beshe
        if ($object['huc']) {
               // space va characterhaye namotaref nemigire hadaqal 3 ta hadaksar 50 hatman ba yek harf shoro beshe faqat farsi o englisi
                if (!preg_match('/^(?!.*[(@ #!%$&*)])[A-Za-z\s\x{0600}-\x{06FF}][A-Za-z\s\x{0600}-\x{06FF}0-9_\.\- ]{2,50}$/u',$object['username'])) {
                return $responsee->withJson(getResponse(" نام کاربری باید از بین حروف و اعداد و دارای حداقل 3 حرف باشد ",400),400);
            }
        }

        if (isset($object['email'])) {
            if ( !is_null($object['email'])) {
                if(!filter_var($object['email'], FILTER_VALIDATE_EMAIL)) {
                    return $responsee->withJson(getResponse("ایمیل وارد شده صحیح نمیباشد",400),400);
                }
            }
        }



        $responsee=$next($request,$responsee);
        return $responsee;
    }

}