<?php
namespace Src\Paths;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Src\Inc\Admin_Handler;


class Admin
{
    private $container;
    private $con;

    function __construct($container)
    {
        $this->container = $container;
        $this->con = $this->container->db->connect();
    }


     public function getSupport(Request $request, Response $responsee) {
         $db = new Admin_Handler($this->con);
         $page=$request->getQueryParams()["page"];
         $limit = 4 ;
         $response = $db->getSupports($page,$limit);
         return $response[STATUS]==200 ? $responsee->withJson($response['supports'],201)  : $responsee->withJson($response,$response[STATUS]);
          }
		  
		  
		    public function checkSupport(Request $request, Response $responsee ,$args) {
         $db = new Admin_Handler($this->con);
		 $support_id= $args["support_id"];
         $respond = $db->checkSupport($support_id);
         return $responsee->withJson($respond,$respond[STATUS]);
          }




    ////////////////////////////////////////////////////SOCIAL_FUNCTIONS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    public function makeSocialCat(Request $request, Response $responsee)
    {

        if (isTheseParametersAvailable(array("details"))) {
            $response = array();

            $r_cat_details = $request->getParsedBody()["details"];
            // manzor cat details khame


            $cat_details = json_decode($r_cat_details, true);

            $db = new Admin_Handler($this->con);
            $result = $db->makeSocialCat($cat_details);
            $status_code = $result->error_code;
            $response['error'] = $result->error;
            $response['message'] = $result->message;
            return $responsee->withJson($response, $status_code);

        }


    }
    public function getSocialA(Request $request, Response $responsee)
    {
        $db = new Admin_Handler($this->con);
        $response = $db->getAllSocial();
        return $response[STATUS]==201 ? $responsee->withHeader('Cache-Control', 'public, max-stale=13910400')->withJson($response["socials"],201) : $responsee->withJson($response,$response[STATUS]);


    }
    public function updateSocial(Request $request, Response $responsee, $args)
    {

        $r_cat_details = $request->getParsedBody()["details"];  // manzor cat details khame
        $cat_details = json_decode($r_cat_details, true);
        $db = new Admin_Handler($this->con);
        $response = $db->updateSocial($args['social_id'], $cat_details);
        return $responsee->withJson($response, $response[STATUS]);

    }
    public function updateSocialActive(Request $request, Response $responsee, $args)
    {


        $state = $request->getParsedBody()["state"];  // manzor cat details khame

        $db = new Admin_Handler($this->con);
        $response =  $db->updateSocialActive($args['social_id'],$state);

        return $responsee->withJson($response,$response[STATUS]);



    }
    public function updateSocialPic(Request $request, Response $responsee)
    {

      if (isTheseParametersAvailable(array("pic_name")))  {
          $pic_name = $request->getParsedBody()["pic_name"];

          $db = new Admin_Handler($this->con);
          $response =     $db->updateSocialPic($pic_name);
          return $responsee->withJson($response,$response[STATUS]);

      }


    }
    public function updateSocialIcon(Request $request, Response $responsee)
    {

        if (isTheseParametersAvailable(array("icon_name")))  {
            $icon_name = $request->getParsedBody()["icon_name"];
            $db = new Admin_Handler($this->con);
            $response =     $db->updateSocialIcon($icon_name);
            return $responsee->withJson($response,$response[STATUS]);

        }


    }

    ////////////////////////////////////////////////////CATEGORY_FUNCTIONS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function makeCat(Request $request, Response $responsee)
    {

        if (isTheseParametersAvailable(array("cat_name")))  {
            $cat_name = $request->getParsedBody()["cat_name"];  // manzor cat details khame
            $db = new Admin_Handler($this->con);
            $response =  $db->makeCat($cat_name);
            return $responsee->withJson($response,$response[STATUS]);

        }


    }
    public function getCatAdmin(Request $request, Response $responsee,$args)
    {
            $db = new Admin_Handler($this->con);
            $response = $db->getCats();

        return $response[STATUS]==201 ? $responsee->withJson($response['cats'],201)  : $responsee->withJson($response,$response[STATUS]);
    }
    public function updateCatname(Request $request, Response $responsee,$args)
    {
        $name = $request->getParsedBody()["name"];
        $db  = new Admin_Handler($this->con);
        $response = $db->updateCatname($name,$args["cat_id"]);
        return $responsee->withJson($response,$response[STATUS]);
    }
    public function updateCatState(Request $request, Response $responsee,$args)
    {
        $state = $request->getParsedBody()["state"];
        $db  = new Admin_Handler($this->con);
        $response = $db->updateCatState($state,$args["cat_id"]);
        return $responsee->withJson($response,$response[STATUS]);
    }
    public function updateCatIcon(Request $request, Response $responsee)
    {

        $icon_name = $request->getParsedBody()["icon_name"];
        $db = new Admin_Handler($this->con);
        $response =     $db->updateCatIcon($icon_name);
        return $responsee->withJson($response,$response[STATUS]);
    }
////////////////////////////////////////////////////////////Chanels\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  \\\\\\\\\\\\\\\\\\\\
    public function getWatingPages(Request $request, Response $response) {
        $limit= $request->getQueryParams()['limit'];
        $last_id= $request->getQueryParams()['last_id'];
        $db = new Admin_Handler($this->con);
        $respond =  $db->getWatingPages($limit,$last_id);
        return $response->withJson($respond,200);

    }
    public function confirmPage(Request $request, Response $response , $args) {
        global $admin_id;
        $page_id = $args['page_id'];
        $status = $request->getParsedBody()['status'];   // vase taiine vaziate active shodan ya rad shodan page

        
        $reciver_id = $request->getParsedBody()['reciver_id'];

        $state = $request->getParsedBody()['state']; // vase taiine range message ya hamoon state message
        $title = $request->getParsedBody()['title'];
        $message = $request->getParsedBody()['message'];
        $db = new Admin_Handler($this->con);
        $respond = $db->confirmPageState($page_id,$status,$admin_id,$reciver_id,$state,$title,$message);
        return $response->withJson($respond,$respond[STATUS]);
    }

    public function confrimScreen(Request $request, Response $response , $args) {
        global $admin_id;
        $screen_id = $args['ps_id'];
        $state = $request->getParsedBody()['state']; // vase taiine range message ya hamoon state message

        $db = new Admin_Handler($this->con);
        $respond = $db->confrimScreen($screen_id,$state);
        return $response->withJson($respond,$respond[STATUS]);
    }


    public function getUpdatedPages(Request $request, Response $response) {
        $limit= $request->getQueryParams()['limit'];
        $last_id= $request->getQueryParams()['last_time'];
        $db = new Admin_Handler($this->con);
        $respond =    $db->getUpdatedPages($limit,$last_id);
        return $respond[STATUS]==200  ? $response->withJson($respond["pages"],200) : $response->withJson($respond ,$respond[STATUS]);
    }
    public function confirmUpdatedPage(Request $request, Response $response , $args) {
        global $admin_id; 
        $pu_id  = $args['pu_id'];
        $accept = $request->getParsedBody()['accept']; // agar 1 bood update taiid mishe va ba table asli merge mishe dar qeyre insort pak mishe az page_update
        $user_id = $request->getParsedBody()['user_id'];
        $message = $request->getParsedBody()['message'];
        $title = $request->getParsedBody()['title'];
        
        $page_id = $request->getParsedBody()['page_id'];
        $db=new Admin_Handler($this->con);
        $respond = $db->confrimUpdatedPage($pu_id,$page_id , $user_id,$accept,$message ,$title,$admin_id);
        return $response->withJson($respond,$respond[STATUS]);
    }
    public function getWatingProfiles(Request $request, Response $response ) {
        $limit= $request->getQueryParams()['limit'];
        $last_id= $request->getQueryParams()['last_time'];
        $db = new Admin_Handler($this->con);
        $respond =    $db->getWatingProfiles($limit,$last_id);
       return $respond[STATUS]==200  ? $response->withJson($respond["profiles"],200) : $response->withJson($respond ,$respond[STATUS]);
    }

    public function confirmProfile(Request $request, Response $response , $args) {
            global $admin_id;   
            $user_id = $args['user_id'];
            $accept = $request->getParsedBody()['accept']; // 1 yani taiid mishe va  hich fildi null nemishe // agar 0 bood bayad ye seri az filda zakhmi shan

          if ($accept==0) {
              $username = $request->getParsedBody()['username'];  // meqdare ina ya 1 hastesh yani bayad null beshe ya 0 ke gucgu
              $thumb = $request->getParsedBody()['thumb'];
              $bio = $request->getParsedBody()['bio'];
              $name = $request->getParsedBody()['name'];
              $state = $request->getParsedBody()['state']; // vase taiine range message ya hamoon state message
              $title = $request->getParsedBody()['title'];
              $message = $request->getParsedBody()['message'];
              
          }

        $db= new Admin_Handler($this->con);
        if ($accept==1) {
            $respond =  $db->confirmProfileState($user_id,$accept,null,null,null,null,null,null,null,$admin_id);
            return $response->withJson($respond,$respond[STATUS]);
        } else {
            $respond =  $db->confirmProfileState($user_id,$accept,$username,$thumb,$bio,$name,$state,$title,$message,$admin_id);
            return $response->withJson($respond,$respond[STATUS]);
        }






    }

    public function getWatingComments(Request $request, Response $response , $args) {
        $limit = 10 ;
        $page = $args["page"];
        $db = new Admin_Handler($this->con);
        $respond =  $db->getWatingsCm($limit,$page);
        return $respond[STATUS] ==200 ? $response->withJson($respond["comments"] , 200) : $response->withJson($respond,$respond[STATUS]);
    }
    public function confrimComment(Request $request, Response $response , $args) {
        global $admin_id;    
        $comment_id = $args["cm_id"];
        $type = $request->getParsedBody()["type"] ; // user bashe yani comment user ro mikhaim taiid konim // admin bashe yani commente admin ro mikhaim taiid konim :)
        $accept = $request->getParsedBody()["accept"] ;
         $db =new Admin_Handler($this->con);
        $respond = $db->confrimComment($comment_id,$accept,$type);
        return $response->withJson($respond,$respond[STATUS]);
    }

    public function confirmCount(Request $request, Response $response) {
        $db = new Admin_Handler($this->con);
        $respond = $db->confirmCount();
        return $response->withJson($respond,200);

    }
    public function getCustomMessages(Request $request, Response $response ,$args) {

        $db = new Admin_Handler($this->con);
        $respond = $db->customMessages($args["parent_id"]);
        return $response->withJson($respond,200);
    }
    public function getBanners(Request $request, Response $response){
        $limit= $request->getQueryParams()['limit'];
        $last_id= $request->getQueryParams()['last_id'];
        $db = new Admin_Handler($this->con);
        $respond =    $db->getWatingBanners($limit,$last_id);
        return $respond[STATUS]==200  ? $response->withJson($respond["banners"],200) : $response->withJson($respond ,$respond[STATUS]);
    }

    public function confirmBanner(Request $request, Response $response,$args) {
        global $admin_id;  
        $pb_id = $args['pb_id'];
        $banner = $request->getParsedBody()['banner'];
        $page_id = $request->getParsedBody()['page_id'];  // vase in mikhaimesh ke check konim bebin banner qabli nulle ya na
        $status = $request->getParsedBody()['status'];   // vase taiine vaziate rad shodan ya taiid shodan
        $reciver_id = $request->getParsedBody()['reciver_id'];
        $title = $request->getParsedBody()['title'];
        $message = $request->getParsedBody()['message'];
        $db = new Admin_Handler($this->con);
        $respond = $db->confirmBanner($pb_id,$banner,$page_id, $status,$admin_id,$reciver_id,$title,$message);
        return $response->withJson($respond,$respond[STATUS]);
    }
/////////////////////////////////////////////////////////////Special\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function reserveBu(Request $request, Response $response,$args) {
        $special_id = $args["special_id"];
        $page_id = $request->getQueryParams()["page_id"];
        $db =new Admin_Handler($this->con);
        $needs = $db->getPurchaseNeeds($special_id,$page_id);

        $is_main = $needs["is_main"];
        $max = $needs["max"];
        $cat_id = $needs["cat_id"];
        $social_id = $needs["social_id"];
        $user_id = $needs["user_id"];
        $prefix = $needs["prefix"];
        $banner_need = $needs["banner_need"];
        $respond = $db->reservByUs($max,$cat_id,$is_main,$social_id,$page_id,$special_id,$user_id,$prefix,$banner_need);
        return $response->withJson($respond,$respond[STATUS]);

    }
    /////////////////////////////////////////////////////////////Activation\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function logindAdmin(Request $request, Response $response) {
         
        $mobile = $request->getParsedBody()["mobile"];
        $password = $request->getParsedBody()["password"];
      
        $db = new Admin_Handler($this->con);
        $respond =     $db->adLog($mobile,$password);
        return $respond[STATUS]==200 ? $response->write($respond["apikey"]) : $response->withJson($respond,$respond[STATUS]);
    }
    public function getUser(Request $request, Response $response) {
        $mobile = $request->getQueryParams()["mobile"];
      
        $db =new Admin_Handler($this->con);
        $user = $db->getUserByMobile($mobile);
        return  $user[STATUS]==200 ? $response->withJson($user["user"],200) : $response->withJson($user,$user[STATUS]);

    }
    public function getUserTransactions(Request $request, Response $response ,$args) {
        $user_id = $args["user_id"];
        $limit = 5 ;
        $page=$request->getQueryParams()["page"];
        $db =new Admin_Handler($this->con);
        $respond = $db->getUserTransactioon($page,$limit,$user_id);
        return $respond[STATUS]==200 ? $response->withJson($respond["transactions"],200) : $response->withJson($respond,$respond[STATUS]);
    }
    public function increaseInventory(Request $request, Response $response,$args) {
        $user_id = $args["user_id"];
        $inventory = $request->getParsedBody()["inventory"];
        $db =new Admin_Handler($this->con);
        $respond = $db->increaseInventory($user_id,$inventory);
        return   $response->withJson($respond,$respond[STATUS]);

    }
    public function activateUser(Request $request, Response $response,$args) {
        $user_id = $args["user_id"];
        $active = $request->getParsedBody()["active"];
        $db = new Admin_Handler($this->con);
        $respond = $db->activateUser($user_id,$active);
        return   $response->withJson($respond,$respond[STATUS]);

    }

//////////////////////////////////////////////////////////////////Specialization\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

   public function special(Request $request, Response $response)
   {
       $user_id = 1;
       $so_id = 1;
       $reserved_flag = 2;
       $page_id = $request->getParsedBody()["page_id"];
       $special_id = $request->getParsedBody()["special_id"];
       $db=new Admin_Handler($this->con);
       if ($special_id > 2) {
           $cat_id = $db->getCatIdByPageId($page_id);
       } else {
           $cat_id = null;
       }
       $social_id = $db->getSocialByPageid($page_id);



      $stmt=  $this->con->prepare("insert into special_r (page_id,special_id,reserved_flag,social_id,cat_id,user_id,so_id,exp_time) VALUES (?,?,?,?,?,?,?,now())");
      $stmt->bind_param("iiiiiii",$page_id,$special_id,$reserved_flag,$social_id,$cat_id,$user_id,$so_id);

      if ($stmt->execute()) {
          if ($special_id== 1  || $special_id == 3) {
              return $response->withJson(getResponse(" بنر مورد نظر ثبت گردید ",200),200);
          }else {
              return $response->withJson(getResponse(" ویژه مورد نظر ثبت گردید ",200),200);
          }

      }else {
          return $response->withJson(getResponse(" خطا در ثبت",200),200);
      }

   }


//////////////////////////////////////////////////////////////////Specialization\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    public function screen_cron(Request $request, Response $responsee){

        $SCREEN_DIR = __DIR__ .'/../uploads/chanel_screen/';
        $stmt=$this->con->prepare("SELECT ps_id , name , thumb_name  from page_screen where page_id is null and TIMESTAMPDIFF(MINUTE,created_at,now()) > 30");
        $stmt->execute();
        $result = $stmt->get_result();
        while ($single_screen = $result->fetch_assoc()) {
            @ unlink($SCREEN_DIR.$single_screen['name']);
            @ unlink($SCREEN_DIR.$single_screen['thumb_name']);
        }
        $stmt=$this->con->prepare("delete  from page_screen where page_id is null and TIMESTAMPDIFF(MINUTE,created_at,now()) > 30");
        $stmt->execute();
    }


    public function special_cron(Request $request, Response $responsee){


      //for thos reserve that have not been compeleted in the time of pay by bank
     $stmt=$this->con->prepare("DELETE FROM special_r where reserved_flag = 0 and  reserved_exp < now() ;");
     $stmt->execute();


     $stmt=$this->con->prepare("DELETE FROM special_r where reserved_flag = 1 and exp_time < now() ;");
     $stmt->execute();


    }


    public function word_cron(Request $request, Response $responsee){
        //for thos reserve that have not been compeleted in the time of pay by bank
        $stmt=$this->con->prepare("DELETE FROM word_r where reserved_flag = 0 and  finish_bank < now() ;");
        $stmt->execute();


        $stmt=$this->con->prepare("DELETE FROM word_r where reserved_flag = 1 and exp_time < now() ;");
        $stmt->execute();


    }



}
