<?php


namespace Src\Paths;


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use SoapClient;
use function Src\Inc\jdate;
use \Src\Inc\User_Handler;
use \Src\Inc\MCrypt;

include_once('jdf.php');

session_set_cookie_params(3600 * 24 * 365);

session_start();


$apikey = null;
$user_id = null;

class Users
{
    //7972464177524a39496e3751334b41354f446a7551544a65366744343646546572626e534c6b5844704b647138786e594b315632794c43787a76755959324f31
    private $container;
    private $con;

    function __construct($container)

    {
        $this->container = $container;
        $this->con = $this->container->db->connect();
    }

    public function test(Request $request, Response $response)

    {



    /*    $array1 = array(1,2,4);
        $array12 = array(1,4,6,7);
        $array3 = array_diff($array12,$array1);*/


/*        $scrrends = array(17,18,19);

        $db->handle_sreens($scrrends,200);
*/
//        $time = "2020/03/08 13:40:56";
//
//        $old = new \DateTime($time);
//        $jalali  = jdate("Y/m/d H:i:s", $old->getTimestamp());
//
//        $crypto = new MCrypt();
//
//
//        echo $crypto->encrypt2("8deebfc14b601e026b51b02220e20699");

//
//        $db = new User_Handler($this->con);
//        $count = $db->getMemberCount(5,"alborz");

//        $name = "سلام عزیزم";
//
//        $subeed = mb_substr($name,0,5,'utf-8');
//
//
//        echo  mb_strlen($subeed);


//        $db = new User_Handler($this->con);
//        $db->insertShortrandoms();



     //   die($test);



       $name = $request->getQueryParams()["name"];



        $mcypt = new MCrypt();
        $encrypted = $mcypt->encrypt2($name);



        echo $encrypted ;


        //   fireBaseNotify("سلام fبا مرام","chetori ","confrim",1,1,1);


    }


    public function sendNotification(Request $request, Response $response)
    {
        $server_key = null;
        $to = null;
        // recive moshkhash mikone ke payam vase ki ya che topici mire agar topic 1 bood bayad maslan global bashe agar 0 bood bayad fcm_token bashe
        $reciver = $request->getParsedBody()["reciver"];
        $topic = $request->getParsedBody()["topic"];  // 1 bood yani topice dar qeyre insorat be shakhsh ersal mishe
        $title = $request->getParsedBody()["title"];
        $body = $request->getParsedBody()["body"];
        $flag = $request->getParsedBody()["flag"];     //  in flag ham vase samte androide 1 frlan vase payame mamolie
        $admin = $request->getParsedBody()["admin"];  // 1 bood yani be admin ersal she dar vaqe az serve key admin estefade mikone 0 bood yani nbe user


        if ($topic == 1) {
            $to = "/topics/$reciver";
        } else {
            $to = "$reciver";
        }
        $payload = array(
            "to" => $to,
            "priority" => "high",
            "mutable_content" => true,
            "data" => array(
                "title" => "$title",
                "body" => "$body",
                "flag" => $flag,
                "timestamp" => "'" . date('Y-m-d G:i:s') . "'"

            )
        );
        if ($admin == 1) {
            $server_key = SERVER_KEY_FCM_ADMIN;
        } else {
            $server_key = SERVER_KEY_FCM;
        }
        // 1 bood yani be admin ersal she dar vaqe az serve key admin estefade mikone 0 bood yani nbe user
        $headers = array(
            'Authorization:key=' . $server_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        $result = curl_exec($ch);
        curl_close($ch);

        die($result);

    }

    public function getSC(Request $request, Response $response) {
        $db = new User_Handler($this->con);
        $respond = $db->getsc();
        return $response->withJson($respond, 200);
    }

    //etelate update app
    public function info(Request $request, Response $response)
    {
        $db = new User_Handler($this->con);
        $respond = $db->getInfo();
        return $response->withHea-der("Cache-Control", "no-cache, must-revalidate")->withJson($respond, 200);
    }

    public function faq(Request $request, Response $response)
    {
        $db = new User_Handler($this->con);
        $respond = $db->getFaq();
        return $response->withJson($respond, 200);
    }


    //qavanine app
    public function rules(Request $request, Response $response)
    {

        $this->container->view->render($response, "rules.html");

    }

    //ersale payame be poshtibani
    public function support(Request $request, Response $response)
    {
        $db = new User_Handler($this->con);
     $apikey = $request->getHeader("Authorization")[0];
     $user_id = 1;

     if (!empty($apikey)) {
         $mcrypt = new MCrypt();
         $apikey=$mcrypt->decrypt2($apikey);
         $user_id=$db->getUserIdByApikey($apikey);
         if ($user_id==-1) {
             return $response->withJson(getResponse("Invalid Apikey", 401));
         }

     }

        if (!isset($_POST["object"])) {
            return $response->withJson(getResponse("اطلاعات ارسال نشده است", 400));
        }

        $object_temp = $request->getParsedBody()["object"];
        $object = json_decode($object_temp, true);

        $respond = $db->support($object, $user_id);
        return $response->withJson($respond, $respond[STATUS]);
    }

    public function supportTypes(Request $request, Response $response) {

        $db = new User_Handler($this->con);
        $respond = $db->getSupportTypes();
        return $response->withJson($respond,200);


    }


    //temp firebase
    public function firebase(Request $request, Response $response)
    {

        $payload = array(
            'to' => '/topics/global',
            'priority' => 'high',
            "mutable_content" => true,
            "notification" => array(
                "title" => "kaftar",
                "body" => "jafar"
            ),
            'data' => array(
                'action' => 'models',
                'model_id' => '2701',
            )
        );

        //ersal be yek user
//        $payload = array(
//            'to'=>'fN6j2gbou9A:APA91bGzkS4qtJ5tkgNCP93t0z7--B9GLwe7-ylei1zP92KSyrbNkyoRE6I72H8JMJFtWn-wE_1Ux9IGg4g3lSVnwWJWyuh_tb8f1tMR1i0kQjY4YqJdA3gym_bPKwBgLuMy1XdlRYg1',
//            'priority'=>'high',
//            "mutable_content"=>true,
//            "notification"=>array(
//                "title"=> "naser hastam yek olagh",
//                "body"=> "افرین افرین"
//            ),
//            'data'=>array(
//                'action'=>'models',
//                'model_id'=>'2701',
//            )
//        );

        $headers = array(
            'Authorization:key=AAAAhhFWfF4:APA91bHNKnypBDbitu2Xlr4zT-iv10OL10diWCrQ015I1t_hpb_QLfXw64fKl4AIInUFZAj-0WuqRS4e6OvxbBdEwWV4ubmX522i9CFzJoCxcMQpnn6rSP5XcxCz7YwbDbr22OinB-nW',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        $result = curl_exec($ch);
        curl_close($ch);
        var_dump($result);
        exit;

    }


    ///////////////////////////////////////////////////////////Registeration\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function register(Request $request, Response $response)
    {

        if (isTheseParametersAvailable(array('mobile'))) {
            $mobile = $request->getParsedBody()["mobile"];
            //check kardane dorost boodane shomare . ba 0 va 9 shoro mishe va 9 raqam badesh
            $pattern = "/^(09)\d{9}$/";
            if (!preg_match($pattern, $mobile)) {
                $respond = getResponse("فرمت شماره موبایل وارد شده صحیح نمیباشد", 400);
                return $response->withJson($respond, $respond[STATUS]);
            } else {
                $otp = rand(100000, 999999);
                $db = new User_Handler($this->con);
                $respond = $db->createUser($mobile, $otp);
                return $response->withJson($respond, $respond[STATUS]);
            }

        };

    }

    public function verify(Request $request, Response $response)
    {

        if (isTheseParametersAvailable(array('mobile', 'otp'))) {
            $mobile = $request->getParsedBody()['mobile'];
            $otp = $request->getParsedBody()['otp'];
            $password = $request->getParsedBody()['password'];
            $moaref = $request->getParsedBody()['moaref'];

            if ($password != null) {
                if (strlen($password) < 6) {
                    $respond = getResponse("رمز عبور باید دارای حداقل 6 کاراکتر باشد", 400);
                    return $response->withJson($respond, $respond[STATUS]);
                }
            }


            $db = new User_Handler($this->con);
            $respond = $db->activeUser($mobile, $otp, $password, $moaref);
            if ($respond[STATUS] == 201) {
                if ($password != null) {
                    $_SESSION["login"] = true;
                    $_SESSION["user"] = json_encode($respond["user"]);
                    $_SESSION["user_id"] = $respond["user"]["user_id"];
                    $_SESSION["apikey"] = $respond["user"]["apikey"];
                }

                return $response->withJson($respond, 201);

            } else {
                return $response->withJson($respond, $respond[STATUS]);
            }


            //    return $response->withJson($respond, $respond[STATUS]);
        }


    }

    public function login(Request $request, Response $response)
    {

        if (isTheseParametersAvailable(array('mobile', 'password'))) {
            $mobile = $request->getParsedBody()['mobile'];
            $password = $request->getParsedBody()['password'];
            $db = new User_Handler($this->con);
            $respond = $db->loginUser($mobile, $password);

            if ($respond[STATUS] == 200) {
                $user = $respond["user"];
                $_SESSION["login"] = true;
                $_SESSION["user"] = json_encode($respond["user"]);
                $_SESSION["user_id"] = $user["user_id"];
                $_SESSION["apikey"] = $user["apikey"];
                return $response->withJson($respond["user"], 200);
            } else {
                return $response->withJson($respond, $respond[STATUS]);
            }

        }

    }

    public function reqReset(Request $request, Response $response)
    {

        if (isTheseParametersAvailable(array('mobile'))) {
            $otp = rand(100000, 999999);
            $mobile = $request->getParsedBody()['mobile'];
            $db = new User_Handler($this->con);
            $respond = $db->reqReset($mobile, $otp);
            return $response->withJson($respond, $respond[STATUS]);
        }
    }

    public function resetPassword(Request $request, Response $response)
    {

        if (isTheseParametersAvailable(array('mobile', 'otp'))) {
            $mobile = $request->getParsedBody()['mobile'];
            $code = $request->getParsedBody()['otp'];
            $password = $request->getParsedBody()['password'];
            $db = new User_Handler($this->con);
            $respond = $db->resetPassword($mobile, $code, $password);

            if ($respond[STATUS] == 200) {
                $user = $respond["user"];
                $_SESSION["login"] = true;
                $_SESSION["user"] = json_encode($respond["user"]);
                $_SESSION["user_id"] = $user["user_id"];
                $_SESSION["apikey"] = $user["apikey"];
                return $response->withJson($respond, 200);
            } else {
                return $response->withJson($respond, $respond[STATUS]);
            }
        }
    }

    public function infoUpdate(Request $request, Response $response)
    {
        global $user_id;
        $hasImage = isset($_FILES['pic']);
        $object_temp = $request->getParsedBody()['object'];
        $object = json_decode($object_temp, true);
        $db = new User_Handler($this->con);
        $result = $db->updateUserinfo($object, $hasImage, $user_id);
        if ($result[STATUS] == 200) {
            $_SESSION["user"] = json_encode($result["userinfo"]);
        }
        return $result[STATUS] == 200 ? $response->withJson($result['userinfo'], 200) : $response->withJson($result, $result[STATUS]);

    }

    public function updateFCM(Request $request, Response $response)
    {

        global $user_id;
        $fcm_token = $request->getParsedBody()["fcm_token"];
        if (!preg_match('/^\s*\S.*$/', $fcm_token)) {
            $respond = getResponse("FCM TOKEN IS EMPTY", 401);
            return $response->withJson($respond, $respond[STATUS]);
        }

        $db = new User_Handler($this->con);
        $respond = $db->updateFcmToken($user_id, $fcm_token);
        return $response->withJson($respond, $respond[STATUS]);

    }

    public function getUser(Request $request, Response $response)
    {
        global $user_id;
        $db = new User_Handler($this->con);
        $respond = $db->getUser($user_id);
        return $response->withHeader("Cache-Control", "no-cache, must-revalidate")->withJson($respond, 200);
    }

    public function getUpdatedCode(Request $request, Response $response)
    {
        global $user_id;
        $db = new User_Handler($this->con);
        $respond = $db->getUserUpdatedCode($user_id);
        header("Cache-Control: no-cache, must-revalidate");
        return $respond . "";

    }

    public function getMessages(Request $request, Response $response)
    {
        global $user_id;
        $db = new User_Handler($this->con);
        $limit = 10;
        $last_id = $request->getQueryParams()["last_id"];
        $respond = $db->getMessages($user_id, $limit, $last_id);

        return $respond[STATUS] == 200 ? $response->withHeader('Cache-Control', 'public, max-stale=604800')->withJson($respond["messages"], 200) :
        $response->withJson($respond, $respond[STATUS]);

    }

    public function getMessageCount(Request $request, Response $response)
    {
        global $user_id;
        $db = new User_Handler($this->con);
        $count = $db->getMessageCount($user_id);
        header("Cache-Control: no-cache, must-revalidate");
        return $count . "";
    }




    public function sendMessage(Request $request, Response $response)
    {

        sleep(2);
        global $user_id;

        $reciver_id = $request->getParsedBody()["reciver_id"];
        $title = $request->getParsedBody()["title"];
        $state = 0;
        $message = $request->getParsedBody()["message"];


        if (!preg_match('/^\s*\S.*$/', $title)) {
            $respond = getResponse("عنوان نمیتواند خالی بماند", 401);
            return $response->withJson($respond, $respond[STATUS]);
        } else if (!preg_match('/^\s*\S.*$/', $message)) {
            $respond = getResponse("پیام نمیتواند خالی بماند", 401);
            return $response->withJson($respond, $respond[STATUS]);
        }
        $db = new User_Handler($this->con);
        $respond = $db->sendMessage($user_id, $reciver_id, $title, $message, $state);
        return $response->withJson($respond, $respond[STATUS]);


    }

    public function logout(Request $request, Response $response)
    {
        unset($_SESSION["login"]);
        unset($_SESSION["user_id"]);
        unset($_SESSION["apikey"]);
        unset($_SESSION["user"]);
        return $response->withJson(getResponse("خروج از حساب امجام شد", 200), 200);

    }


/////////////////////////////////////////////////////////ChanelFunctions\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    public function gettags(Request $request, Response $response, $args)
    {
        $db = new User_Handler($this->con);
        $tags = $db->getTags();
        return $response->withJson($tags, 200);
    }

    public function estelam(Request $request, Response $response, $args)
    {
        $social_id = $args['social_id'];
        $id = $request->getParsedBody()['id'];
        $prefix = $request->getParsedBody()['prefix'];


        $db = new User_Handler($this->con);
        $check = $db->checkPageExists($id, $social_id);
        if ($check == 0) {
            return $response->withJson(getResponse($prefix . " مورد نظر قبلا ثبت گردیده و در انتظار تایید میباشد", 400), 400);
        } else if ($check == 1) {
            return $response->withJson(getResponse($prefix . " مورد نظر قبلا ثبت گردیده ", 400), 400);
        } else if ($check == 3) {
            return $response->withJson(getResponse($prefix . "  مورد نظر به حالت تعلیق در آمده و قابل ثبت نیست . برای پیگیری از بخش حساب کاربری با پشتیبانی تماس حاصل نمایید ", 400), 400);
        } else {
            $member_cont = $db->getMemberCount($social_id, $id);
            if ($member_cont != 0 && $member_cont == null) {
                return $response->withJson(getResponse("خطا در ارتباط با سرور لطفا روباره تلاش نمایید", 400), 400);
            } else if ($member_cont == -1) {
                return $response->withJson(getResponse("آی دی " . $prefix . " وارد شده صحیح نمیباشد", 404), 404);
            } else {
                return $member_cont . "";
            }
        }

    }

    public function teleEstelam(Request $request, Response $response)
    {

        $id = $request->getParsedBody()['id'];
        $query = http_build_query(array('chat_id' => "@" . $id), null, "&", PHP_QUERY_RFC3986);
        $tokens = array();
        $tokens[0] = "896140234:AAE2bRxzsGMhPJ9AEFGxPN9LoHTfGZOCbIo";
        $tokens[1] = "815734807:AAFmP1nrZFaJA6rbTuaiR2MvAVSBaZpLyec";
        $tokens[2] = "864634326:AAHaUnVRUHHNhsg4MkfEfcHvl1_DQxvCqKY";
        $tokens[3] = "754265063:AAFxI7Qoi4QWn3RElhFReIgDcGDg21xcMlU";
        $tokens[4] = "800141501:AAE1Tls--jVGsBztCSVaEbN6Su8jXdKUffA";
        $token = $tokens[rand(0, 4)];

        $url = "https://api.telegram.org/bot$token/getChatMembersCount?" . $query;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array());
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array());
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        $response = json_decode(curl_exec($curl), true);

        if (curl_errno($curl)) {
            //  die(curl_error($curl) );
            return null;
        } else {
            echo json_encode($response);
        }


    }


    public function estelamAndUpdate(Request $request, Response $response, $args)
    {

        $page_id = $args['page_id'];
        $db = new User_Handler($this->con);
        $needs = $db->getEstelamNeeds($page_id);
        $current_count = $needs["member"];

        $member_cont = $db->getMemberCount($needs["social_id"], $needs["id"]);


        if ($member_cont == null) {
            $respond=array();
            $respond["message"] = "خطا در ارتباط با سرور لطفا روباره تلاش نمایید";
            $respond["id"] = $page_id;
            return $response->withJson($respond, 400);
        } else if ($member_cont == -1) {
            $respond=array();
            $respond["message"] = "آی دی  " . $needs["member_prefix"] . " وارد شده صحیح نمیباشد";
            $respond["id"] = $page_id;
            return $response->withJson($respond, 404);
        } else {
            $respond = array();
            if ($current_count!=$member_cont) {
                $db->updateMemberCount($page_id, $member_cont);
            }
            $respond["id"] = $page_id;
            $respond["member_count"]=number_format($member_cont)  . " " . $needs["member_prefix"];
            return $response->withJson($respond,200);

        }
    }

    public function screenUpload(Request $request, Response $response, $args)
    {
        $div_name = $request->getParsedBody()['name'];
        $db = new User_Handler($this->con);
        $my_response = $db->screenUpload();
        if ($my_response[STATUS] == 201) {
            $my_response["name"] = $div_name;
        }
        return $response->withJson($my_response, $my_response[STATUS]);

    }


    public function screenDelete(Request $request, Response $response, $args)
    {
        $ps_id = $args["ps_id"];
        $db = new User_Handler($this->con);
        $respond = $db->screenDelete($ps_id);
        return $response->withJson($respond,$respond[STATUS]);

    }


    public function makePage(Request $request, Response $response, $args)
    {
        global $user_id;
        $social_id = $args['social_id'];
        $object = $request->getParsedBody()['object'];
        $tags_object = $request->getParsedBody()['tags'];
        $screen_object = $request->getParsedBody()['screens'];
        $object = json_decode($object, true);
        $tags = json_decode($tags_object);
        $screens = json_decode($screen_object);


        $db = new User_Handler($this->con);
        $aparat = null;

        if (count($tags)>10) {
            return $response->withJson(getResponse("تعداد برچسب ها نباید بیشتر از 10 عدد باشد", 400), 400);
        }
        //first we check if aparat text is filed
        if (strlen(trim($object["aparat"])) != 0) {
            //second we check if it has / to be sure the address is acurate

            if (strpos($object["aparat"], '/') == false) {
                return $response->withJson(getResponse("لینک آپارات وارد شده صحیح نمیباشد", 400), 400);
            } else {
                $aparat_frame = $db->getAparatFragme(substr($object["aparat"], strrpos($object["aparat"], '/') + 1));
                if ($aparat_frame == null) {
                    return $response->withJson(getResponse("لینک آپارات وارد شده صحیح نمیباشد", 400), 400);
                } else {
                    $aparat = $aparat_frame;
                }
            }
        }
        $member_cont = $db->getMemberCount($social_id, $object['id']);
        if ($member_cont == null) {
            return $response->withJson(getResponse("خطا در ارتباط با سرور .. لطفا روباره تلاش نمایید", 400), 400);
        } else if ($member_cont == -1) {
            return $response->withJson(getResponse("آی دی " . $object['prefix'] . " وارد شده صحیح نمیباشد", 400), 400);
        } else {

            $result = $db->makePage($social_id, $user_id, $member_cont, $object, $aparat, $tags, $screens);
            return $response->withJson($result, $result[STATUS]);
        }

    }

    public function updatePage(Request $request, Response $response, $args)
    {
        global $user_id;
        $page_id = $args['page_id'];
        $object = $request->getParsedBody()['object'];
        $tags = $request->getParsedBody()['tags'];
        $screens = $request->getParsedBody()['screens'];


        $object = json_decode($object, true);
        $tags = json_decode($tags);
        $screens = json_decode($screens);

        if (count($tags)>10) {
            return $response->withJson(getResponse("تعداد برچسب ها نباید بیشتر از 10 عدد باشد", 400), 400);
        }


        $aparat = null;
        $db = new User_Handler($this->con);
        //first we check if aparat text is filed
         if (strlen(trim($object["aparat"])) != 0) {
            //second we check if it has / to be sure the address is acurate

            if (strpos($object["aparat"], '/') == false) {
                return $response->withJson(getResponse("لینک آپارات وارد شده صحیح نمیباشد", 400), 400);
            } else {
                $aparat_frame = $db->getAparatFragme(substr($object["aparat"], strrpos($object["aparat"], '/') + 1));
                if ($aparat_frame == null) {
                    return $response->withJson(getResponse("لینک آپارات وارد شده صحیح نمیباشد", 400), 400);
                } else {
                    $aparat = $aparat_frame;
                }

            }

        }

        if (!$db->isUserOwner($page_id, $user_id)) {
            return $response->withJson(getResponse("عملیات غیر مجاز میباشد", 403), 403);
        }



        $respond = $db->updatePage($page_id, $user_id, $object,$aparat,$tags,$screens);
        return $response->withJson($respond, $respond[STATUS]);

    }



    public function getBanner(Request $request, Response $response, $args)
    {
        global $user_id;
        $page_id = $args["page_id"];
        $db = new User_Handler($this->con);
        if (!$db->isUserOwner($page_id, $user_id)) {
            return $response->withJson(getResponse("عملیات غیر مجاز میباشد", 403), 403);
        }
        $respond = $db->getBanner($page_id);
        return $response->withJson($respond, 200);
    }

    public function bannerUpdate(Request $request, Response $response, $args)
    {
        global $user_id;
        $page_id = $args["page_id"];
        $db = new User_Handler($this->con);
        if (!$db->isUserOwner($page_id, $user_id)) {
            return $response->withJson(getResponse("عملیات غیر مجاز میباشد", 403), 403);
        }
        $respond = $db->updateBanner($page_id, $user_id);
        return $response->withJson($respond, $respond[STATUS]);
    }

    public function getUserPages(Request $request, Response $response)
    {
        global $user_id;
        $last_id = $request->getQueryParams()['last_id'];
        $limit = 12;
        $db = new User_Handler($this->con);
        $respond = $db->getUserPages($user_id, $last_id, $limit);
        return $respond[STATUS] == 200 ? $response->withJson($respond["pages"], 200) : $response->withJson($respond, $respond[STATUS]);

    }

    public function getPageDetails(Request $request, Response $response, $args)
    {
        global $user_id;
        $page_id = $args['page_id'];
        $db = new User_Handler($this->con);
        $respond = $db->getPageDetails($user_id, $page_id);
        return $response->withJson($respond, 200);

    }

    public function getPage(Request $request, Response $response, $args)
    {

        global $user_id;
        $page_id = $args['page_id'];
        $db = new User_Handler($this->con);
        $respond = $db->getPage($user_id, $page_id);
        if ($respond==null) {
            return $response->withJson(getResponse("Authorization Failed",401), 401);
        }
        return $response->withJson($respond, 200);

    }



    public function likePage(Request $request, Response $response, $args)
    {
        global $user_id;
        $page_id = $args['page_id'];
        $db = new User_Handler($this->con);
        $respond = $db->likePage($user_id, $page_id);
        return $response->withJson($respond, $respond[STATUS]);
    }

    public function unlikePage(Request $request, Response $response, $args)
    {
        global $user_id;
        $page_id = $args['page_id'];
        $db = new User_Handler($this->con);
        $respond = $db->unlikePage($user_id, $page_id);
        return $response->withJson($respond, $respond[STATUS]);
    }

    public function ratePage(Request $request, Response $response, $args)
    {

        global $user_id;
        $page_id = $args['page_id'];
        $rate = $request->getParsedBody()["rate"];
        if ($rate > 10 || $rate <0 ) {
            return $response->withJson(getResponse("Invalid Rate",401),401);
        }

        $db = new User_Handler($this->con);
        $respond = $db->ratePage($user_id, $page_id,$rate);
        return $response->withJson($respond, $respond[STATUS]);
    }


    public function getComments(Request $request, Response $response, $args)
    {
        $page_id = $args['page_id'];
        $last_id = $request->getQueryParams()['last_id'];
        $limit = 15;
        $db = new User_Handler($this->con);
        $respond = $db->getComments($page_id, $last_id, $limit);
        return $response->withJson($respond, 200);
    }

    public function commentPage(Request $request, Response $response, $args)
    {
        global $user_id;
        $page_id = $args ['page_id'];
        $text = $request->getParsedBody()['text'];

        if (!preg_match('/^\s*\S.*$/', $text)) {
            $respond = getResponse("نظر نمیتواند خالی بماند", 401);
            return $response->withJson($respond, $respond[STATUS]);
        }
        $db = new User_Handler($this->con);
        $respond = $db->commentOnPage($user_id, $page_id, $text);
        return $response->withJson($respond, $respond[STATUS]);
    }

    public function replyComment(Request $request, Response $response, $args)
    {


        global $user_id;
        $comment_id = $args['comment_id'];
        $reply = $request->getParsedBody()['reply'];


        if (!preg_match('/^\s*\S.*$/', $reply)) {
            $respond = getResponse("پاسخ نمیتواند خالی بماند", 401);
            return $response->withJson($respond, $respond[STATUS]);
        }

        $db = new User_Handler($this->con);
        $respond = $db->replyComment($user_id, $reply, $comment_id);
        return $response->withJson($respond, $respond[STATUS]);

    }

    public function compress(Request $request, Response $response, $args)
    {

        $page_id = $args['page_id'];
        $db = new User_Handler($this->con);
        $db->increaseView($page_id);
        return $response->withHeader('Cache-Control', 'public, max-age=3')->withStatus(204);
    }



    public function reportPage(Request $request, Response $response, $args)
    {
        global $user_id;
        $page_id = $args["page_id"];
        $rt_id = $request->getParsedBody()["rt_id"];
        $prefix = $request->getParsedBody()["prefix"];
        $db = new User_Handler($this->con);
        $respond = $db->reportPage($page_id, $user_id, $rt_id, $prefix);
        return $response->withJson($respond, $respond[STATUS]);
    }

    public function reportCm(Request $request, Response $response, $args)
    {
        global $user_id;
        $comment_id = $args["comment_id"];
        $admin = $request->getParsedBody()["admin"];



        $db = new User_Handler($this->con);
        $respond = $db->report_cm($user_id, $comment_id,$admin);

        return $response->withJson($respond, $respond[STATUS]);

    }


    ////////////////////////////////////////////////////PurchaseGold\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function getInventory(Request $request, Response $response)
    {
        global $user_id;
        $db = new User_Handler($this->con);
        $inventory = $db->getInventory($user_id);
        header("Cache-Control: no-cache, must-revalidate");
        return $response->withStatus(200)->getBody()->write($inventory);
    }

    public function getGoldList(Request $request, Response $response)
    {
        global $user_id;
        $db = new User_Handler($this->con);
        $respond = $db->getGoldList($user_id);
        return $response->withHeader("Cache-Control", "no-cache, must-revalidate")->withJson($respond, 200);
    }

    public function goldReq(Request $request, Response $response)
    {
        $apikey = $request->getHeader("Authorization")[0];
        $gold_id = $request->getParsedBody()["gold_id"];
        $aow = $request->getParsedBody()["aow"]; // define the request is from app or web for handle call back
        $data = array();
        $data["gold_id"] = $gold_id;
        $data["aow"] = $aow;
        $data_json = json_encode($data);
        $mcypt = new MCrypt();
        $encrypted = $mcypt->encrypt3($data_json, $apikey);
        return $response->withJson(getResponse($encrypted, 200), 200);
    }


    public function goldPur(Request $request, Response $response, $args)

    {
        $mycrypt = new MCrypt();
        $this->container->view->render($response, "temp.php");
        $server_soap = "https://api.nextpay.org/gateway/token.wsdl";
        $request_http = "https://api.nextpay.org/gateway/payment";
        $api_key = "56519f32-55fb-4647-8894-7f85a891461a";
        $title = "نتیجه  پرداخت خرید سکه ";

        $user_id = $args['user_id'];  // first we get user id to achive apikey to decrypt data for purchase
        $db = new User_Handler($this->con);
        $api_key_user = $db->getApikeyByUserid($user_id);
        //if the user id is wrong apikey will return null
        if ($api_key_user == null) {
            return $response->withJson(getResponse("Invalid Apikey", 401), 401);
        }
        //we encrypt apikey to get real apikey
        $api_key_user = $mycrypt->encrypt2($api_key_user);

        $data_temp = $request->getQueryParams()["data"];   //{"gold_id":"1","aow":"app"}

        $data = json_decode($mycrypt->decrypt3($data_temp, $api_key_user), true); // dycrpt to get data

        //if data is null it mean that user id and apikey that data is encrypted with are not match
        if ($data == null) {
            return $response->withJson(getResponse("Invalid Apikey", 401), 401);
        }

        //get purchase info
        $gold_id = $data["gold_id"];
        $aow = $data['aow'];
        //define callback here to see if the request if from app or web
        $callback_uri = BASE_API . "goldPurBack?aow=" . $aow;


        $faktor_respond = $db->makeItinialGoldFaktor($gold_id, $user_id);
        if ($faktor_respond["order_id"] == null) {
            $message = $faktor_respond[MESSAGE];
            return $this->payResylt($message, $response,$aow,0,$title);
        }
        $order_id = $faktor_respond["order_id"];
        $price = $faktor_respond["price"];

        $parameters = array
        (
            "api_key" => $api_key,
            "order_id" => $order_id,
            "amount" => $price,
            "callback_uri" => $callback_uri);

        try {
            $soap_client = new SoapClient($server_soap, array('encoding' => 'UTF-8'));
            $res = $soap_client->TokenGenerator($parameters);
            $res = $res->TokenGeneratorResult;
            if ($res != "" && $res != NULL && is_object($res)) {
                if (intval($res->code) == -1) {
                    $trans_id = $res->trans_id;

                    if ($db->goldFaktorTransUpdate($trans_id, $order_id)) {

                        return $response->withStatus(302)->withHeader('Location', "$request_http/$trans_id");

                        /*    echo "<script type='text/javascript'>  window.location='$request_http/$trans_id'; </script>";
                            header("location:.$request_http./$trans_id");*/

                    } else {
                        $db->updateGoldState(2, $order_id);

                        $message = "خطا در ذخیره شناسه پرداخت لطفا دوباره تلاش کنید ";
                        return $this->payResylt($message, $response,$aow,0,$title);

                    }

                } else {
                    $db->updateGoldState(3, $order_id);
                    $db->updateGoldCode(intval($res->code), $order_id);
                    $message = $db->code_error(intval($res->code));
                    return $this->payResylt($message, $response,$aow,0,$title);
                }


            } else {
                $db->updateGoldState(4, $order_id);
                $message = "خطا در ساخت تراکنش ! ";
                return $this->payResylt($message, $response,$aow,0,$title);
            }

        } catch (\Exception $e) {

            $db->updateGoldState(5, $order_id);
            $message = $e->getMessage();
            return $this->payResylt($message, $response,$aow,0,$title);
        }

    }

    public function goldPurBack(Request $request, Response $response)
    {

        $aow = $request->getQueryParams()["aow"];
        $api_key = "56519f32-55fb-4647-8894-7f85a891461a";
        $price = 0;
        $title = "نتیجه  پرداخت خرید سکه ";

        $trans_id = isset($_POST['trans_id']) ? $request->getParsedBody()["trans_id"] : false;
        $order_id = isset($_POST['order_id']) ? $request->getParsedBody()["order_id"] : false;

        if (!$trans_id) {
            $message = "خطا در انجام عملیات بانکی ، شناسه تراکنش موجود نمی باشد";
            return $this->payResylt($message, $response,$aow,0,$title);

        }

        if (!is_string($trans_id) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $trans_id) !== 1)) {
            $message = "کد تراکنش ارسال شده معتبر نمیباشد";
            return $this->payResylt($message, $response,$aow,0,$title);
        }


        $db = new User_Handler($this->con);
        $result = $db->checkGoldTransaction($trans_id, $order_id);

        if ($result["state"] == 0) {
            $message="تراکنش مورد نظر موجود نمیباشد!";
            return $this->payResylt($message, $response,$aow,0,$title);


        } else if ($result["state"] == 1) {
            $message = "وضعیت تراکنش قبلا مشخص گردیده است";
            return $this->payResylt($message, $response,$aow,0,$title);

        } else if ($result["state"] == 2) {
            $user_id = $result["user_id"];
            $price = $result["price"];
            $cnt = $result["cnt"];
            $parameters = array("api_key" => $api_key,
                "order_id" => $order_id,
                "amount" => $price,
                "trans_id" => $trans_id);
            $client = new SoapClient('http://api.nextpay.org/gateway/verify.wsdl');
            $verify_obj = $client->PaymentVerification($parameters);
            $code = $verify_obj->PaymentVerificationResult->code;
            if ($code == 0) {
                if ($db->goldPurSuccess($user_id, $cnt, $order_id, $code)) {
                    $inventory = $db->getInventory($user_id);
                    $message="خرید سکه با موفقیت انجام شد موجدی جدید : " . $inventory  .
                        "<br>"
                        ."کد پیگیری : " . $trans_id;
                    return $this->payResylt($message, $response,$aow,1,$title);

                } else {
                    $message = "خطا در به روز رسانی تراکنش " . $trans_id . "   لطفا برای پیگیری با پشتیبانی تماس حاصل فرمایید   ";
                    return $this->payResylt($message, $response,$aow,0,$title);

                }
            } else {
                $db->updateGoldState(6, $order_id);
                $db->updateGoldCode($code, $order_id);
                $message = "خطا در پرداخت ! ";
                return $this->payResylt($message, $response, $aow, 0, $title);

            }


        }


    }


    function payResylt($message, Response $response,$aow,$state,$title)
    {

        $mycrypt = new MCrypt();
        $data = array();
        $data["message"] = $message;
        $data["aow"]=$aow;
        $data["state"]=$state;
        $data["title"]=$title;

        $data = json_encode($data);
        $data = $mycrypt->encrypt2($data);

        $mylink = BASE_URL . "payr?data=$data";
        return $response->withStatus(302)->withHeader('Location', $mylink);


    }



    public function checkGoldPur(Request $request, Response $response)
    {

        $api_key = "56519f32-55fb-4647-8894-7f85a891461a";
        $price = 0;
        $trans_id = isset($_GET['trans_id']) ? $request->getQueryParams()["trans_id"] : false;
        $order_id = isset($_GET['order_id']) ? $request->getQueryParams()["order_id"] : false;
        if (!$trans_id) {
            return $this->goldFailure("خطا در انجام عملیات بانکی ، شناسه تراکنش موجود نمی باشد", $response);
        }
        if (!is_string($trans_id) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $trans_id) !== 1)) {

            return $this->goldFailure("کد تراکنش ارسال شده معتبر نمیباشد", $response);
        }
        $db = new User_Handler($this->con);
        $result = $db->checkGoldTransaction($trans_id, $order_id);

        if ($result["state"] == 0) {
            return $this->goldFailure("تراکنش مورد نظر موجود نمیباشد!", $response);

        } else if ($result["state"] == 1) {
            return $this->goldFailure("وضعیت تراکنش قبلا مشخص گردیده است", $response);
        } else if ($result["state"] == 2) {
            $user_id = $result["user_id"];
            $price = $result["price"];
            $cnt = $result["cnt"];
            $parameters = array("api_key" => $api_key,
                "order_id" => $order_id,
                "amount" => $price,
                "trans_id" => $trans_id);
            $client = new SoapClient('http://api.nextpay.org/gateway/verify.wsdl');
            $verify_obj = $client->PaymentVerification($parameters);
            $code = $verify_obj->PaymentVerificationResult->code;
            if ($code == 0) {
                if ($db->goldPurSuccess($user_id, $cnt, $order_id, $code)) {
                    $inventory = $db->getInventory($user_id);
                    return $this->goldSuccessh($trans_id, $cnt, $inventory, $response);
                } else {
                    return $this->goldFailure("خطا در به روز رسانی تراکنش " . $trans_id . "   لطفا برای پیگیری با پشتیبانی تماس حاصل فرمایید   ", $response);
                }
            } else {
                $db->updateGoldState(6, $order_id);
                $db->updateGoldCode($code, $order_id);
                $message = $db->code_error(intval($code));
                return $this->goldFailure($message, $response);
            }

        }


    }  // for admin level


    public function goldPurBackTest(Request $request, Response $response)
    {
        $trans_id = $request->getParsedBody()["trans_id"];


        $api_key = "56519f32-55fb-4647-8894-7f85a891461a";
        $price = 0;
        $trans_id = isset($_POST['trans_id']) ? $request->getParsedBody()["trans_id"] : false;
        $order_id = isset($_POST['order_id']) ? $request->getParsedBody()["order_id"] : false;


        if (!$trans_id) {
            return $this->goldFailure("خطا در انجام عملیات بانکی ، شناسه تراکنش موجود نمی باشد", $response);
        }

        if (!is_string($trans_id) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $trans_id) !== 1)) {

            return $this->goldFailure("کد تراکنش ارسال شده معتبر نمیباشد", $response);
        }


        $db = new User_Handler($this->con);
        $result = $db->checkGoldTransaction($trans_id, $order_id);

        if ($result["state"] == 0) {
            return $this->goldFailure("تراکنش مورد نظر موجود نمیباشد!", $response);

        } else if ($result["state"] == 1) {
            return $this->goldFailure("وضعیت تراکنش قبلا مشخص گردیده است", $response);
        } else if ($result["state"] == 2) {
            $user_id = $result["user_id"];
            $price = $result["price"];
            $cnt = $result["cnt"];
            $parameters = array("api_key" => $api_key,
                "order_id" => $order_id,
                "amount" => $price,
                "trans_id" => $trans_id);
            $client = new SoapClient('http://api.nextpay.org/gateway/verify.wsdl');
            $verify_obj = $client->PaymentVerification($parameters);
            $code = $verify_obj->PaymentVerificationResult->code;
            if ($code == 0) {
                if ($db->goldPurSuccess($user_id, $cnt, $order_id, $code)) {
                    $inventory = $db->getInventory($user_id);
                    return $this->goldSuccessh($trans_id, $cnt, $inventory, $response);
                } else {
                    return $this->goldFailure("خطا در به روز رسانی تراکنش " . $trans_id . "   لطفا برای پیگیری با پشتیبانی تماس حاصل فرمایید   ", $response);
                }
            } else {
                $db->updateGoldState(6, $order_id);
                $db->updateGoldCode($code, $order_id);
                return $this->goldFailure("خطا در پرداخت !", $response);

            }

        }


    }

    ////////////////////////////////////////////////////PurchaseSpecial\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function getSpecials(Request $request, Response $response, $args)
    {
        global $user_id;
        $page_id = $args["page_id"];
        $db = new User_Handler($this->con);

        if (!$db->isUserOwner($page_id, $user_id) || !$db->isPageActive($page_id)) {
            return $response->withJson(getResponse("عملیات غیر مجاز میباشد", 403), 403);
        }

        $respond = $db->getSpecials($page_id,$user_id);
        return $response->withJson($respond, 200);

    }

    public function getuserSpecials(Request $request, Response $response, $args)
    {

        global $user_id;

        $db = new User_Handler($this->con);

        $respond = $db->getUserspecials($user_id);

        return $response->withJson($respond, 200);

    }

    public function reserveBg(Request $request, Response $response, $args)
    {
        $special_id = $args["special_id"];
        $page_id = $request->getQueryParams()["page_id"];
        $db = new User_Handler($this->con);
        $needs = $db->getPurchaseNeeds($special_id, $page_id);

        $is_main = $needs["is_main"];
        $max = $needs["max"];
        $day = $needs["day"];
        $message = $needs["message"];
        $gold = $needs["gold"];
        $cat_id = $needs["cat_id"];
        $social_id = $needs["social_id"];
        $user_id = $needs["user_id"];
        $prefix = $needs["prefix"];
        $banner_need = $needs["banner_need"];



        $respond = $db->reservByGold($max, $cat_id, $is_main, $social_id, $page_id, $special_id, $user_id, $gold, $prefix, $day, $message, $banner_need);
        return $response->withJson($respond, $respond[STATUS]);

    }


    public function reserveReq(Request $request, Response $response)
    {
        $apikey = $request->getHeader("Authorization")[0];
        $special_id =$request->getParsedBody()["special_id"];
        $aow = $request->getParsedBody()["aow"]; // define the request is from app or web for handle call back
        $page_id = $request->getParsedBody()["page_id"];
        $data = array();
        $data["special_id"] = $special_id;
        $data["aow"] = $aow;
        $data["page_id"] = $page_id;
        $data_json = json_encode($data);
        $mcypt = new MCrypt();
        $encrypted = $mcypt->encrypt3($data_json, $apikey);
        return $response->withJson(getResponse($encrypted, 200), 200);
    }

    public function reserveBc(Request $request, Response $response, $args)
    {
        $server_soap = "https://api.nextpay.org/gateway/token.wsdl";
        $request_http = "https://api.nextpay.org/gateway/payment";
        $api_key = "56519f32-55fb-4647-8894-7f85a891461a";



        $db = new User_Handler($this->con);
        $user_id = $args["user_id"];

        $api_key_user = $db->getApikeyByUserid($user_id);

        $data_encrypted =  $request->getQueryParams()["data"];
        $mycrypt = new MCrypt();

        $api_key_user = $mycrypt->encrypt2($api_key_user);


        $data = json_decode($mycrypt->decrypt3($data_encrypted, $api_key_user), true); // dycrpt to get data

        //if data is null it mean that user id and apikey that data is encrypted with are not match
        if ($data == null) {
            return $response->withJson(getResponse("Invalid Apikey", 401), 401);
        }

        $special_id = $data["special_id"];
        $page_id = $data["page_id"];
        $aow = $data["aow"];

        $title = "نتیجه پرداخت رزرو جایگاه ";



        $needs = $db->getPurchaseNeeds($special_id, $page_id);
        $is_main = $needs["is_main"];
        $max = $needs["max"];
        $day = $needs["day"];
        $price = $needs["price"];
        $cat_id = $needs["cat_id"];
        $social_id = $needs["social_id"];
        $user_id = $needs["user_id"];
        $prefix = $needs["prefix"];
        $banner_need = $needs["banner_need"];



        $callback_uri = BASE_API . "reserveBack?aow=$aow";

        $result = $db->reservByCache($max, $cat_id, $prefix, $is_main, $social_id, $page_id, $special_id, $user_id, $price, $banner_need);
        if ($result["state"] == -1) {
            $message = $prefix .  " مورد نظر در حال حاضر در این جایگاه رزرو میباشد";
            return $this->payResylt($message,$response,$aow,0,$title);

        } else if ($result["state"] == -2) {

            //error inserting data ; rare!!!
            $message = "خطا در ثبت ! دوباره تلاش کنید";
            return $this->payResylt($message,$response,$aow,0,$title);

           // return $this->RFailure($prefix . " خطا در ثبت  .کد خطا . لطفا دوباره تلاش کنید ", $response);
        } else if ($result["state"] == -3) {
            $message="متاسفانه ظرفیت این جایگاه در حال حاضر تکمیل  گردیده است";
            return $this->payResylt($message,$response,$aow,0,$title);
        //    return $this->RFailure(" ", $response);
        } else if ($result["state"] == -4) {
            $message="خطا در ثبت ! دوباره تلاش کنید";
            return $this->payResylt($message,$response,$aow,0,$title);

        //    return $this->RFailure("خطا در ثبت  .کد خطا : 2 . لطفا دوباره تلاش کنید ", $response);
        } else if ($result["state"] == -5) {
            $message="برای رزور این جایگاه ابتدا باید بنر " . $prefix . " را ثبت نمایید";

            return $this->payResylt($message,$response,$aow,0,$title);

        } else {
            $order_id = $result["so_id"];
            $sr_id = $result["sr_id"];
            $parameters = array
            (
                "api_key" => $api_key,
                "order_id" => $order_id,
                "amount" => $price,
                "callback_uri" => $callback_uri);

            try {
                $soap_client = new SoapClient($server_soap, array('encoding' => 'UTF-8'));
                $res = $soap_client->TokenGenerator($parameters);
                $res = $res->TokenGeneratorResult;
                if ($res != "" && $res != NULL && is_object($res)) {
                    if (intval($res->code) == -1) {
                        $trans_id = $res->trans_id;
                        if ($db->reserveTransUpdate($trans_id, $order_id, $sr_id)) {

                            return $response->withStatus(302)->withHeader('Location', "$request_http/$trans_id");

                /*            echo "<script type='text/javascript'>  window.location='$request_http/$trans_id'; </script>";
                            header("location:.$request_http./$trans_id");*/

                        } else {
                            $db->deleteSr($order_id);
                            $db->updateReserveState(2, $order_id);
                            $message = "خطا در ذخیره شناسه پرداخت لطفا دوباره تلاش کنید ";
                            return $this->payResylt($message,$response,$aow,0,$title);

                        }

                    } else {
                        $db->deleteSr($order_id);
                        $db->updateReserveState(3, $order_id);
                        $db->updateReserveCode(intval($res->code), $order_id);
                        $message = $db->code_error(intval($res->code));
                        return $this->payResylt($message,$response,$aow,0,$title);
                    }
                } else {
                    $db->deleteSr($order_id);
                    $db->updateReserveState(4, $order_id);
                    $message = "خطا در ساخت تراکنش ! ";
                    return $this->payResylt($message,$response,$aow,0,$title);
                }

            } catch (\Exception $e) {
                $db->deleteSr($order_id);
                $db->updateReserveState(5, $order_id);
                $message = $e->getMessage();
                return $this->payResylt($message,$response,$aow,0,$title);

            }
        }

    }

    public function reserveBack(Request $request, Response $response)
    {
        $api_key = "56519f32-55fb-4647-8894-7f85a891461a";
        $price = 0;
        $trans_id = isset($_POST['trans_id']) ? $request->getParsedBody()["trans_id"] : false;
        $order_id = isset($_POST['order_id']) ? $request->getParsedBody()["order_id"] : false;
        $aow = $request->getQueryParams()["aow"];
        $title =null ;


        if (!$trans_id) {
            $message="خطا در انجام عملیات بانکی ، شناسه تراکنش موجود نمی باشد";
            return $this->payResylt($message, $response, $aow, 0, "نتیجه پرداخت رزرو جایگاه");
        }

        if (!is_string($trans_id) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $trans_id) !== 1)) {
            $message="کد تراکنش ارسال شده معتبر نمیباشد";
            return $this->payResylt($message, $response, $aow, 0, "نتیجه پرداخت رزرو جایگاه");
        }


        $db = new User_Handler($this->con);
        $result = $db->checkReserveTransaction($trans_id, $order_id);

        if ($result["state"] == 0) {


            $message="تراکنش مورد نظر موجود نمیباشد!";
            return $this->payResylt($message, $response, $aow, 0, "نتیجه پرداخت رزرو جایگاه");


        } else if ($result["state"] == 1) {
            $message="وضعیت تراکنش قبلا مشخص گردیده است";
            return $this->payResylt($message, $response, $aow, 0, "نتیجه پرداخت رزرو جایگاه");

        } else if ($result["state"] == 2) {

            $price = $result["price"];
            $page_id = $result["page_id"];
            $special_id = $result["special_id"];
            $title = $result["title"];


            $parameters = array("api_key" => $api_key,
                "order_id" => $order_id,
                "amount" => $price,
                "trans_id" => $trans_id);
            $client = new SoapClient('http://api.nextpay.org/gateway/verify.wsdl');
            $verify_obj = $client->PaymentVerification($parameters);
            $code = $verify_obj->PaymentVerificationResult->code;
            if ($code == 0) {
                $needs = $db->getPurchaseNeedsInback($special_id, $page_id);
                $day = $needs["day"];
                $message_special = $needs["message"];

                $prefix = $needs["prefix"];
                $pname_= $needs["p_name"];
                $cat_name = $needs["cat_name"];
                $page_name = $needs["page_name"];
                // be rooz resanie nahai
                $times = $db->reserveSuccess($order_id, $day);
                $start_time = $times["start_time"];
                $exp_time = $times["exp_time"];


                $start_jalali = jdate("H:i Y/m/d" , $start_time);
                $exp_jalali = jdate("H:i Y/m/d" , $exp_time);


                $db->updateReserveState(6, $order_id);
                $db->updateReserveCode(0, $order_id);

                 //it means it is reserved in main (banner or special)
                if ($special_id < 3) {
                    $message = $prefix. " " . $page_name .  "  به مدت " . $day *24 . " ساعت در " .$message_special . " " . $pname_. " رزرو گردید " . "<br />" .
                        " کد پیگیری : " . $trans_id;
                }else {
                    $message = $prefix. " " . $page_name .  "  به مدت " . $day*24 . " ساعت در " .$message_special . " " . $pname_. " دسته بندی ". $cat_name .
                        " رزرو گردید " . "<br />" .
                        " کد پیگیری : " . $trans_id;
                }

                return $this->payResylt($message, $response, $aow, 1, $title);


            } else {
                $db->deleteSr($order_id);
                $db->updateReserveState(6, $order_id);
                $db->updateReserveCode($code, $order_id);
                $message = "خطا در پرداخت !";
                return $this->payResylt($message, $response, $aow, 0, $title);

            }

        }


    }



    ////////////////////////////////////////////////////PurchaseWord\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // inja taiin mikonim ke hazine reserve chand ta seke va chand rooze

    public function reserveWordReq(Request $request, Response $response)
    {
        $apikey = $request->getHeader("Authorization")[0];
        $word =$request->getParsedBody()["word"];
        $aow = $request->getParsedBody()["aow"]; // define the request is from app or web for handle call back
        $page_id = $request->getParsedBody()["page_id"];
        $db = new User_Handler($this->con);
        $social_id = $db->getWordReserveNeeds($page_id,$word);

        if (!preg_match('/^\s*\S.*$/', $word)) {
            $respond = getResponse("متن انتخابی نمیتواند خالی بماند", 400);
            return $response->withJson($respond, $respond[STATUS]);
        }
        //if the return value is 0 it means it is currenlty reserved
        if ($social_id==0) {
            return $response->withJson(getResponse("عبارت مورد نظر در حال حاضر رزرو میباشد !", 400), 400);
        }else {
            $data = array();
            $data["word"] = $word;
            $data["aow"] = $aow;
            $data["page_id"] = $page_id;
            $data["social_id"] = $social_id;
            $data_json = json_encode($data);
            $mcypt = new MCrypt();
            $encrypted = $mcypt->encrypt3($data_json, $apikey);
            return $response->withJson(getResponse($encrypted, 200), 200);
        }


    }

    public function wordReserveBC(Request $request, Response $response, $args)
    {
        $server_soap = "https://api.nextpay.org/gateway/token.wsdl";
        $request_http = "https://api.nextpay.org/gateway/payment";
        $api_key = "56519f32-55fb-4647-8894-7f85a891461a";


        $db = new User_Handler($this->con);
        $user_id = $args["user_id"];

        $api_key_user = $db->getApikeyByUserid($user_id);

        $data_encrypted =  $request->getQueryParams()["data"];
        $mycrypt = new MCrypt();

        $api_key_user = $mycrypt->encrypt2($api_key_user);


        $data = json_decode($mycrypt->decrypt3($data_encrypted, $api_key_user), true); // dycrpt to get data

        //if data is null it mean that user id and apikey that data is encrypted with are not match
        if ($data == null) {
            return $response->withJson(getResponse("Invalid Apikey", 401), 401);
        }

        $word = $data["word"];
        $page_id = $data["page_id"];
        $aow = $data["aow"];
        $social_id = $data["social_id"];


        $day = 2;
        $price = 150;
        $title = "نتیجه پرداخت رزرو عبارت ";


        $callback_uri = BASE_API . "wordReserveBack?aow=$aow";


        $result = $db->reserveWordByCache($social_id,$page_id,$user_id,$price,$word,$day);


        if ($result["state"] == -1) {
            $message =    "عبارت مورد نظر در حال حاضر رزرو میباشد";
            return $this->payResylt($message,$response,$aow,0,$title);

        } else if ($result["state"] == -2) {

            //error inserting data ; rare!!!
            $message = "خطا در ثبت ! دوباره تلاش کنید";
            return $this->payResylt($message,$response,$aow,0,$title);

            // return $this->RFailure($prefix . " خطا در ثبت  .کد خطا . لطفا دوباره تلاش کنید ", $response);
        }  else if ($result["state"] == -4) {
            $message="خطا در ثبت ! دوباره تلاش کنید";
            return $this->payResylt($message,$response,$aow,0,$title);

            //    return $this->RFailure("خطا در ثبت  .کد خطا : 2 . لطفا دوباره تلاش کنید ", $response);
        } else {
            $order_id = $result["wo_id"];
            $wr_id = $result["wr_id"];
            $parameters = array
            (
                "api_key" => $api_key,
                "order_id" => $order_id,
                "amount" => $price,
                "callback_uri" => $callback_uri);

            try {
                $soap_client = new SoapClient($server_soap, array('encoding' => 'UTF-8'));
                $res = $soap_client->TokenGenerator($parameters);
                $res = $res->TokenGeneratorResult;
                if ($res != "" && $res != NULL && is_object($res)) {
                    if (intval($res->code) == -1) {
                        $trans_id = $res->trans_id;
                        if ($db->reserveWordTransUpdate($trans_id, $order_id, $wr_id)) {

                            return $response->withStatus(302)->withHeader('Location', "$request_http/$trans_id");


                        } else {
                            $db->deleteWr($order_id);
                            $db->updateWordReserveState(2, $order_id);
                            $message = "خطا در ذخیره شناسه پرداخت لطفا دوباره تلاش کنید ";
                            return $this->payResylt($message,$response,$aow,0,$title);

                        }

                    } else {
                        $db->deleteWr($order_id);
                        $db->updateWordReserveState(3, $order_id);
                        $db->updateWordReserveCode(intval($res->code), $order_id);
                        $message = $db->code_error(intval($res->code));
                        return $this->payResylt($message,$response,$aow,0,$title);
                    }
                } else {
                    $db->deleteWr($order_id);
                    $db->updateWordReserveState(4, $order_id);
                    $message = "خطا در ساخت تراکنش ! ";
                    return $this->payResylt($message,$response,$aow,0,$title);
                }

            } catch (\Exception $e) {
                $db->deleteWr($order_id);
                $db->updateWordReserveState(5, $order_id);
                $message = $e->getMessage();
                return $this->payResylt($message,$response,$aow,0,$title);

            }
        }
    }

    public function wordReserveBack(Request $request, Response $response)
    {
        $api_key = "56519f32-55fb-4647-8894-7f85a891461a";
        $price = 0;
        $trans_id = isset($_POST['trans_id']) ? $request->getParsedBody()["trans_id"] : false;
        $order_id = isset($_POST['order_id']) ? $request->getParsedBody()["order_id"] : false;
        $aow = $request->getQueryParams()["aow"];
        $title ="نتیجه پرداخت رزرو عبارت" ;


        if (!$trans_id) {
            $message="خطا در انجام عملیات بانکی ، شناسه تراکنش موجود نمی باشد";
            return $this->payResylt($message, $response, $aow, 0, "نتیجه پرداخت رزرو عبارت");
        }

        if (!is_string($trans_id) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $trans_id) !== 1)) {
            $message="کد تراکنش ارسال شده معتبر نمیباشد";
            return $this->payResylt($message, $response, $aow, 0, $title);
        }


        $db = new User_Handler($this->con);
        $result = $db->checkWordReserveTransaction($trans_id, $order_id);

        if ($result["state"] == 0) {

            $message="تراکنش مورد نظر موجود نمیباشد!";
            return $this->payResylt($message, $response, $aow, 0, $title);


        } else if ($result["state"] == 1) {
            $message="وضعیت تراکنش قبلا مشخص گردیده است";
            return $this->payResylt($message, $response, $aow, 0, $title);

        } else if ($result["state"] == 2) {

            $price = $result["price"];
            $page_id = $result["page_id"];

            $parameters = array("api_key" => $api_key,
                "order_id" => $order_id,
                "amount" => $price,
                "trans_id" => $trans_id);
            $client = new SoapClient('http://api.nextpay.org/gateway/verify.wsdl');
            $verify_obj = $client->PaymentVerification($parameters);
            $code = $verify_obj->PaymentVerificationResult->code;
            if ($code == 0) {
                $needs = $db->getWordNeedsInBack($page_id,$order_id);
                $day = $needs["day"];
                $word = $needs["word"];
                $page_name = $needs["page_name"];
                $prefix = $needs["prefix"];

                // be rooz resanie nahai
                $times = $db->reserveWordSuccess($order_id, $day);

                $start_time = $times["start_time"];
                $exp_time = $times["exp_time"];


                $start_jalali = jdate("H:i Y/m/d" , $start_time);
                $exp_jalali = jdate("H:i Y/m/d" , $exp_time);


                $db->updateWordReserveState(6, $order_id);
                $db->updateWordReserveCode(0, $order_id);

                //it means it is reserved in main (banner or special)
                $message = "عبارت " . $word . " با موفقیت به مدت " . $day * 24 .  " ساعت برای " . $prefix . " " .
                    $page_name . " رزرو گردید برای اطلاع از زمان باقیمانده به بخش ویژه شده ها در حساب کاربری مراجعه نمایید";

                return $this->payResylt($message, $response, $aow, 1, $title);


            } else {
                $db->deleteWr($order_id);
                $db->updateWordReserveState(6, $order_id);
                $db->updateWordReserveCode($code, $order_id);
                $message = "خطا در پرداخت !";
                return $this->payResylt($message, $response, $aow, 0, $title);

            }

        }


    }


    public function reserveWord(Request $request, Response $response, $args)
    {
        global $user_id;

        $gold = 10;
        $days = 2;
        $page_id = $args["page_id"];
        $word = $request->getParsedBody()["word"];
        if (!preg_match('/^\s*\S.*$/', $word)) {
            $respond = getResponse("متن انتخابی نمیتواند خالی بماند", 401);
            return $response->withJson($respond, $respond[STATUS]);
        }
        $db = new User_Handler($this->con);
        $social_id = $db->getWordReserveNeeds($page_id,$word);

        if ($social_id==0) {
            return $response->withJson(getResponse("عبارت مورد نظر در حال حاضر رزرو میباشد ",400),400);
        }

        $respond = $db->reserveWord($page_id, $social_id, $user_id, $word, $gold, $days);
        return $response->withJson($respond, $respond[STATUS]);

    }



    ////////////////////////////////////////////////////Reciving\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    //gereftane bannerga che main che to category
    // manozoor public e az p
    public function PProfile(Request $request, Response $response, $args)
    {
        $user_id = $args ["user_id"];
        $db = new User_Handler($this->con);
        $profile = $db->get_P_Profile($user_id);
        return $response->withJson($profile, 200);
    }
    // dar edameye profile gereftane safehaye sakhte shodeye karbar
    //public user pages
    public function P_Upages(Request $request, Response $response, $args)
    {
        $user_id = $args ["user_id"];
        $social_id = $request->getQueryParams()["social_id"];
        $page = $request->getQueryParams()["page"];
        $prefix = $request->getQueryParams()["prefix"];
        $limit = 6;
        $db = new User_Handler($this->con);
        $respond = $db->get_P_Upage($user_id, $social_id, $limit, $page, $prefix);
        return $respond[STATUS] == 200 ? $response->withJson($respond["ppages"], 200) : $response->withJson($respond, $respond[STATUS]);
    }

    public function PSpecials(Request $request, Response $response, $args)
    {
        $page_id = $args["page_id"];
        $db = new User_Handler($this->con);
        $respond = $db->get_P_Specials($page_id);
        return $response->withJson($respond, 200);
    }

    public function PDetail(Request $request, Response $response, $args)
    {
        // agar karbar login nakarde bashe user_id mishe 0
        $user_id = 0;
        $page_id = $args["page_id"];
        $user_id = $request->getQueryParams()["user_id"];
        $db = new User_Handler($this->con);
        $respond = $db->get_P_Detail($page_id, $user_id);
        return $response->withJson($respond, 200);

    }

    public function PSections(Request $request, Response $response, $args)
    {
        $social_id = $args["social_id"];
        $main = $request->getQueryParams()["main"];
        $cat_id = $request->getQueryParams()["cat_id"];
        $prefix = $request->getQueryParams()["prefix"];
        $db = new User_Handler($this->con);
        $respond = $db->get_P_sections($main, $cat_id, $social_id, $prefix);

        return $response->withJson($respond, 200);
    }

    public function PList(Request $request, Response $response, $args)
    {
        $social_id = $args["social_id"];
        $main = $request->getQueryParams()["main"];
        $cat_id = $request->getQueryParams()["cat_id"];
        $key = $request->getQueryParams()["key"];
        $page = $request->getQueryParams()["page"];
        $db = new User_Handler($this->con);
        $respond = $db->get_P_list($social_id, $key, $main, $cat_id, $page);
        return  $response->withJson($respond, 200);
    }

    public function Psearch(Request $request, Response $response, $args)
    {
        $social_id = $args["social_id"];
        $page = $request->getQueryParams()["page"];
        $search = $request->getQueryParams()["search"];
        $limit = 8;
        $db = new User_Handler($this->con);
        $respond = $db->get_P_search($social_id, $page, $search, $limit);
        return $response->withJson($respond["ppages"], 200);

    }


    ////////////////////////////////////////////////////Categories\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function categories(Request $request, Response $response)
    {
        $db = new User_Handler($this->con);
        $result = $db->getCats();
        return $result[STATUS] == 200 ? $response->withJson($result['cats'], 200) : $response->withJson($result, $result[STATUS]);
    }

    ////////////////////////////////////////////////////Web\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    public function getDashbord(Request $request, Response $response)
    {
        global $user_id;
        $db = new User_Handler($this->con);
        $respond = $db->getDashbord($user_id);
        header("Cache-Control: no-cache, must-revalidate");
        return $response->withJson($respond, 200);
    }

    public function getPagesWithComments (Request $request, Response $response) {
            global $user_id;
            $db = new User_Handler($this->con);
            $respond = $db->getPagesWithComments($user_id);
            return $response->withJson($respond, 200);
    }


//////////////////////////////////////////////////////////////////Functions\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    function RFailure($message, Response $response)
    {
        $mylink = "http://192.168.1.147/pooshka/nav/Rfailure.php?message=$message";
        return $response->withStatus(302)->withHeader('Location', $mylink);

    }

    function RSuccess($message, $trans_id, Response $response)
    {
        $mylink = "http://192.168.1.147/pooshka/nav/Rsuccess.php?trans_id=$trans_id&message=$message";
        return $response->withStatus(302)->withHeader('Location', $mylink);

    }




}