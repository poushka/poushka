<?php
//*************************************************************AdminRoutes*************************************************************************\\
$app->get('/getSocialA', "Admin:getSocialA");

///////////////////////////////////////////////////////////////GENERAL_ROUTES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
$app->group("/kfc", function () use ($app , $container) {

    $app->get('/getSupport', "Admin:getSupport")->add(new Src\MiddleWare\AdminAuth(5,$container));
    $app->put('/checkSupport/{support_id}', "Admin:checkSupport")->add(new Src\MiddleWare\AdminAuth(5,$container));

///////////////////////////////////////////////////////////////SOCIAL_ROUTES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $app->post('/makeSocialCat', "Admin:makeSocialCat")->add(new \Src\MiddleWare\Social_Img_filter(0))->add(new Src\MiddleWare\AdminAuth(8,$container));
    
    $app->put('/updateSocial/{social_id}',"Admin:updateSocial")->add(new Src\MiddleWare\AdminAuth(8,$container));
    $app->put('/updateSocialActive/{social_id}',"Admin:updateSocialActive")->add(new Src\MiddleWare\AdminAuth(8,$container));
    $app->post('/updateSocialPic',"Admin:updateSocialPic")->add(new \Src\MiddleWare\Social_Img_filter(1))->add(new Src\MiddleWare\AdminAuth(8,$container));
    $app->post('/updateSocialIcon',"Admin:updateSocialIcon")->add(new \Src\MiddleWare\Social_Img_filter(2))->add(new Src\MiddleWare\AdminAuth(8,$container));

///////////////////////////////////////////////////////////////CATEGORY_ROUTES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $app->post('/makeCat',"Admin:makeCat")->add(new \Src\MiddleWare\Social_Img_filter(2))->add(new Src\MiddleWare\AdminAuth(8,$container));
    $app->get('/getCatAdmin', "Admin:getCatAdmin")->add(new Src\MiddleWare\AdminAuth(5,$container));
    $app->put('/updateCatname/{cat_id}',"Admin:updateCatname")->add(new Src\MiddleWare\AdminAuth(8,$container));
    $app->put('/updateCatState/{cat_id}',"Admin:updateCatState")->add(new Src\MiddleWare\AdminAuth(8,$container));
    $app->put('/updateCatIcon',"Admin:updateCatIcon")->add(new \Src\MiddleWare\Social_Img_filter(2))->add(new Src\MiddleWare\AdminAuth(8,$container));
///////////////////////////////////////////////////////////////PAGEROUTES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $app->get('/getWatingPages',"Admin:getWatingPages")->add(new Src\MiddleWare\AdminAuth(5,$container));
    $app->put('/confirmPage/{page_id}',"Admin:confirmPage")->add(new Src\MiddleWare\AdminAuth(5,$container));

    $app->put('/confrimScreen/{ps_id}',"Admin:confrimScreen")->add(new Src\MiddleWare\AdminAuth(5,$container));


    $app->get('/getWatingProfiles',"Admin:getWatingProfiles")->add(new Src\MiddleWare\AdminAuth(5,$container));
    $app->put('/confirmProfile/{user_id}',"Admin:confirmProfile")->add(new Src\MiddleWare\AdminAuth(5,$container));

    $app->get("/getUpdatedPages","Admin:getUpdatedPages")->add(new Src\MiddleWare\AdminAuth(5,$container));
    $app->put("/confirmUpdatedPage/{pu_id}","Admin:confirmUpdatedPage")->add(new Src\MiddleWare\AdminAuth(5,$container));


    $app->get("/getWatingComments/{page}" ,"Admin:getWatingComments")->add(new Src\MiddleWare\AdminAuth(5,$container));
    $app->put("/confrimComment/{cm_id}" ,"Admin:confrimComment")->add(new Src\MiddleWare\AdminAuth(5,$container));



    $app->get('/confirmCount',"Admin:confirmCount")->add(new Src\MiddleWare\AdminAuth(5,$container));
    $app->get('/getCustomMessages/{parent_id}',"Admin:getCustomMessages")->add(new Src\MiddleWare\AdminAuth(5,$container));

    $app->get('/getBanners',"Admin:getBanners")->add(new Src\MiddleWare\AdminAuth(5,$container));
    $app->post('/confirmBanner/{pb_id}', "Admin:confirmBanner")->add(new Src\MiddleWare\AdminAuth(5,$container));
/////////////////////////////////////////////////////////////SPECIAL\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $app->get("/reserveBu/{special_id}" ,"Admin:reserveBu")->add(new Src\MiddleWare\AdminAuth(8,$container));

/////////////////////////////////////////////////////////////activation\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $app->post("/logindAdmin","Admin:logindAdmin");
    $app->get("/getUser" , "Admin:getUser")->add(new Src\MiddleWare\AdminAuth(8,$container));
    $app->put("/increaseInventory/{user_id}" , "Admin:increaseInventory")->add(new Src\MiddleWare\AdminAuth(8,$container));
    $app->put("/activateUser/{user_id}" , "Admin:activateUser")->add(new Src\MiddleWare\AdminAuth(8,$container));
    $app->get("/getUserTransactions/{user_id}" , "Admin:getUserTransactions")->add(new \Src\MiddleWare\AdminAuth(5,$container));
    $app->get("/checkGoldPur" , "Users:checkGoldPur");
     /////////////////////////////////////////////////////////////Speciialization\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     $app->post("/special","Admin:special");
 
});

///////////////////////////////////////////////////////////////CRONS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

$app->group("/crons", function () use ($app,$container) {
    $app->delete('/screen_cron', "Admin:screen_cron");
    $app->delete('/special_cron', "Admin:special_cron");
    $app->delete('/word_cron', "Admin:word_cron");
});

