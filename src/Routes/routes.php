<?php
//todo taiine amniat baraye route haye admin
//todo ezafe kardane rate limit be route ha



//*************************************************************UserRoutes*************************************************************************\\

/////////////////////////////////////////////////////////////UserRegistration\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
$app->group('',function() use ($app , $container){

    $app->get('/testcurl', "Users:testcurl");
    $app->get('/zarin', "Users:zarin");
    $app->get('/zarinback', "Users:zarinback");

    $app->get('/getSC', "Users:getSC");


    $app->post('/test', "Users:test");
    $app->post('/sendNotification', "Users:sendNotification");
    $app->get('/info', "Users:info");
    $app->get('/faq', "Users:faq");
    $app->get('/gettags', "Users:gettags");
    $app->get('/rules', "Users:rules");
    $app->get('/firebase', "Users:firebase");
    $app->post('/support', "Users:support")->add(new \Src\MiddleWare\Support_Filter())->add(new \Src\MiddleWare\Chanel_img_filter(false));

    $app->get('/supportTypes', "Users:supportTypes");
    $app->post('/register', "Users:Register");
    $app->post('/verify', "Users:verify");
    $app->post('/login', "Users:login");
    $app->post('/reqReset', "Users:reqReset");
    $app->post('/resetPassword', "Users:resetPassword");
    $app->post('/infoUpdate', "Users:infoUpdate")->add(new \Src\MiddleWare\Chanel_img_filter(false))->add(new Src\MiddleWare\UserInfo_Filter())
        ->add(new \Src\MiddleWare\Authentication($container));
	$app->put('/updateFCM', "Users:updateFCM")->add(new \Src\MiddleWare\Authentication($container));	
    $app->get("/getUser" , "Users:getUser")->add(new \Src\MiddleWare\Authentication($container));
    // vase inke bebine vaziate updatesh profile ro change ;
    $app->get("/getUpdatedCode" , "Users:getUpdatedCode")->add(new \Src\MiddleWare\Authentication($container));
    $app->get("/getMessages" , "Users:getMessages")->add(new \Src\MiddleWare\Authentication($container));
    $app->get("/getMessageCount" , "Users:getMessageCount")->add(new \Src\MiddleWare\Authentication($container));
    $app->post("/sendMessage" , "Users:sendMessage")->add(new \Src\MiddleWare\Authentication($container));
    $app->get("/logout" , "Users:logout");

});
//////////////////////////////////////////////////////////////////UserPages\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
$app->post('/estelam/{social_id}' , "Users:estelam");
$app->post('/teleEstelam',"Users:teleEstelam");


$app->put('/estelamAndUpdate/{page_id}' , "Users:estelamAndUpdate")->add(new \Src\MiddleWare\Authentication($container));
$app->post('/screenUpload',"Users:screenUpload")->add(new \Src\MiddleWare\Chanel_img_filter(true));
$app->delete('/screenDelete/{ps_id}',"Users:screenDelete")->add(new \Src\MiddleWare\Authentication($container));


$app->post('/makePage/{social_id}' , "Users:makePage")->add(new \Src\MiddleWare\Chanel_img_filter(true))->add
(new \Src\MiddleWare\Authentication($container))->add(new \Src\MiddleWare\MakePage_Filter());

$app->post('/updatePage/{page_id}' , "Users:updatePage")->add(new \Src\MiddleWare\Chanel_img_filter(false))->add
(new \Src\MiddleWare\Authentication($container))->add(new \Src\MiddleWare\MakePage_Filter());

$app->get('/getBanner/{page_id}',"Users:getBanner")->add(new \Src\MiddleWare\Authentication($container));
$app->post('/bannerUpdate/{page_id}',"Users:bannerUpdate")->add(new \Src\MiddleWare\Banner_Filter())->add(new \Src\MiddleWare\Authentication($container));

$app->get('/getUserPages' , "Users:getUserPages")->add(new \Src\MiddleWare\Authentication($container));
$app->get('/getPageDetails/{page_id}' , "Users:getPageDetails")->add(new \Src\MiddleWare\Authentication($container));
$app->get('/getPage/{page_id}' , "Users:getPage")->add(new \Src\MiddleWare\Authentication($container));


$app->get('/getComments/{page_id}' , "Users:getComments");
$app->post('/commentPage/{page_id}' , "Users:commentPage")->add(new \Src\MiddleWare\Authentication($container));
$app->put('/replyComment/{comment_id}' , "Users:replyComment" )->add(new \Src\MiddleWare\Authentication($container));

$app->post('/likePage/{page_id}' , "Users:likePage")->add(new \Src\MiddleWare\Authentication($container));
$app->delete('/unlikePage/{page_id}' , "Users:unlikePage")->add(new \Src\MiddleWare\Authentication($container));
$app->post('/ratePage/{page_id}' , "Users:ratePage")->add(new \Src\MiddleWare\Authentication($container));

$app->put('/compress/{page_id}' , "Users:compress");
$app->post('/reportPage/{page_id}' , "Users:reportPage")->add(new \Src\MiddleWare\Authentication($container));
$app->post('/reportCm/{comment_id}' , "Users:reportCm")->add(new \Src\MiddleWare\Authentication($container));

$app->get('/getPagesWithComments' , "Users:getPagesWithComments")->add(new \Src\MiddleWare\Authentication($container));


////////////////////////////////////////////////////////////UserPurchaeGold\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
$app->get("/getInventory" ,"Users:getInventory")->add(new \Src\MiddleWare\Authentication($container));
$app->get("/getGoldList" ,"Users:getGoldList")->add(new \Src\MiddleWare\Authentication($container));
$app->post("/goldReq" ,"Users:goldReq")->add(new \Src\MiddleWare\Authentication($container));
$app->get("/goldPur/{user_id}" ,"Users:goldPur");
$app->post("/goldPurBack","Users:goldPurBack");
$app->post("/goldPurBackTest","Users:goldPurBackTest");

////////////////////////////////////////////////////////////UserPurchaeSpecial\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

$app->get("/getSpecials/{page_id}","Users:getSpecials")->add(new \Src\MiddleWare\Authentication($container));
$app->get("/getuserSpecials","Users:getuserSpecials")->add(new \Src\MiddleWare\Authentication($container));


//yani rezer kardan by gold
$app->get("/reserveBg/{special_id}" ,"Users:reserveBg")->add(new \Src\MiddleWare\Authentication($container));
//yani rezer kardan by cash

$app->post("/reserveReq" ,"Users:reserveReq")->add(new \Src\MiddleWare\Authentication($container));
$app->get("/reserveBc/{user_id}" ,"Users:reserveBc");
$app->post("/reserveBack","Users:reserveBack");
////////////////////////////////////////////////////////////ReseveWord\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
///

$app->post("/reserveWordReq" ,"Users:reserveWordReq")->add(new \Src\MiddleWare\Authentication($container));
$app->get( "/wordReserveBC/{user_id}" ,"Users:wordReserveBC");
$app->post("/wordReserveBack","Users:wordReserveBack");

$app->post("/reserveWord/{page_id}" , "Users:reserveWord")->add(new \Src\MiddleWare\Authentication($container));



////////////////////////////////////////////////////publicReciving\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

$app->get("/PProfile/{user_id}" ,"Users:PProfile");
$app->get("/P_Upages/{user_id}" ,"Users:P_Upages");

$app->get("/PDetail/{page_id}" ,"Users:PDetail");
$app->get("/PSections/{social_id}" ,"Users:PSections");
$app->get("/PList/{social_id}" ,"Users:PList");
$app->get("/Psearch/{social_id}" ,"Users:Psearch");

$app->get("/PSpecials/{page_id}" ,"Users:PSpecials");



////////////////////////////////////////////////////////////UserCat\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
$app->get('/categories' , "Users:categories");
////////////////////////////////////////////////////////////WEB_ROUTES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
$app->get("/getDashbord" , "Users:getDashbord")->add(new \Src\MiddleWare\Authentication($container));