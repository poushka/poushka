<?php

require_once __DIR__ . "/../Inc/Config.php";
$conn = null ;

$conn = new \mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
$conn->set_charset("utf8mb4");

if (mysqli_connect_errno()) {
    $response = ['error'=>"faild" , 'message' =>'faild to connect to database'];
    die(json_encode($response));
}else {
    $SCREEN_DIR = __DIR__ .'/../uploads/chanel_screen/';
    $stmt=$conn->prepare("SELECT ps_id , name , thumb_name  from page_screen where page_id is null and TIMESTAMPDIFF(MINUTE,created_at,now()) > 30");
    $stmt->execute();
    $result = $stmt->get_result();
    while ($single_screen = $result->fetch_assoc()) {
        @ unlink($SCREEN_DIR.$single_screen['name']);
        @ unlink($SCREEN_DIR.$single_screen['thumb_name']);
    }
    $stmt=$conn->prepare("delete  from page_screen where page_id is null and TIMESTAMPDIFF(MINUTE,created_at,now()) > 30");
    $stmt->execute();
}


?>