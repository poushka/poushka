<?php

require_once __DIR__ . "/../Inc/Config.php";
$conn = null ;

$conn = new \mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
$conn->set_charset("utf8mb4");

if (mysqli_connect_errno()) {
    $response = ['error'=>"faild" , 'message' =>'faild to connect to database'];
    die(json_encode($response));
}else {
    $stmt=$conn->prepare("DELETE FROM special_r where reserved_flag = 1 and exp_time < now() ;");
    $stmt->execute();

    $stmt=$conn->prepare("DELETE FROM word_r where  exp_time < now() ;");
    $stmt->execute();

    $conn->close();

}

?>