<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

//date_default_timezone_set('Asia/Tehran');
require '../vendor/autoload.php';

//ading test

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$app = new Slim\App(['settings' => ['displayErrorDetails' => true
    ,"addContentLengthHeader"=>false
    ]]

);

//$app->add(\RateLimit\Middleware\RateLimitMiddleware::createDefault(
//    \RateLimit\RateLimiterFactory::createRedisBackedRateLimiter([
//        'host' => '127.0.0.1',
//        'port' => 6379,
//    ], 30, 120) ,
//    [
//        'limitExceededHandler' => function (\Psr\Http\Message\RequestInterface $request , \Psr\Http\Message\ResponseInterface $response) {
//           return $response->withJson(getResponse("to fast",429),429);
//        },
//    ]
//));


$app->add(new RKA\Middleware\IpAddress());
$container = $app->getContainer();
$container['db'] = function () {
    return new \Src\Inc\DbConnect();
};

$container['conn'] = function ($container) {
    $mydb = new \Src\Inc\DbConnect();
    return $mydb->connect();
};


$container['view'] = function ($container) {
    return new \Slim\Views\PhpRenderer('../src/templetes/');
};

$container['Users'] = function ($container) {
    return new Src\Paths\Users($container);
};
$container['Admin'] = function ($container) {
    return new Src\Paths\Admin($container);
};

require __DIR__ . '/../src/Routes/routes.php';
require __DIR__ . '/../src/Routes/adroutes.php';
require __DIR__.'/../src/Inc/GlobalMethods.php';



$app->run();